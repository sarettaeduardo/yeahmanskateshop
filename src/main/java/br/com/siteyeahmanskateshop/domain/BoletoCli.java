package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

public class BoletoCli implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nome;
    private String email;
    private String telefone;
    private String cpf;
    

    public BoletoCli(){
        setNome("");
        setEmail("");
        setTelefone("");
        setCpf("");
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    
    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return "Dados Boleto:" 
        + " Nome: " + getNome()
        + " Email: " + getEmail()
        + " Telefone: " + getTelefone()
        + " CPF: " + getCpf();
    }


}

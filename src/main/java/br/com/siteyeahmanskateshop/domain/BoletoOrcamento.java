package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;


public class BoletoOrcamento implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nome;
    private String email;
    private String telefone;
    private String cpf;
    private HashMap<String, String> listaItens;
    private Instant dataGeracao;
    private Instant dataVencimento;
    private Long total;
    private Long desconto;
    private Long area;
    private String mensagem;

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the area
     */
    public Long getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(Long area) {
        this.area = area;
    }

    /**
     * @return the listaItens
     */
    public HashMap<String, String> getListaItens() {
        return listaItens;
    }

    /**
     * @param listaItens the listaItens to set
     */
    public void setListaItens(HashMap<String, String> listaItens) {
        this.listaItens = listaItens;
    }

    /**
     * @return the desconto
     */
    public Long getDesconto() {
        return desconto;
    }

    /**
     * @param desconto the desconto to set
     */
    public void setDesconto(Long desconto) {
        this.desconto = desconto;
    }

    /**
     * @return the mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * @param mensagem the mensagem to set
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * @return the total
     */
    public Long getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Long total) {
        this.total = total;
    }

    /**
     * @return the dataVencimento
     */
    public Instant getDataVencimento() {
        return dataVencimento;
    }

    /**
     * @param dataVencimento the dataVencimento to set
     */
    public void setDataVencimento(Instant dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    /**
     * @return the dataGeracao
     */
    public Instant getDataGeracao() {
        return dataGeracao;
    }

    /**
     * @param dataGeracao the dataGeracao to set
     */
    public void setDataGeracao(Instant dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return "Dados Boleto:" 
        + " Nome: " + getNome()
        + " Email: " + getEmail()
        + " Telefone: " + getTelefone()
        + " CPF: " + getCpf()
        + " ValorTotal: " + getTotal()
        + " Mensagem: " + getMensagem()
        + " DataGeracao: " + getDataGeracao()
        + " DataVencimento: " + getDataVencimento()
        + " Itens: " + getListaItens().toString();
    }


}

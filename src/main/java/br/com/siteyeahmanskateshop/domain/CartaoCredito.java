package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class CartaoCredito implements Serializable {


    private static final long serialVersionUID = 1L;
    private Cliente cliente;
    private EnderecoEntrega endereco;
    private Long parcelas;
    private Long id_transacao;
    private String payment_token;
    private ArrayList<Integer> produtos;
    private String numero_cartao;

    public CartaoCredito(){
        
    }

    /**
     * @return the numero_cartao
     */
    public String getNumero_cartao() {
        return numero_cartao;
    }

    /**
     * @param numero_cartao the numero_cartao to set
     */
    public void setNumero_cartao(String numero_cartao) {
        this.numero_cartao = numero_cartao;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ArrayList<Integer> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Integer> produtos) {
        this.produtos = produtos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public EnderecoEntrega getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntrega endereco) {
        this.endereco = endereco;
    }

    public Long getParcelas() {
        return parcelas;
    }

    public void setParcelas(Long parcelas) {
        this.parcelas = parcelas;
    }

    public Long getId_transacao() {
        return id_transacao;
    }

    public void setId_transacao(Long id_transacao) {
        this.id_transacao = id_transacao;
    }

    public String getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(String payment_token) {
        this.payment_token = payment_token;
    }
}

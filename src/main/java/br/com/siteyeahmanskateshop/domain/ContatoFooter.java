package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;

public class ContatoFooter implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nome;
    private String email;
    private String mensagem;

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the mensagem
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * @param mensagem the mensagem to set
     */
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return "Contato: Nome: " + getNome() + " , Email: " + getEmail() + " , Mensagem: " + getMensagem() ;
    }

}

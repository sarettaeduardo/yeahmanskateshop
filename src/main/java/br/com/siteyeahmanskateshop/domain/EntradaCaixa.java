package br.com.siteyeahmanskateshop.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A EntradaCaixa.
 */
@Entity
@Table(name = "entrada_caixa")
public class EntradaCaixa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @NotNull
    @Column(name = "valor", nullable = false)
    private Long valor;

    @NotNull
    @Column(name = "data", nullable = false)
    private Instant data;

    @OneToOne
    @JoinColumn(unique = true)
    private Cliente cliente;

    @ManyToMany
    @JoinTable(name = "entrada_caixa_produtos",
               joinColumns = @JoinColumn(name = "entrada_caixas_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "produtos_id", referencedColumnName = "id"))
    private Set<Produtos> produtos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public EntradaCaixa nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public EntradaCaixa descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getValor() {
        return valor;
    }

    public EntradaCaixa valor(Long valor) {
        this.valor = valor;
        return this;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public Instant getData() {
        return data;
    }

    public EntradaCaixa data(Instant data) {
        this.data = data;
        return this;
    }

    public void setData(Instant data) {
        this.data = data;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public EntradaCaixa cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Set<Produtos> getProdutos() {
        return produtos;
    }

    public EntradaCaixa produtos(Set<Produtos> produtos) {
        this.produtos = produtos;
        return this;
    }

    public EntradaCaixa addProdutos(Produtos produtos) {
        this.produtos.add(produtos);
        produtos.getEntradaCaixas().add(this);
        return this;
    }

    public EntradaCaixa removeProdutos(Produtos produtos) {
        this.produtos.remove(produtos);
        produtos.getEntradaCaixas().remove(this);
        return this;
    }

    public void setProdutos(Set<Produtos> produtos) {
        this.produtos = produtos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EntradaCaixa entradaCaixa = (EntradaCaixa) o;
        if (entradaCaixa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), entradaCaixa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EntradaCaixa{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", valor=" + getValor() +
            ", data='" + getData() + "'" +
            "}";
    }
}

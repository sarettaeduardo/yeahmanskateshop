package br.com.siteyeahmanskateshop.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Orcamento.
 */
@Entity
@Table(name = "orcamento")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Orcamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "codigo_transacao")
    private Long codigoTransacao;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "telefone", nullable = false)
    private String telefone;

    @NotNull
    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Column(name = "jhi_link")
    private String link;

    @Column(name = "lista_itens")
    private String listaItens;

    @Column(name = "data_geracao")
    private Instant dataGeracao;

    @Column(name = "data_vencimento")
    private Instant dataVencimento;

    @Column(name = "total")
    private Long total;

    @Column(name = "desconto")
    private Long desconto;

    @Column(name = "area")
    private Long area;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCodigoTransacao() {
        return codigoTransacao;
    }

    public Orcamento codigoTransacao(Long codigoTransacao) {
        this.codigoTransacao = codigoTransacao;
        return this;
    }

    public void setCodigoTransacao(Long codigoTransacao) {
        this.codigoTransacao = codigoTransacao;
    }

    public String getNome() {
        return nome;
    }

    public Orcamento nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public Orcamento email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public Orcamento telefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public Orcamento cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getLink() {
        return link;
    }

    public Orcamento link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getListaItens() {
        return listaItens;
    }

    public Orcamento listaItens(String listaItens) {
        this.listaItens = listaItens;
        return this;
    }

    public void setListaItens(String listaItens) {
        this.listaItens = listaItens;
    }

    public Instant getDataGeracao() {
        return dataGeracao;
    }

    public Orcamento dataGeracao(Instant dataGeracao) {
        this.dataGeracao = dataGeracao;
        return this;
    }

    public void setDataGeracao(Instant dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    public Instant getDataVencimento() {
        return dataVencimento;
    }

    public Orcamento dataVencimento(Instant dataVencimento) {
        this.dataVencimento = dataVencimento;
        return this;
    }

    public void setDataVencimento(Instant dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Long getTotal() {
        return total;
    }

    public Orcamento total(Long total) {
        this.total = total;
        return this;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getDesconto() {
        return desconto;
    }

    public Orcamento desconto(Long desconto) {
        this.desconto = desconto;
        return this;
    }

    public void setDesconto(Long desconto) {
        this.desconto = desconto;
    }

    public Long getArea() {
        return area;
    }

    public Orcamento area(Long area) {
        this.area = area;
        return this;
    }

    public void setArea(Long area) {
        this.area = area;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Orcamento orcamento = (Orcamento) o;
        if (orcamento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orcamento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Orcamento{" +
            "id=" + getId() +
            ", codigoTransacao=" + getCodigoTransacao() +
            ", nome='" + getNome() + "'" +
            ", email='" + getEmail() + "'" +
            ", telefone='" + getTelefone() + "'" +
            ", cpf='" + getCpf() + "'" +
            ", link='" + getLink() + "'" +
            ", listaItens='" + getListaItens() + "'" +
            ", dataGeracao='" + getDataGeracao() + "'" +
            ", dataVencimento='" + getDataVencimento() + "'" +
            ", total=" + getTotal() +
            ", desconto=" + getDesconto() +
            ", area=" + getArea() +
            "}";
    }
}

package br.com.siteyeahmanskateshop.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Produtos.
 */
@Entity
@Table(name = "produtos")
public class Produtos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    
    @Lob
    @Column(name = "foto_0", nullable = false)
    private byte[] foto0;

    @Column(name = "foto_0_content_type", nullable = false)
    private String foto0ContentType;

    
    @Lob
    @Column(name = "foto_1")
    private byte[] foto1;

    @Column(name = "foto_1_content_type")
    private String foto1ContentType;

    
    @Lob
    @Column(name = "foto_2")
    private byte[] foto2;

    @Column(name = "foto_2_content_type")
    private String foto2ContentType;

    
    @Lob
    @Column(name = "foto_3")
    private byte[] foto3;

    @Column(name = "foto_3_content_type")
    private String foto3ContentType;

    
    @Lob
    @Column(name = "foto_4")
    private byte[] foto4;

    @Column(name = "foto_4_content_type")
    private String foto4ContentType;

    @NotNull
    @Column(name = "valor_venda", nullable = false)
    private Long valorVenda;

    @NotNull
    @Column(name = "valor_custo", nullable = false)
    private Long valorCusto;

    @NotNull
    @Column(name = "quantidade", nullable = false)
    private Long quantidade;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Column(name = "promocao")
    private Long promocao;

    @Column(name = "destaque")
    private Boolean destaque;

    @OneToOne
    @JoinColumn(unique = true)
    private Categoria categoria;

    @OneToOne
    @JoinColumn(unique = true)
    private Colecao colecao;

    @ManyToMany(mappedBy = "produtos")
    @JsonIgnore
    private Set<Venda> vendas = new HashSet<>();

    @ManyToMany(mappedBy = "produtos")
    @JsonIgnore
    private Set<EntradaCaixa> entradaCaixas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getFoto0() {
        return foto0;
    }

    public Produtos foto0(byte[] foto0) {
        this.foto0 = foto0;
        return this;
    }

    public void setFoto0(byte[] foto0) {
        this.foto0 = foto0;
    }

    public String getFoto0ContentType() {
        return foto0ContentType;
    }

    public Produtos foto0ContentType(String foto0ContentType) {
        this.foto0ContentType = foto0ContentType;
        return this;
    }

    public void setFoto0ContentType(String foto0ContentType) {
        this.foto0ContentType = foto0ContentType;
    }

    public byte[] getFoto1() {
        return foto1;
    }

    public Produtos foto1(byte[] foto1) {
        this.foto1 = foto1;
        return this;
    }

    public void setFoto1(byte[] foto1) {
        this.foto1 = foto1;
    }

    public String getFoto1ContentType() {
        return foto1ContentType;
    }

    public Produtos foto1ContentType(String foto1ContentType) {
        this.foto1ContentType = foto1ContentType;
        return this;
    }

    public void setFoto1ContentType(String foto1ContentType) {
        this.foto1ContentType = foto1ContentType;
    }

    public byte[] getFoto2() {
        return foto2;
    }

    public Produtos foto2(byte[] foto2) {
        this.foto2 = foto2;
        return this;
    }

    public void setFoto2(byte[] foto2) {
        this.foto2 = foto2;
    }

    public String getFoto2ContentType() {
        return foto2ContentType;
    }

    public Produtos foto2ContentType(String foto2ContentType) {
        this.foto2ContentType = foto2ContentType;
        return this;
    }

    public void setFoto2ContentType(String foto2ContentType) {
        this.foto2ContentType = foto2ContentType;
    }

    public byte[] getFoto3() {
        return foto3;
    }

    public Produtos foto3(byte[] foto3) {
        this.foto3 = foto3;
        return this;
    }

    public void setFoto3(byte[] foto3) {
        this.foto3 = foto3;
    }

    public String getFoto3ContentType() {
        return foto3ContentType;
    }

    public Produtos foto3ContentType(String foto3ContentType) {
        this.foto3ContentType = foto3ContentType;
        return this;
    }

    public void setFoto3ContentType(String foto3ContentType) {
        this.foto3ContentType = foto3ContentType;
    }

    public byte[] getFoto4() {
        return foto4;
    }

    public Produtos foto4(byte[] foto4) {
        this.foto4 = foto4;
        return this;
    }

    public void setFoto4(byte[] foto4) {
        this.foto4 = foto4;
    }

    public String getFoto4ContentType() {
        return foto4ContentType;
    }

    public Produtos foto4ContentType(String foto4ContentType) {
        this.foto4ContentType = foto4ContentType;
        return this;
    }

    public void setFoto4ContentType(String foto4ContentType) {
        this.foto4ContentType = foto4ContentType;
    }

    public Long getValorVenda() {
        return valorVenda;
    }

    public Produtos valorVenda(Long valorVenda) {
        this.valorVenda = valorVenda;
        return this;
    }

    public void setValorVenda(Long valorVenda) {
        this.valorVenda = valorVenda;
    }

    public Long getValorCusto() {
        return valorCusto;
    }

    public Produtos valorCusto(Long valorCusto) {
        this.valorCusto = valorCusto;
        return this;
    }

    public void setValorCusto(Long valorCusto) {
        this.valorCusto = valorCusto;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public Produtos quantidade(Long quantidade) {
        this.quantidade = quantidade;
        return this;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public String getNome() {
        return nome;
    }

    public Produtos nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public Produtos descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getPromocao() {
        return promocao;
    }

    public Produtos promocao(Long promocao) {
        this.promocao = promocao;
        return this;
    }

    public void setPromocao(Long promocao) {
        this.promocao = promocao;
    }

    public Boolean isDestaque() {
        return destaque;
    }

    public Produtos destaque(Boolean destaque) {
        this.destaque = destaque;
        return this;
    }

    public void setDestaque(Boolean destaque) {
        this.destaque = destaque;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public Produtos categoria(Categoria categoria) {
        this.categoria = categoria;
        return this;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Colecao getColecao() {
        return colecao;
    }

    public Produtos colecao(Colecao colecao) {
        this.colecao = colecao;
        return this;
    }

    public void setColecao(Colecao colecao) {
        this.colecao = colecao;
    }

    public Set<Venda> getVendas() {
        return vendas;
    }

    public Produtos vendas(Set<Venda> vendas) {
        this.vendas = vendas;
        return this;
    }

    public Produtos addVenda(Venda venda) {
        this.vendas.add(venda);
        venda.getProdutos().add(this);
        return this;
    }

    public Produtos removeVenda(Venda venda) {
        this.vendas.remove(venda);
        venda.getProdutos().remove(this);
        return this;
    }

    public void setVendas(Set<Venda> vendas) {
        this.vendas = vendas;
    }

    public Set<EntradaCaixa> getEntradaCaixas() {
        return entradaCaixas;
    }

    public Produtos entradaCaixas(Set<EntradaCaixa> entradaCaixas) {
        this.entradaCaixas = entradaCaixas;
        return this;
    }

    public Produtos addEntradaCaixa(EntradaCaixa entradaCaixa) {
        this.entradaCaixas.add(entradaCaixa);
        entradaCaixa.getProdutos().add(this);
        return this;
    }

    public Produtos removeEntradaCaixa(EntradaCaixa entradaCaixa) {
        this.entradaCaixas.remove(entradaCaixa);
        entradaCaixa.getProdutos().remove(this);
        return this;
    }

    public void setEntradaCaixas(Set<EntradaCaixa> entradaCaixas) {
        this.entradaCaixas = entradaCaixas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Produtos produtos = (Produtos) o;
        if (produtos.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), produtos.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Produtos{" +
            "id=" + getId() +
            ", foto0='" + getFoto0() + "'" +
            ", foto0ContentType='" + getFoto0ContentType() + "'" +
            ", foto1='" + getFoto1() + "'" +
            ", foto1ContentType='" + getFoto1ContentType() + "'" +
            ", foto2='" + getFoto2() + "'" +
            ", foto2ContentType='" + getFoto2ContentType() + "'" +
            ", foto3='" + getFoto3() + "'" +
            ", foto3ContentType='" + getFoto3ContentType() + "'" +
            ", foto4='" + getFoto4() + "'" +
            ", foto4ContentType='" + getFoto4ContentType() + "'" +
            ", valorVenda=" + getValorVenda() +
            ", valorCusto=" + getValorCusto() +
            ", quantidade=" + getQuantidade() +
            ", nome='" + getNome() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", promocao=" + getPromocao() +
            ", destaque='" + isDestaque() + "'" +
            "}";
    }
}

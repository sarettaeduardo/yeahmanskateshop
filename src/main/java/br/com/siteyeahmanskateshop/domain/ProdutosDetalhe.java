package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;
import java.math.BigInteger;



public class ProdutosDetalhe implements Serializable {



    private BigInteger id;
    private BigInteger valorVenda;
    private BigInteger quantidade;
    private String nome;
    private String descricao;
    private Boolean destaque;
    private String nomeColecao;
    private String descricaoColecao;
    private String nomeCategoria;
    private String descricaoCategoria;
    private BigInteger promocao;

    public ProdutosDetalhe(BigInteger id, BigInteger valorVenda, BigInteger quantidade, String nome, String descricao, Boolean destaque, String nomeColecao,
                           String descricaoColecao, String nomeCategoria, String descricaoCategoria, BigInteger promocao) {
        this.id = id;
        this.valorVenda = valorVenda;
        this.quantidade = quantidade;
        this.nome = nome;
        this.descricao = descricao;
        this.destaque = destaque;
        this.nomeColecao = nomeColecao;
        this.descricaoColecao = descricaoColecao;
        this.nomeCategoria = nomeCategoria;
        this.descricaoCategoria = descricaoCategoria;
        this.promocao = promocao;
    }
}

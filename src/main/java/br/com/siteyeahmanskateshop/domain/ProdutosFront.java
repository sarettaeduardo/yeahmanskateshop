package br.com.siteyeahmanskateshop.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

public class ProdutosFront implements Serializable {

   /*
            const dados3 = {
            id: 13,
            nome: 'Prancha Sant',
            valor: 225,
            quantidade: 1,
            imagem: 'a'
          };
          */
   
    private static final long serialVersionUID = 1L;
    private long id;
    private String nome;
    private long valor;
    private long quantidade;
    private byte[] imagem;
    

    public ProdutosFront(){
        //setId(id);
        //setImagem(imagem);
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the valor
     */
    public long getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(long valor) {
        this.valor = valor;
    }

    /**
     * @return the imagem
     */
    public byte[] getImagem() {
        return imagem;
    }

    /**
     * @param imagem the imagem to set
     */
    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    /**
     * @return the quantidade
     */
    public long getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }



}

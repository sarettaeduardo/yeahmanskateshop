package br.com.siteyeahmanskateshop.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Venda.
 */
@Entity
@Table(name = "venda")
public class Venda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @NotNull
    @Column(name = "codigo_transacao", nullable = false)
    private String codigoTransacao;

    @Column(name = "jhi_link")
    private String link;

    @Column(name = "lista_itens")
    private String listaItens;

    @Column(name = "data_geracao")
    private Instant dataGeracao;

    @Column(name = "data_vencimento")
    private Instant dataVencimento;

    @Column(name = "total")
    private Long total;

    @Column(name = "desconto")
    private Long desconto;

    @OneToOne
    @JoinColumn(unique = true)
    private Cliente cliente;

    @OneToOne
    @JoinColumn(unique = true)
    private EnderecoEntrega endereco;

    @ManyToMany
    @JoinTable(name = "venda_produtos",
               joinColumns = @JoinColumn(name = "vendas_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "produtos_id", referencedColumnName = "id"))
    private Set<Produtos> produtos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public Venda descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigoTransacao() {
        return codigoTransacao;
    }

    public Venda codigoTransacao(String codigoTransacao) {
        this.codigoTransacao = codigoTransacao;
        return this;
    }

    public void setCodigoTransacao(String codigoTransacao) {
        this.codigoTransacao = codigoTransacao;
    }

    public String getLink() {
        return link;
    }

    public Venda link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getListaItens() {
        return listaItens;
    }

    public Venda listaItens(String listaItens) {
        this.listaItens = listaItens;
        return this;
    }

    public void setListaItens(String listaItens) {
        this.listaItens = listaItens;
    }

    public Instant getDataGeracao() {
        return dataGeracao;
    }

    public Venda dataGeracao(Instant dataGeracao) {
        this.dataGeracao = dataGeracao;
        return this;
    }

    public void setDataGeracao(Instant dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    public Instant getDataVencimento() {
        return dataVencimento;
    }

    public Venda dataVencimento(Instant dataVencimento) {
        this.dataVencimento = dataVencimento;
        return this;
    }

    public void setDataVencimento(Instant dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Long getTotal() {
        return total;
    }

    public Venda total(Long total) {
        this.total = total;
        return this;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getDesconto() {
        return desconto;
    }

    public Venda desconto(Long desconto) {
        this.desconto = desconto;
        return this;
    }

    public void setDesconto(Long desconto) {
        this.desconto = desconto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Venda cliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public EnderecoEntrega getEndereco() {
        return endereco;
    }

    public Venda endereco(EnderecoEntrega enderecoEntrega) {
        this.endereco = enderecoEntrega;
        return this;
    }

    public void setEndereco(EnderecoEntrega enderecoEntrega) {
        this.endereco = enderecoEntrega;
    }

    public Set<Produtos> getProdutos() {
        return produtos;
    }

    public Venda produtos(Set<Produtos> produtos) {
        this.produtos = produtos;
        return this;
    }

    public Venda addProdutos(Produtos produtos) {
        this.produtos.add(produtos);
        produtos.getVendas().add(this);
        return this;
    }

    public Venda removeProdutos(Produtos produtos) {
        this.produtos.remove(produtos);
        produtos.getVendas().remove(this);
        return this;
    }

    public void setProdutos(Set<Produtos> produtos) {
        this.produtos = produtos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Venda venda = (Venda) o;
        if (venda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), venda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Venda{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            ", codigoTransacao='" + getCodigoTransacao() + "'" +
            ", link='" + getLink() + "'" +
            ", listaItens='" + getListaItens() + "'" +
            ", dataGeracao='" + getDataGeracao() + "'" +
            ", dataVencimento='" + getDataVencimento() + "'" +
            ", total=" + getTotal() +
            ", desconto=" + getDesconto() +
            "}";
    }
}

package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.Colecao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Colecao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ColecaoRepository extends JpaRepository<Colecao, Long> {

}

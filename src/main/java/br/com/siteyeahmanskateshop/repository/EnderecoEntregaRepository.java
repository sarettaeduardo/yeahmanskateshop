package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.EnderecoEntrega;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EnderecoEntrega entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EnderecoEntregaRepository extends JpaRepository<EnderecoEntrega, Long> {

}

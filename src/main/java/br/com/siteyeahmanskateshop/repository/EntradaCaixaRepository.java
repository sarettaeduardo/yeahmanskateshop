package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.EntradaCaixa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the EntradaCaixa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EntradaCaixaRepository extends JpaRepository<EntradaCaixa, Long> {

    @Query(value = "select distinct entrada_caixa from EntradaCaixa entrada_caixa left join fetch entrada_caixa.produtos",
        countQuery = "select count(distinct entrada_caixa) from EntradaCaixa entrada_caixa")
    Page<EntradaCaixa> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct entrada_caixa from EntradaCaixa entrada_caixa left join fetch entrada_caixa.produtos")
    List<EntradaCaixa> findAllWithEagerRelationships();

    @Query("select entrada_caixa from EntradaCaixa entrada_caixa left join fetch entrada_caixa.produtos where entrada_caixa.id =:id")
    Optional<EntradaCaixa> findOneWithEagerRelationships(@Param("id") Long id);

}

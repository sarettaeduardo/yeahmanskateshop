package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.Hser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Hser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HserRepository extends JpaRepository<Hser, Long> {

}

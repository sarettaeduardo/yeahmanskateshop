package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.Produtos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Produtos entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProdutosRepository extends JpaRepository<Produtos, Long> {

   /* @Query(value = "SELECT p.id, c.nome as colName , c.descricao as colDesc, a.nome as catName, a.descricao as catDesc, p.promocao, p.valor_venda, p.quantidade, " +
        "p.nome as prodName, p.descricao as prodDesc, p.destaque" +
        " FROM produtos p, colecao c, categoria a WHERE p.id = ?1  and a.id = categoria_id  and c.id = colecao_id", nativeQuery = true)
    String findByIDProdNoFoto(Long id); */
}


package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.SaidaCaixa;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SaidaCaixa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaidaCaixaRepository extends JpaRepository<SaidaCaixa, Long> {

}

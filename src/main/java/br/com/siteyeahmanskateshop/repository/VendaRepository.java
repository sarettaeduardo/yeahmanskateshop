package br.com.siteyeahmanskateshop.repository;

import br.com.siteyeahmanskateshop.domain.Venda;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Venda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VendaRepository extends JpaRepository<Venda, Long> {

    @Query(value = "select distinct venda from Venda venda left join fetch venda.produtos",
        countQuery = "select count(distinct venda) from Venda venda")
    Page<Venda> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct venda from Venda venda left join fetch venda.produtos")
    List<Venda> findAllWithEagerRelationships();

    @Query("select venda from Venda venda left join fetch venda.produtos where venda.id =:id")
    Optional<Venda> findOneWithEagerRelationships(@Param("id") Long id);

}

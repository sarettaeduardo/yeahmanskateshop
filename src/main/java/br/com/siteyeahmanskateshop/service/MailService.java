package br.com.siteyeahmanskateshop.service;

import br.com.siteyeahmanskateshop.domain.CartaoCredito;
import br.com.siteyeahmanskateshop.domain.ContatoFooter;
import br.com.siteyeahmanskateshop.domain.Orcamento;
import br.com.siteyeahmanskateshop.domain.User;

import io.github.jhipster.config.JHipsterProperties;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Locale;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender, MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }

    @Async
    public void sendCreationOrcamento(Orcamento orcamento) {
        log.debug("Sending creation orcamento by email to '{}'", orcamento.getEmail());
        //sendEmailFromTemplateOrcamento(orcamento, "mail/activationEmail", "Email de Orcamento");
        sendEmailContato( orcamento.getEmail(), "orcamento@lucianatrevisol.com.br" , "Orçamento de " + orcamento.getNome() ,
            "Olá "+orcamento.getNome() +
            ".\n\nCódigo de Orçamento "+ orcamento.getCodigoTransacao()+
            "\n\nRecebemos seu pedido de Orçamento, para elaboração de projeto civil com " + orcamento.getArea() + "m² de área." +
            "\nNossa equipe irá entrar em contato o mais rápido possível." +
            "\n\nSegue o link para visualizar a segunda via do seu orçamento:\nwww.lucianatrevisol.com.br/api/downloadPDF?in=" + orcamento.getLink() +
            "\n\nAgradecemos a preferência.\n\nAtt.\nLucianaTrevisol.com.br\n(51) 980110073\nGuaporé - RS" , false, false);

        sendEmailContato( "dudusaretta@gmail.com", "orcamento@lucianatrevisol.com.br" , "Orcamento cod." + orcamento.getCodigoTransacao() ,

            "Oi Lu, \naqui é um robo do Dudu< *-* >. Ebaaa!! Temos um novo pedido de orçamento <o/ \nSegue os dados do contato que acabou de realizar um orçamento online" +
            "\nNOME: " + orcamento.getNome() +
            "\nTELEFONE: " + orcamento.getTelefone() +
            "\nCPF: " + orcamento.getCpf() +
            "\nValor TOTAL: " + orcamento.getTotal() +
            "\nValor Descondo do site: " + orcamento.getDesconto() +
            "\n\n---- Mensagem Enviada para: "+ orcamento.getEmail() +" ----" +
            "\n\nOlá "+orcamento.getNome() +
            "\n\nCódigo de Orçamento "+ orcamento.getCodigoTransacao()+
            "\n\nRecebemos seu pedido de Orcamento, para elaboração de projeto civil com " + orcamento.getArea() + "m² de área." +
            "\nNossa equipe irá entrar em contato o mais rápido possível." +
            "\n\nSegue o link para visualizar a segunda via do seu orçamento:\nwww.lucianatrevisol.com.br/api/downloadPDF?in=" + orcamento.getLink() +
            "\n\nAgradecemos a preferência.\n\nAtt.\nLucianaTrevisol.com.br\n(51) 980110073\nGuaporé - RS" , false, false);
    }

    @Async
    public void sendEmailByBuyProduct(CartaoCredito card , JSONArray itemLista) {
        sendEmailByBuyProduct(card , itemLista , "");
    }

    @Async
    public void sendEmailByBuyProduct(CartaoCredito card , JSONArray itemLista , String linkBoleto) {
        log.debug("Sending E-Mail bu Buy Product to '{}'", card.getCliente().getEmail() );

        String mensagemCliente = "Olá " + card.getCliente().getNome() + " " + card.getCliente().getSobrenome() + "," +
            ".\n\nCódigo do seu Pedido "+ card.getId_transacao() +
            "\n\n\nRecebemos seu pedido de Compra, estamos separando a(as) unidade(s) e logo voltamos a entrar em contato." +
            "\n\nSegue a lista de itens de sua compra: " +
            "\n\n" + percorreListaProdutos(itemLista) +
            "\n\n" + mensagemLinkBoleto(linkBoleto) +
            "\n\nNossa equipe enviará o código de rastreio do seu produto assim que for despachado, para acompanhar a entrega." +
            "\n\n\nAgradecemos a preferência.\n\n\nAtt.\n\nYeahManSkateShop.com.br\n\n(48) 99164-7223 \n\nFlorianópolis - SC";
        String mensagemVendedor = "Olá Equipe YeahMan Skate Shop, " +
            "\n\nTemos uma nova compra online <o/ " +
            "\n\nSegue os dados do contato que acabou de realizar uma compra online" +
            "\n\nNOME: " + card.getCliente().getNome() + " " + card.getCliente().getSobrenome() +
            "\n\nTELEFONE: " + card.getCliente().getTelefone() +
            "\n\nCPF: " + card.getCliente().getCpf() +
            "\n\nEMAIL: " + card.getCliente().getEmail() +
            "\n\nEndereço - Rua: " + card.getEndereco().getRua() +
            "\n\nEndereço - Numero: " + card.getEndereco().getNumero() +
            "\n\nEndereço - Bairro: " + card.getEndereco().getBairro() +
            "\n\nEndereço - Cep: " + card.getEndereco().getCep() +
            "\n\nEndereço - Cidade: " + card.getEndereco().getCidade() +
            "\n\nEndereço - UF: " + card.getEndereco().getUf() +
            "\n\nEndereço - Complemento: " + card.getEndereco().getComplemento() +
            "\n\n\n---- Mensagem Enviada para: "+ card.getCliente().getEmail() +" ----\n\n";

        sendEmailContato( card.getCliente().getEmail(), "compra@yeahmanskateshop.com.br" , "Compra n " + card.getId_transacao() ,
            mensagemCliente , false, false);

        sendEmailContato( "dudusaretta@gmail.com", "compra.balanceboard@yeahmanskateshop.com.br" , "Compra n " + card.getId_transacao() ,
            mensagemVendedor + mensagemCliente , false, false);

       // sendEmailContato( "loja@yeahmanskateshop.com.br", "compra.balanceboard@yeahmanskateshop.com.br" , "Compra n " + card.getId_transacao() ,
       //     mensagemVendedor + mensagemCliente , false, false);
    }

    private String percorreListaProdutos(JSONArray list) {
        String resp = "";
        for(int i = 0; i < list.length() ; i++){
            JSONObject data = list.getJSONObject(i);
            String string = data.getLong("value")+"";
            StringBuilder stringBuilder = new StringBuilder(string);
            stringBuilder.insert(string.length() - 2, ',');
            resp += "\n\n [" + i + "] " + data.getString("name") + " - R$ " + stringBuilder.toString();
        }
        return resp;
    }

    private String mensagemLinkBoleto(String link){
        String resp = "";
        if (!link.equals("")) {
            resp = "\n\n========================================================================" +
                    "\n\nSegue o Link para Visualisar seu Boleto:" +
                    "\n\n" + link +
                    "\n\n========================================================================";
        }
        return resp;
    }

    @Async
    public void sendEmailFromTemplateOrcamento(Orcamento orcamento, String templateName, String titleKey) {
        //Locale locale = Locale.forLanguageTag("pt-br");
        //Context context = new Context(locale);
        //context.setVariable(USER, user);
        //context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        //String content = templateEngine.process(templateName, context);
        //String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmailContato(orcamento.getEmail(), "orcamento@lucianatrevisol.com.br" , "TITULO", "conteudoS DO CARAIO", false, false);
    }

    @Async
    public void sendContatoFooter(ContatoFooter contato){
        String mensagemCliente = "Olá " + contato.getNome() + ","
        + "\n\n\nObrigado por entrar em contato, teremos o prazer em atende-lo."
        + "\n\nO mais rápido possível nossa equipe entrará em contato com você através do seu e-mail (" + contato.getEmail() + ")"
        + "\n\n\nSua mensagem: " + contato.getMensagem() + "."
        + "\n\n\nAgradecemos a preferência.\n\n\nAtt.\n\nyeahmanskateshop.com.br\n\n(48) 99164-7223 \n\nFlorianópolis - SC";

        String mensagemEngenheiro = "Olá Cristiano,"
        + "\n\n\nEntraram em contato através do site."
        + "\n\nSegue os dados do contato: "
        + "\n\nNome: " + contato.getNome()
        + "\n\nEmail: " + contato.getEmail()
        + "\n\nMensagem: " + contato.getMensagem()

        + "\n\n\nSegue mensagem enviada para ele: \n" + mensagemCliente;

        sendEmailContato( contato.getEmail(), "contato.loja@yeahmanskateshop.com.br" , "Contato YeahMan Skate Shop" , mensagemCliente , false, false );
        sendEmailContato( "dudusaretta@gmail.com", "contato.loja@yeahmanskateshop.com.br" , "Contato por yeahmanskateshop.com.br" , mensagemEngenheiro , false, false );
        //sendEmailContato( "loja@yeahmanskateshop.com.br", "contato.loja@yeahmanskateshop.com.br" , "Contato por yeahmanskateshop.com.br" , mensagemEngenheiro , false, false );
    }

    @Async
    public void sendEmailContato(String to, String from, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(from);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

}

package br.com.siteyeahmanskateshop.service;

import br.com.siteyeahmanskateshop.domain.Portfolio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Portfolio.
 */
public interface PortfolioService {

    /**
     * Save a portfolio.
     *
     * @param portfolio the entity to save
     * @return the persisted entity
     */
    Portfolio save(Portfolio portfolio);

    /**
     * Get all the portfolios.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Portfolio> findAll(Pageable pageable);


    /**
     * Get the "id" portfolio.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Portfolio> findOne(Long id);

    /**
     * Delete the "id" portfolio.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}

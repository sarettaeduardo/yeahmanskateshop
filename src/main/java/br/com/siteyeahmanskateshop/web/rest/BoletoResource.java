package br.com.siteyeahmanskateshop.web.rest;

import br.com.gerencianet.gnsdk.Gerencianet;
import br.com.gerencianet.gnsdk.exceptions.GerencianetException;
import br.com.siteyeahmanskateshop.domain.BoletoOrcamento;
import br.com.siteyeahmanskateshop.domain.ContatoFooter;
import br.com.siteyeahmanskateshop.domain.Orcamento;
import br.com.siteyeahmanskateshop.repository.OrcamentoRepository;
import br.com.siteyeahmanskateshop.repository.UserRepository;
import br.com.siteyeahmanskateshop.service.MailService;
import br.com.siteyeahmanskateshop.service.UserService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * REST controller for managing Transacao.
 */
@RestController
@RequestMapping("/api")
public class BoletoResource {

    @Autowired
    private OrcamentoRepository orcamentoRepository;

    private final Logger log = LoggerFactory.getLogger(BoletoResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;

    public BoletoResource(UserRepository userRepository, UserService userService, MailService mailService) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }

    @PostMapping("/contatoSendFooter")
    public ResponseEntity<String> contatoSendFooter(@Valid @RequestBody ContatoFooter contatoFooter) throws URISyntaxException {
        System.out.println("==============Contato=============");
        System.out.println(contatoFooter.toString());
        System.out.println("==================================");
        
        mailService.sendContatoFooter(contatoFooter);

        return ResponseEntity.ok().body("OK");
    }


    @PostMapping("/geraBoleto")
    public ResponseEntity<String> geraBoletoOrcamento(@Valid @RequestBody BoletoOrcamento boleto) throws URISyntaxException {

        // == Monta Objeto do Boleto ==
        Instant now = Instant.now();
        Instant nowPlus15Days = now.plus(15, ChronoUnit.DAYS);
        Instant nowPlusFiveDays = now.plus(5, ChronoUnit.DAYS);
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
                .withZone(ZoneId.systemDefault());
        String dataPLus15 = DATE_TIME_FORMATTER.format(nowPlus15Days);
        String dataPLusFive = DATE_TIME_FORMATTER.format(nowPlusFiveDays);
        boleto.setDataGeracao(now);
        boleto.setDataVencimento(nowPlus15Days);
        // boleto.setMensagem("Não aceitar após a data de vencimento. \n Conceder
        // Desconto de " + boleto.getDesconto() + "% sobre o valor Total do Boleto.");

        System.out.println("==================================");
        System.out.println(boleto.toString());
        System.out.println("==================================");

        // === Monta JSON nova Transacao ===
        JSONObject bodyNewTransaction = new JSONObject();
        JSONArray itemLista = new JSONArray();

        if (boleto.getDesconto() == 99) {
            JSONObject itemX = new JSONObject();
            itemX.put("name", "Orcamento para projeto de " + boleto.getArea() + "m², inclui: Projeto Arquitetônico, Estrutural, Elétrico, hidrossanitário, Interiores 3D.");
            itemX.put("amount", 1 );
            itemX.put("value", 1000000);
            itemLista.put(itemX);
            boleto.setDesconto(boleto.getTotal() < 11000 ? 1200l : 1l);
        } else {
            boleto.setDesconto(boleto.getDesconto() > 13 ? 1000 : boleto.getDesconto() * 100);

            Iterator it = boleto.getListaItens().entrySet().iterator();
            while (it.hasNext()) {

                Map.Entry listMap = (Map.Entry) it.next();

                if (listMap.getValue().equals("true")) {

                    JSONObject itemX = new JSONObject();
                    String itemNome = "";
                    int valorItem = 0;

                    if (listMap.getKey().equals("item0")) {
                        itemNome = "Desmembramento e Unificação de Lote";
                        valorItem = 400;
                    }

                    else if (listMap.getKey().equals("item1")) {
                        itemNome = "Projeto Arquitetônico";
                        valorItem = 1500;
                    }

                    else if (listMap.getKey().equals("item2")) {
                        itemNome = "Projeto Estrutural";
                        valorItem = 1500;
                    }

                    else if (listMap.getKey().equals("item3")) {
                        itemNome = "Projeto Eletrico";
                        valorItem = 500;
                    }

                    else if (listMap.getKey().equals("item4")) {
                        itemNome = "Projeto Hidrosanitario";
                        valorItem = 500;
                    }

                    else if (listMap.getKey().equals("item5")) {
                        itemNome = "Projeto Interiores 3D";
                        valorItem = 500;
                    }

                    else if (listMap.getKey().equals("item6")) {
                        itemNome = "PPCI / PSCI";
                        valorItem = 1000;
                    }

                    else if (listMap.getKey().equals("item7")) {
                        itemNome = "Acompanhamento em Obra";
                        valorItem = 1500;
                    }

                    itemX.put("name", itemNome);
                    itemX.put("amount", boleto.getArea());
                    itemX.put("value", valorItem);

                    itemLista.put(itemX);

                    System.out.println("JSON lista add: " + itemX.toString());

                }
            }
        } // end ELSE

        bodyNewTransaction.put("items", itemLista);
        
        /*** Envia email com os dados do cara  *** */
        
        Orcamento orcamento = new Orcamento();
        orcamento.setArea(boleto.getArea());
        orcamento.setCodigoTransacao(1234567L);
        orcamento.setCpf(boleto.getCpf());
        orcamento.setDataGeracao(now);
        orcamento.setDataVencimento(nowPlus15Days);
        orcamento.setDesconto(boleto.getDesconto());
        orcamento.setEmail(boleto.getEmail());
        orcamento.setLink("lucianatrevisol.com.br/api/downloadPDF?in="+"teste");
        orcamento.setListaItens(boleto.getListaItens().toString());
        orcamento.setNome(boleto.getNome());
        orcamento.setTelefone(boleto.getTelefone());
        orcamento.setTotal(boleto.getTotal());

        mailService.sendCreationOrcamento(orcamento);
        /************ENDA EMAIL ************/

        Long codigo_id = criaNovaTransacao(bodyNewTransaction);
        System.out.println("======> Clia Nova Transação");
        if (codigo_id == 0L) {
            return new ResponseEntity<>("Erro ao criar nova Transação", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        System.out.println("======> Vincula boleto a Transação");

        String link = vinculaBoletoTransacao(codigo_id, boleto.getNome(), boleto.getCpf(), boleto.getTelefone(),
                boleto.getEmail(), dataPLus15, boleto.getDesconto(), dataPLusFive);
        if (link.equals("") || link == null) {
            return new ResponseEntity<>("Erro ao gera um boleto via API", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // DownloadResource dow = new DownloadResource();

        // return dow.downloadBoletoPDF(link);

        System.out.println("=====LINK Original=============================================");
        System.out.println(link);
        System.out.println("=====LINK=============================================");

        if (link.contains("https://visualizacaosandbox.gerencianet.com.br/")) {
            link = link.substring(47, link.length());
            // link = link.replaceAll("https://visualizacaosandbox.gerencianet.com.br/",
            // "");
        } else if (link.contains("https://visualizacao.gerencianet.com.br/")) {
            link = link.substring(40, link.length());
        } else {
            return new ResponseEntity<>("Erro ao criptografar o link do boleto!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // ***Salva dados na base de dados - tabela Orcamento **
        
        //Orcamento orcamento = new Orcamento();
        //orcamento.setArea(boleto.getArea());
        orcamento.setCodigoTransacao(codigo_id);
        //orcamento.setCpf(boleto.getCpf());
        //orcamento.setDataGeracao(now);
        //orcamento.setDataVencimento(nowPlus15Days);
        //orcamento.setDesconto(boleto.getDesconto());
        //orcamento.setEmail(boleto.getEmail());
        orcamento.setLink("lucianatrevisol.com.br/api/downloadPDF?in="+link);
        //orcamento.setListaItens(boleto.getListaItens().toString());
        //orcamento.setNome(boleto.getNome());
        //orcamento.setTelefone(boleto.getTelefone());
        //orcamento.setTotal(boleto.getTotal());

        System.out.println("--------Inicia Transacao com Orcamento--------");

        OrcamentoResource orcamentoRes = new OrcamentoResource(orcamentoRepository);
        ResponseEntity<Orcamento> resp = orcamentoRes.createOrcamento(orcamento);

        if (resp.getStatusCode().equals("201")){
            System.out.println("--------Status OK , to save orcamento--------");
        } else {
            System.out.println("--------Status FAIL , to save orcamento--------");
            System.out.println("--------RETURN: " + resp.getStatusCode());
        }


        System.out.println("=====LINK ENVIADO=============================================");
        System.out.println(link);
        System.out.println("=====LINK=============================================");

        return ResponseEntity.ok().body(link);
    }

    private String vinculaBoletoTransacao(Long id_transacao, String nome, String cpf, String telefone, String email,
            String validade, Long desconto, String descontoValidate) {

        /* ********* Set credentials parameters DEVELOPER ********/
        JSONObject options = new JSONObject();
        options.put("client_id", "Client_Id_1aa0aa28b6210f694b47ae56dfef709dfa9b0c86");
        options.put("client_secret", "Client_Secret_e2d27c8e89c8fe15e718b48cd1966861bfe09d4b");
        options.put("sandbox", true);
        /* ********************************************* */

        /********* Set credentials parameters PRODUCAO ********
        JSONObject options = new JSONObject(); 
        options.put("client_id", "Client_Id_018145b7d0558917103b1abe68a0e450b4dc7cd4");
        options.put("client_secret", "Client_Secret_9890381473621e818ae59ac20b0a6ef24fdd38fe");
        options.put("sandbox", false); 
        /* ******************************************** */

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", "" + id_transacao);

        JSONObject customer = new JSONObject();
        // customer.put("name", "Luciana P. Trevisol");
        customer.put("name", nome);
        // customer.put("cpf", "01702801012");
        customer.put("cpf", cpf);
        // customer.put("phone_number", "51980110073");
        customer.put("phone_number", telefone);

        // Desconto té uma data especifica
        JSONObject conditionalDiscount = new JSONObject();
        conditionalDiscount.put("type", "percentage");
        conditionalDiscount.put("value", desconto);
        // conditionalDiscount.put("until_date", descontoValidate);

        JSONObject bankingBillet = new JSONObject();
        bankingBillet.put("expire_at", validade);
        bankingBillet.put("customer", customer);
        bankingBillet.put("discount", conditionalDiscount);
        // bankingBillet.put("message", mensagemDescritiva);

        JSONObject payment = new JSONObject();
        payment.put("banking_billet", bankingBillet);

        JSONObject body = new JSONObject();
        body.put("payment", payment);

        JSONObject data = null;

        try {
            Gerencianet gn = new Gerencianet(options);
            JSONObject response = gn.call("payCharge", params, body);
            System.out.println("--------------RETURN API-------------------");
            System.out.println(response.toString());
            data = response.getJSONObject("data");
            System.out.println(data.getString("link"));
            System.out.println("---------------------------------");

        } catch (GerencianetException e) {
            System.out.println(e.getCode());
            System.out.println(e.getError());
            System.out.println(e.getErrorDescription());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return data.getString("link");
    }

    private Long criaNovaTransacao(JSONObject body) {
        /* ********* Set credentials parameters DEVELOPER ********/
        JSONObject options = new JSONObject();
        options.put("client_id", "Client_Id_1aa0aa28b6210f694b47ae56dfef709dfa9b0c86");
        options.put("client_secret", "Client_Secret_e2d27c8e89c8fe15e718b48cd1966861bfe09d4b");
        options.put("sandbox", true);
        /* ********************************************* */

        /*
         * ********* Set credentials parameters PRODUCAO ********
         JSONObject options = new JSONObject();
         options.put("client_id", "Client_Id_018145b7d0558917103b1abe68a0e450b4dc7cd4");
         options.put("client_secret", "Client_Secret_9890381473621e818ae59ac20b0a6ef24fdd38fe");
         options.put("sandbox", false); /*
         ********************************************* */

        JSONObject data = null;
        try {
            Gerencianet gn = new Gerencianet(options);
            JSONObject response = gn.call("createCharge", new HashMap<String, String>(), body);
            System.out.println(response);
            System.out.println("--------------RETURN API-------------------");
            System.out.println(response.toString());
            data = response.getJSONObject("data");
            System.out.println(data.getLong("charge_id"));
            System.out.println("---------------------------------");
            return data.getLong("charge_id");
        } catch (GerencianetException e) {
            System.out.println(e.getCode());
            System.out.println(e.getError());
            System.out.println(e.getErrorDescription());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return 0L;
    }

}

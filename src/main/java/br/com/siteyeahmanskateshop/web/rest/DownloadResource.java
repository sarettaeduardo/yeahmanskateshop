package br.com.siteyeahmanskateshop.web.rest;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing Transacao.
 */
@RestController
@RequestMapping("/api")
public class DownloadResource {

    private final Logger log = LoggerFactory.getLogger(DownloadResource.class);

    /*
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "download/pdf")
    public ResponseEntity<InputStreamResource> downloadPDFFile() throws IOException {

        final String DEST = "./html_1.pdf";
        File file = new File(DEST);
        InputStream pdfFile = new FileInputStream(file);
        // ClassPathResource pdfFile = new ClassPathResource("html_1.pdf");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok().headers(headers).contentLength(pdfFile.read())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(pdfFile));
    }
    */

    @GetMapping("/downloadPDF")
    public ResponseEntity<Object> downloadBoletoPDF(String in) {
        //InputStreamResource body = null;
        File file = null;
        Document document = null;
        
        /* ************** DEV ***** ou ***** PROD **********************/
        boolean type = true; //dev = true . prod = false
        /********************************************** */

        try {
            //URL url = new URL("https://visualizacaosandbox.gerencianet.com.br/emissao/177040_9_FOBO0/A4XB-177040-328147-ZEDRA3");
            URL url;
            if (type) {
                // Desenvolvimento
                url = new URL("https://visualizacaosandbox.gerencianet.com.br/"+in);
            } else {
                // Produção
                url = new URL("https://visualizacao.gerencianet.com.br/"+in);
            }

            System.out.println("=====LINK RECEBIDO=============================================");
            System.out.println("https://visualizacaosandbox.gerencianet.com.br/"+in);
            System.out.println("=====LINK=============================================");

            BufferedReader htmlPagina = new BufferedReader(new InputStreamReader(url.openStream()));

            File arquivo;
            String quebraLinha = "\n", teste;

            // if(destinoFile.equals("nulled")){
            arquivo = new File("./ArquivoLocalProjeto.html");

            // }else{
            // arquivo = new File("./"+destinoFile);
            // }
            FileOutputStream fos = new FileOutputStream(arquivo);

            while ((teste = htmlPagina.readLine()) != null) {
                
                if (teste.contains("marca-dagua")) {
                    teste = teste.replaceAll("marca-dagua", "marca");
                    fos.write(teste.getBytes());
                } else if (teste.contains("Gerencianet Pagamentos -")) {
                    teste = teste.replaceAll("Gerencianet Pagamentos -", "");
                    fos.write(teste.getBytes());
                } else if (teste.contains("www.gerencianet.com.br")) {
                    teste = teste.replaceAll("www.gerencianet.com.br", "www.lucianatrevisol.com.br");
                    fos.write(teste.getBytes());
                }else if (teste.contains("<meta")) {
                    teste = teste.replaceAll(">", "/>");
                    fos.write(teste.getBytes());
                }else if (teste.contains("texto de responsabilidade do sacador")) {
                    teste = teste.replaceAll("<td", "<td style=\"border-bottom: 0px;\" ");
                    teste = teste.replaceAll("texto de responsabilidade do sacador", "Até o vencimento pagável em qualquer banco. Não receber após o vencimento");
                    fos.write(teste.getBytes());
                } else if (teste.contains("<link")){
                    //Remove as tags de CSS do arquivo original
                } else if (teste.contains("</style>")){
                    teste = "*{font-family:'open-sans';font-size:9pt}.partial-caixa-quadrada{background-color:#e2e2e2;height:35px;width:100%;padding-top:4px}.caixa{background-color:#f5f5f5;height:27px;width:100%;padding-top:2px;text-align:center}.caixa-informacoes span{font-size:11pt}.folha-boleto .texto-corte-aqui{font-size:7pt;font-weight:700;padding-left:10px;vertical-align:top;width:190mm;margin:0 auto;padding-top:15px;page-break-after:always}.dados-cliente{display:inline-block;padding-bottom:15px;text-align:right;font-size:8pt}.titulo-sessao{font-size:15px;font:bold}.demonstrativo table{width:90%}.col-100{padding-left:70px;padding-top:20px;padding-bottom:20px}thead{background-color:#c6b6c0}.borda-padrao{border:1px solid;color:#ccc;font-size:7pt}.linha-tracejada{height:13px;margin-top:-11px;text-align:center;width:100%;overflow:hidden;font-size:10px}table tr td{border-right:.1px solid;border-left:.1px solid;border-bottom:.1px solid} </style>";
                    fos.write(teste.getBytes());
                } else if (teste.contains("nome-lojista-envelope")){
                    //teste = "<img class=\"codigo-barras\" src=\"./boletoHeaderPB.jpg\"/>"+teste;
                    teste = "<img class=\"codigo-barras\" src=\"content/images/boletoHeaderPB.jpg\"/>"+teste;
                    fos.write(teste.getBytes());
                } else if (teste.contains(">Vencimento<")
                        || teste.contains(">Cedente<")
                        || teste.contains(">Agência/ Código cedente<")
                        || teste.contains(">Data documento<")
                        || teste.contains(">Nº documento<")
                        || teste.contains(">Espécie doc<")
                        || teste.contains(">Aceite<")
                        || teste.contains(">Data processamento<")
                        || teste.contains(">Nosso número<")
                        || teste.contains(">Uso do banco<")
                        || teste.contains(">Carteira<")
                        || teste.contains(">Espécie<")
                        || teste.contains(">Quantidade<")
                        || teste.contains(">Valor documento<")
                        || teste.contains(">1(=) Valor documento<")
                        || teste.contains(">3(-) Outras deduções<")
                        || teste.contains(">2(-) Desconto/Abatimentos<")
                        || teste.contains(">4(+) Mora/ Multa")
                        || teste.contains(">5(+) Outros")
                        || teste.contains(">6(=) Valor") ) {
                    teste = teste.replaceAll("<td", "<td style=\"border-bottom: 0px;\" ");
                    fos.write(teste.getBytes());
                } else if (teste.contains(">Sacado<")){
                    teste = teste.replaceAll("colspan=\"6\"", "colspan=\"6\" align=\"center\" style=\"border-bottom: 0px;\" ");
                    fos.write(teste.getBytes());
                } else if (teste.contains("colspan=\"6\"")){
                    teste = teste.replaceAll("colspan=\"6\"", "colspan=\"6\" align=\"center\"");
                    fos.write(teste.getBytes());
                }else {
                   fos.write(teste.getBytes());
                }
                fos.write(quebraLinha.getBytes());
            }
            fos.close();


            final String DEST = "./orcamento.pdf";
            //String HTML_teste = "./ArquivoLocalProjeto2.html";
            file = new File(DEST);
            file.getParentFile().mkdirs();

            // itext 5
            document = new Document();

            // step 2
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            writer.setInitialLeading(12.5f);

            // step 3
            document.open();

            // step 4

            // CSS
            CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);

            
            // HTML
            HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
            htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
            htmlContext.autoBookmark(false);

            // Pipelines
            PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
            HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
            CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

            // XML Worker
            XMLWorker worker = new XMLWorker(css, true);
            XMLParser p = new XMLParser(worker);
            p.parse(new FileInputStream(arquivo));
           
            // step 5
            document.close();

            //// ------------------------monta download direto do PDF------------------
            /* //-----------------V1 
            ClassPathResource pdfFile = new ClassPathResource(HTML);

            // File initialFile = new File("src/main/resources/sample.txt");
            InputStream targetStream = new FileInputStream(file);

            body = new InputStreamResource(targetStream);
            */

        } catch (MalformedURLException excecao) {
            excecao.printStackTrace();
            // return ResponseEntity.ok().body("FALHA!!!!!!!!!!!!!! ");
        } catch (IOException excecao) {
            excecao.printStackTrace();
            // return ResponseEntity.ok().body("FALHA!!!!!!!!!!!!!! ");
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            // return ResponseEntity.ok().body("FALHA!!!!!!!!!!!!!! ");
        }

        // return ResponseEntity.ok().body("Resposta OK!");
        // URI location = "...";
        //HttpHeaders responseHeaders = new HttpHeaders();
        // responseHeaders.setLocation(location);
        //responseHeaders.set("Content-disposition", "attachment; filename=html_1.pdf");

        //return new ResponseEntity<InputStreamResource>(body, responseHeaders, HttpStatus.OK);

        FileWriter filewriter = null;

        try{

            // InputStream buferRead = new FileInputStream(file);
            //BufferedReader buferRead = new BufferedReader(new FileReader("./html_1.pdf"));
            // StringBuilder striBuild = new StringBuilder();

           //  striBuild.append(buferRead.read());

            /*for (String linha = buferRead.readLine(); linha != null; linha = buferRead.readLine()) {
                striBuild.append(linha).append("\n");
            }*/
            // buferRead.close();

            //System.out.print(" ----------------------------- striBuild.toString() -----------------------");

            //System.out.print(striBuild.toString());

            //System.out.print(" ----------------------------- close -----------------------");

            //String filename = "./pdfv1.pdf";

            //filewriter = new FileWriter(filename);
            //filewriter.write(document.open().toString());
            //filewriter.flush();

            

            //File file2 = new File(filename);

            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            HttpHeaders headers = new HttpHeaders();
            //headers.add("content-type" , "application/pdf; charset=UTF-8");
            headers.add("Content-Disposition", String.format("filename=%s", file.getName()));
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Access-Control-Allow-Origin", "*");
            headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
            headers.add("Access-Control-Allow-Headers", "Content-Type");

            ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(file.length())
            //.contentType(MediaType.parseMediaType("application/pdf; charset=us-ascii")).body(resource);
            //us-ascii
            //.contentType(MediaType.parseMediaType("text/html; charset=ISO-8859-1")).body(resource);
            //.contentType(MediaType.parseMediaType("application/pdf; charset=UTF-8;")).body(resource);
            .contentType(MediaType.parseMediaType("application/pdf;")).body(resource);
            System.out.println("==========ARQUIVO PDF=============================");
            System.out.println(resource.toString());
            System.out.println("=======================================");
            return responseEntity;
        } catch (Exception e) {
            return new ResponseEntity<>("error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (filewriter != null)
                try {
                    filewriter.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }



    }

    @RequestMapping(value = "/trans/downloadCSV", method = RequestMethod.GET)
    public ResponseEntity<Object> downloadFileCVS() throws IOException {

        FileWriter filewriter = null;

        try {
            CSVData csv1 = new CSVData();
            csv1.setId("1");
            csv1.setName("talk2amareswaran");
            csv1.setNumber("5601");

            CSVData csv2 = new CSVData();
            csv2.setId("2");
            csv2.setName("Amareswaran");
            csv2.setNumber("8710");

            List<CSVData> csvDataList = new ArrayList<>();
            csvDataList.add(csv1);
            csvDataList.add(csv2);

            StringBuilder filecontent = new StringBuilder("ID, NAME, NUMBER\n");
            for (CSVData csv : csvDataList) {
                filecontent.append(csv.getId()).append(",").append(csv.getName()).append(",").append(csv.getNumber())
                        .append("\n");
            }

            String filename = "./ocsvdata.csv";

            filewriter = new FileWriter(filename);
            filewriter.write(filecontent.toString());
            filewriter.flush();

            File file = new File(filename);

            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", String.format("attachment; filename=%s", file.getName()));
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

            ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/txt")).body(resource);
            return responseEntity;
        } catch (Exception e) {
            return new ResponseEntity<>("error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            if (filewriter != null)
                filewriter.close();
        }
    }

    public class CSVData {

        private String id;
        private String name;
        private String number;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }
    }

}

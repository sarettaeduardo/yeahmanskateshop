package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.domain.EnderecoEntrega;
import br.com.siteyeahmanskateshop.repository.EnderecoEntregaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import br.com.siteyeahmanskateshop.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EnderecoEntrega.
 */
@RestController
@RequestMapping("/api")
public class EnderecoEntregaResource {

    private final Logger log = LoggerFactory.getLogger(EnderecoEntregaResource.class);

    private static final String ENTITY_NAME = "enderecoEntrega";

    private final EnderecoEntregaRepository enderecoEntregaRepository;

    public EnderecoEntregaResource(EnderecoEntregaRepository enderecoEntregaRepository) {
        this.enderecoEntregaRepository = enderecoEntregaRepository;
    }

    /**
     * POST  /endereco-entregas : Create a new enderecoEntrega.
     *
     * @param enderecoEntrega the enderecoEntrega to create
     * @return the ResponseEntity with status 201 (Created) and with body the new enderecoEntrega, or with status 400 (Bad Request) if the enderecoEntrega has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/endereco-entregas")
    @Timed
    public ResponseEntity<EnderecoEntrega> createEnderecoEntrega(@Valid @RequestBody EnderecoEntrega enderecoEntrega) throws URISyntaxException {
        log.debug("REST request to save EnderecoEntrega : {}", enderecoEntrega);
        if (enderecoEntrega.getId() != null) {
            throw new BadRequestAlertException("A new enderecoEntrega cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EnderecoEntrega result = enderecoEntregaRepository.save(enderecoEntrega);
        return ResponseEntity.created(new URI("/api/endereco-entregas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    public EnderecoEntrega createEnderecoEntregaLocal(EnderecoEntrega enderecoEntrega) throws URISyntaxException {
        log.debug("LOCAL request to save EnderecoEntrega : {}", enderecoEntrega);
        if (enderecoEntrega.getId() != null) {
            throw new BadRequestAlertException("A new enderecoEntrega cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EnderecoEntrega result = enderecoEntregaRepository.save(enderecoEntrega);
        return result;
        //return ResponseEntity.created(new URI("/api/endereco-entregas/" + result.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            //.body(result);
    }

    /**
     * PUT  /endereco-entregas : Updates an existing enderecoEntrega.
     *
     * @param enderecoEntrega the enderecoEntrega to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated enderecoEntrega,
     * or with status 400 (Bad Request) if the enderecoEntrega is not valid,
     * or with status 500 (Internal Server Error) if the enderecoEntrega couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/endereco-entregas")
    @Timed
    public ResponseEntity<EnderecoEntrega> updateEnderecoEntrega(@Valid @RequestBody EnderecoEntrega enderecoEntrega) throws URISyntaxException {
        log.debug("REST request to update EnderecoEntrega : {}", enderecoEntrega);
        if (enderecoEntrega.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EnderecoEntrega result = enderecoEntregaRepository.save(enderecoEntrega);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, enderecoEntrega.getId().toString()))
            .body(result);
    }


    /**
     * GET  /endereco-entregas : get all the enderecoEntregas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of enderecoEntregas in body
     */
    @GetMapping("/endereco-entregas")
    @Timed
    public ResponseEntity<List<EnderecoEntrega>> getAllEnderecoEntregas(Pageable pageable) {
        log.debug("REST request to get a page of EnderecoEntregas");
        Page<EnderecoEntrega> page = enderecoEntregaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/endereco-entregas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /endereco-entregas/:id : get the "id" enderecoEntrega.
     *
     * @param id the id of the enderecoEntrega to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the enderecoEntrega, or with status 404 (Not Found)
     */
    @GetMapping("/endereco-entregas/{id}")
    @Timed
    public ResponseEntity<EnderecoEntrega> getEnderecoEntrega(@PathVariable Long id) {
        log.debug("REST request to get EnderecoEntrega : {}", id);
        Optional<EnderecoEntrega> enderecoEntrega = enderecoEntregaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(enderecoEntrega);
    }

    /**
     * DELETE  /endereco-entregas/:id : delete the "id" enderecoEntrega.
     *
     * @param id the id of the enderecoEntrega to delete
     * @return the ResponseEntity with status 200 (OK)

    @DeleteMapping("/endereco-entregas/{id}")
    @Timed
    public ResponseEntity<Void> deleteEnderecoEntrega(@PathVariable Long id) {
        log.debug("REST request to delete EnderecoEntrega : {}", id);

        enderecoEntregaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
     */
}

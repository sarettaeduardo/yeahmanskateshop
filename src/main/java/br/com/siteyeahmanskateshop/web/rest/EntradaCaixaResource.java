package br.com.siteyeahmanskateshop.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.siteyeahmanskateshop.domain.EntradaCaixa;
import br.com.siteyeahmanskateshop.repository.EntradaCaixaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EntradaCaixa.
 */
@RestController
@RequestMapping("/api")
public class EntradaCaixaResource {

    private final Logger log = LoggerFactory.getLogger(EntradaCaixaResource.class);

    private static final String ENTITY_NAME = "entradaCaixa";

    private final EntradaCaixaRepository entradaCaixaRepository;

    public EntradaCaixaResource(EntradaCaixaRepository entradaCaixaRepository) {
        this.entradaCaixaRepository = entradaCaixaRepository;
    }

    /**
     * POST  /entrada-caixas : Create a new entradaCaixa.
     *
     * @param entradaCaixa the entradaCaixa to create
     * @return the ResponseEntity with status 201 (Created) and with body the new entradaCaixa, or with status 400 (Bad Request) if the entradaCaixa has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/entrada-caixas")
    @Timed
    public ResponseEntity<EntradaCaixa> createEntradaCaixa(@Valid @RequestBody EntradaCaixa entradaCaixa) throws URISyntaxException {
        log.debug("REST request to save EntradaCaixa : {}", entradaCaixa);
        if (entradaCaixa.getId() != null) {
            throw new BadRequestAlertException("A new entradaCaixa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EntradaCaixa result = entradaCaixaRepository.save(entradaCaixa);
        return ResponseEntity.created(new URI("/api/entrada-caixas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /entrada-caixas : Updates an existing entradaCaixa.
     *
     * @param entradaCaixa the entradaCaixa to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated entradaCaixa,
     * or with status 400 (Bad Request) if the entradaCaixa is not valid,
     * or with status 500 (Internal Server Error) if the entradaCaixa couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/entrada-caixas")
    @Timed
    public ResponseEntity<EntradaCaixa> updateEntradaCaixa(@Valid @RequestBody EntradaCaixa entradaCaixa) throws URISyntaxException {
        log.debug("REST request to update EntradaCaixa : {}", entradaCaixa);
        if (entradaCaixa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EntradaCaixa result = entradaCaixaRepository.save(entradaCaixa);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, entradaCaixa.getId().toString()))
            .body(result);
    }

    /**
     * GET  /entrada-caixas : get all the entradaCaixas.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of entradaCaixas in body
     */
    @GetMapping("/entrada-caixas")
    @Timed
    public List<EntradaCaixa> getAllEntradaCaixas(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all EntradaCaixas");
        return entradaCaixaRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /entrada-caixas/:id : get the "id" entradaCaixa.
     *
     * @param id the id of the entradaCaixa to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the entradaCaixa, or with status 404 (Not Found)
     */
    @GetMapping("/entrada-caixas/{id}")
    @Timed
    public ResponseEntity<EntradaCaixa> getEntradaCaixa(@PathVariable Long id) {
        log.debug("REST request to get EntradaCaixa : {}", id);
        Optional<EntradaCaixa> entradaCaixa = entradaCaixaRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(entradaCaixa);
    }

    /**
     * DELETE  /entrada-caixas/:id : delete the "id" entradaCaixa.
     *
     * @param id the id of the entradaCaixa to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/entrada-caixas/{id}")
    @Timed
    public ResponseEntity<Void> deleteEntradaCaixa(@PathVariable Long id) {
        log.debug("REST request to delete EntradaCaixa : {}", id);

        entradaCaixaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

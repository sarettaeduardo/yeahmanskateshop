package br.com.siteyeahmanskateshop.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.siteyeahmanskateshop.domain.Hser;
import br.com.siteyeahmanskateshop.repository.HserRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Hser.
 */
@RestController
@RequestMapping("/api")
public class HserResource {

    private final Logger log = LoggerFactory.getLogger(HserResource.class);

    private static final String ENTITY_NAME = "hser";

    private final HserRepository hserRepository;

    public HserResource(HserRepository hserRepository) {
        this.hserRepository = hserRepository;
    }

    /**
     * POST  /hsers : Create a new hser.
     *
     * @param hser the hser to create
     * @return the ResponseEntity with status 201 (Created) and with body the new hser, or with status 400 (Bad Request) if the hser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/hsers")
    @Timed
    public ResponseEntity<Hser> createHser(@RequestBody Hser hser) throws URISyntaxException {
        log.debug("REST request to save Hser : {}", hser);
        if (hser.getId() != null) {
            throw new BadRequestAlertException("A new hser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Hser result = hserRepository.save(hser);
        return ResponseEntity.created(new URI("/api/hsers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hsers : Updates an existing hser.
     *
     * @param hser the hser to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated hser,
     * or with status 400 (Bad Request) if the hser is not valid,
     * or with status 500 (Internal Server Error) if the hser couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/hsers")
    @Timed
    public ResponseEntity<Hser> updateHser(@RequestBody Hser hser) throws URISyntaxException {
        log.debug("REST request to update Hser : {}", hser);
        if (hser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Hser result = hserRepository.save(hser);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hser.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hsers : get all the hsers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of hsers in body
     */
    @GetMapping("/hsers")
    @Timed
    public List<Hser> getAllHsers() {
        log.debug("REST request to get all Hsers");
        return hserRepository.findAll();
    }

    /**
     * GET  /hsers/:id : get the "id" hser.
     *
     * @param id the id of the hser to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the hser, or with status 404 (Not Found)
     */
    @GetMapping("/hsers/{id}")
    @Timed
    public ResponseEntity<Hser> getHser(@PathVariable Long id) {
        log.debug("REST request to get Hser : {}", id);
        Optional<Hser> hser = hserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(hser);
    }

    /**
     * DELETE  /hsers/:id : delete the "id" hser.
     *
     * @param id the id of the hser to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/hsers/{id}")
    @Timed
    public ResponseEntity<Void> deleteHser(@PathVariable Long id) {
        log.debug("REST request to delete Hser : {}", id);

        hserRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.domain.Portfolio;
import br.com.siteyeahmanskateshop.service.PortfolioService;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import br.com.siteyeahmanskateshop.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Portfolio.
 */
@RestController
@RequestMapping("/api")
public class PortfolioResource {

    private final Logger log = LoggerFactory.getLogger(PortfolioResource.class);

    private static final String ENTITY_NAME = "portfolio";

    private final PortfolioService portfolioService;

    public PortfolioResource(PortfolioService portfolioService) {
        this.portfolioService = portfolioService;
    }

    /**
     * POST  /portfolios : Create a new portfolio.
     *
     * @param portfolio the portfolio to create
     * @return the ResponseEntity with status 201 (Created) and with body the new portfolio, or with status 400 (Bad Request) if the portfolio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/portfolios")
    @Timed
    public ResponseEntity<Portfolio> createPortfolio(@Valid @RequestBody Portfolio portfolio) throws URISyntaxException {
        log.debug("REST request to save Portfolio : {}", portfolio);
        if (portfolio.getId() != null) {
            throw new BadRequestAlertException("A new portfolio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Portfolio result = portfolioService.save(portfolio);
        return ResponseEntity.created(new URI("/api/portfolios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /portfolios : Updates an existing portfolio.
     *
     * @param portfolio the portfolio to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated portfolio,
     * or with status 400 (Bad Request) if the portfolio is not valid,
     * or with status 500 (Internal Server Error) if the portfolio couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/portfolios")
    @Timed
    public ResponseEntity<Portfolio> updatePortfolio(@Valid @RequestBody Portfolio portfolio) throws URISyntaxException {
        log.debug("REST request to update Portfolio : {}", portfolio);
        if (portfolio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Portfolio result = portfolioService.save(portfolio);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, portfolio.getId().toString()))
            .body(result);
    }

    /**
     * GET  /portfolios : get all the portfolios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of portfolios in body
     */
    @GetMapping("/portfolios")
    @Timed
    public ResponseEntity<List<Portfolio>> getAllPortfolios(Pageable pageable) {
        log.debug("REST request to get a page of Portfolios");
        Page<Portfolio> page = portfolioService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/portfolios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /portfolios/:id : get the "id" portfolio.
     *
     * @param id the id of the portfolio to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the portfolio, or with status 404 (Not Found)
     */
    @GetMapping("/portfolios/{id}")
    @Timed
    public ResponseEntity<Portfolio> getPortfolio(@PathVariable Long id) {
        log.debug("REST request to get Portfolio : {}", id);
        Optional<Portfolio> portfolio = portfolioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(portfolio);
    }

    /**
     * DELETE  /portfolios/:id : delete the "id" portfolio.
     *
     * @param id the id of the portfolio to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/portfolios/{id}")
    @Timed
    public ResponseEntity<Void> deletePortfolio(@PathVariable Long id) {
        log.debug("REST request to delete Portfolio : {}", id);
        portfolioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/portfoliosOpen")
    @Timed
    public ResponseEntity<List<Portfolio>> getAllPortfoliosOpen(Pageable pageable) {
        log.debug("REST request to get a page of Portfolios");
        Page<Portfolio> page = portfolioService.findAll(pageable);
        
        String array[] = new String[3];
        for (Portfolio p : page.getContent() ) {
            System.out.println("->>>>>> " + p.getLinkYoutube());
            if (p.getLinkYoutube().equals("") || p.getLinkYoutube() == null || p.getLinkYoutube().isEmpty()){
                System.out.println("LINK DO YOUTUBE VAZIO!");
            }else{
                array = p.getLinkYoutube().split("v=");
                if(array[1].indexOf('&') != -1) {
                    System.out.println("TEM MAIS COISA NO LINK");
                    System.out.println("link: " + array[1]);
                    String array2[] = new String[3];
                    array2 = array[1].split("&");
                    System.out.println("link ajustado: " + array2[0]);
                    array[1] = array2[0];

                }
                // System.out.println(array[1]);
                p.setLinkYoutube(array[1]);
            }

            // p.getFoto()
            try {
                int idimagem = Integer.parseInt(p.getFoto());
                UploadResource up = new UploadResource();
                String a = up.FileUploadString(idimagem);
                // byte[] byteA = (byte[]) a.getBody();
                System.out.println("imagem Base64 (aqui): ");
                p.setFoto(a);
            }catch (Throwable e ){
                System.out.println("Nao é Possivel Converter DADO ERRADO: " + e);
            }


        }

        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/portfoliosOpen");
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/portfolios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    
    /*

    // Não funciona, pois se carrgar em diretório externo a aplicação nao permite acessar.
    // E para criar dentro do projeto, não da, pois o pacote e um .WAR

    @PostMapping("/upload")
    @Timed
    public ResponseEntity  handleFileUpload(@RequestParam("file") MultipartFile file) {
        
        //.war!/WEB-INF/classes!/br/com/siteyeahmanskateshop/web/rest
        //URL url = getClass().getResource("../../../../../");
        //System.out.println("== getClass().getResource1(: " + url);
        //URL url1 = getClass().getResource("/content/images/inputData.txt");
        //System.out.println("== getClass().getResource2(: " + url1);
        //URL url2 = getClass().getResource("../../../../../../../");
        //System.out.println("== getClass().getResource3(: " + url2);
        //URL url3 = getClass().getResource("../../../../../../../content/images/");
        //System.out.println("== getClass().getResource4(: " + url3);
        //String path = ClassLoader.getSystemResource("").getPath();
		//System.out.println("== usando ClassLoader.getSystemResource(\"\"): " + path);
		//path = System.getProperty("user.dir");
		//System.out.println("== Usando propriedade user.dir: " + path);
        try {
            System.out.printf("File name=%s, size=%s\n", file.getOriginalFilename(),file.getSize());
            //creating a new file in some local directory
            File fileToSave = new File( "/home/dudu/Git/2/lucianatrevisol/siteyeahmanskateshop/build/www/content/images/" + file.getOriginalFilename());
            // copy file content from received file to new local file
            file.transferTo(fileToSave);
        } catch (IOException ioe) {
            //if something went bad, we need to inform client about it
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        //everything was OK, return HTTP OK status (200) to the client
        return ResponseEntity.ok().build();
    }*/
}

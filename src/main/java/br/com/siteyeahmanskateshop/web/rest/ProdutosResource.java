package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.domain.*;
import br.com.siteyeahmanskateshop.repository.ProdutosRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import br.com.siteyeahmanskateshop.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Produtos.
 */
@RestController
@RequestMapping("/api")
public class ProdutosResource {

    private final Logger log = LoggerFactory.getLogger(ProdutosResource.class);

    private static final String ENTITY_NAME = "produtos";

    private final ProdutosRepository produtosRepository;

    public ProdutosResource(ProdutosRepository produtosRepository) {
        this.produtosRepository = produtosRepository;
    }

    /**
     * POST  /produtos : Create a new produtos.
     *
     * @param produtos the produtos to create
     * @return the ResponseEntity with status 201 (Created) and with body the new produtos, or with status 400 (Bad Request) if the produtos has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/produtos")
    @Timed
    public ResponseEntity<Produtos> createProdutos(@Valid @RequestBody Produtos produtos) throws URISyntaxException {
        log.debug("REST request to save Produtos : {}", produtos);
        if (produtos.getId() != null) {
            throw new BadRequestAlertException("A new produtos cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Produtos result = produtosRepository.save(produtos);
        return ResponseEntity.created(new URI("/api/produtos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /produtos : Updates an existing produtos.
     *
     * @param produtos the produtos to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated produtos,
     * or with status 400 (Bad Request) if the produtos is not valid,
     * or with status 500 (Internal Server Error) if the produtos couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/produtos")
    @Timed
    public ResponseEntity<Produtos> updateProdutos(@Valid @RequestBody Produtos produtos) throws URISyntaxException {
        log.debug("REST request to update Produtos : {}", produtos);
        if (produtos.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Produtos result = produtosRepository.save(produtos);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, produtos.getId().toString()))
            .body(result);
    }

    /**
     * GET  /produtos : get all the produtos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of produtos in body
     */
    @GetMapping("/produtos")
    @Timed
    public ResponseEntity<List<Produtos>> getAllProdutos(Pageable pageable) {
        log.debug("REST request to get a page of Produtos");
        Page<Produtos> page = produtosRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/produtos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/produtosOpen")
    @Timed
    public ResponseEntity<List<ProdutosFront>> getAllProdutosOpen(Pageable pageable) {
        log.debug("REST request to get a page of Produtos");
        Page<Produtos> page = produtosRepository.findAll(pageable);
        List<ProdutosFront> prodFrontList = new ArrayList<>();
        for (Produtos p : page.getContent() ) {
            ProdutosFront prodFront = new ProdutosFront();
            
            prodFront.setId(p.getId());
            prodFront.setNome(p.getNome());
            prodFront.setQuantidade(p.getQuantidade());
            prodFront.setValor(p.getValorVenda());
            try (ByteArrayOutputStream bytesArrayToDB = new ByteArrayOutputStream()){
               // String headerBase64 = "data:" + p.getFoto0ContentType().toString() + ";base64,"; //default
                // byte[] headerBase64byte = headerBase64.getBytes();
               // bytesArrayToDB.write(headerBase64.getBytes());
               // bytesArrayToDB.write(p.getFoto0());
                prodFront.setImagem(p.getFoto0());
               // System.out.println("SAIDA IMAGEM:>>>>>> " + headerBase64);
               // System.out.println("SAIDA IMAGEM:>>>>>> " + bytesArrayToDB);
               // System.out.println("SAIDA IMAGEM:>>>>>> " + bytesArrayToDB.toString());
            }catch (IOException e) {
                e.printStackTrace();
                System.out.println("ERRO TO SAVE IMAGE");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }

            prodFrontList.add(prodFront);
        }
        /*
            const dados3 = {
            id: 13,
            nome: 'Prancha Sant',
            valor: 225,
            quantidade: 1,
            imagem: 'a'
          };
          */
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/produtosOpen");
        return new ResponseEntity<>(prodFrontList, headers, HttpStatus.OK);
    }


    /**
     * GET  /produtosOpen/:id_categoria : get the products by id_categoria.
     *
     * @param id_categoria the id of the categoria to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the produtos with id_categoria, or with status 404 (Not Found)
     */
    @GetMapping("/produtosOpenCategoria/{id_categoria}")
    @Timed
    public ResponseEntity<List<ProdutosFront>> getProdutosByCategoria(@PathVariable Long id_categoria) {
        log.debug("REST request to get ProdutosOpen By Categoria : {}", id_categoria);

        Produtos produto = new Produtos();
        Categoria categoria_prod = new Categoria();
        categoria_prod.setId(id_categoria);
        produto.setCategoria(categoria_prod);

        Example<Produtos> example = Example.of(produto);

        List<Produtos> produtos = produtosRepository.findAll(example); //findById(id_categoria);

        List<ProdutosFront> prodFrontL = formatProdutoByFrontEnd(produtos);

        return new ResponseEntity<>(prodFrontL, HttpStatus.OK);
    }

    /**
     * GET  /produtosOpen/:id_colecao : get the products by id_colecao.
     *
     * @param id_colecao the id of the categoria to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the produtos with id_colecao, or with status 404 (Not Found)
     */
    @GetMapping("/produtosOpenColecao/{id_colecao}")
    @Timed
    public ResponseEntity<List<ProdutosFront>> getProdutosByColecao(@PathVariable Long id_colecao) {
        log.debug("REST request to get ProdutosOpen By Colecao : {}", id_colecao);

        Produtos produto = new Produtos();
        Colecao colecao_prod = new Colecao();
        colecao_prod.setId(id_colecao);
        produto.setColecao(colecao_prod);
        Example<Produtos> example = Example.of(produto);
        List<Produtos> produtos = produtosRepository.findAll(example); //findById(id_categoria);
        List<ProdutosFront> prodFrontL = formatProdutoByFrontEnd(produtos);
        return new ResponseEntity<>(prodFrontL, HttpStatus.OK);
    }

    /**
     * GET  /produtosOpen/:id_colecao : get the products by id_colecao.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the produtos with destaque = 'true', or with status 404 (Not Found)
     */
    @GetMapping("/produtosOpenDestaque")
    @Timed
    public ResponseEntity<List<ProdutosFront>> getProdutosOpenDestaque() {
        log.debug("REST request to get ProdutosOpen By Destaque : {};");

        Produtos produto = new Produtos();
        produto.setDestaque(true);
        Example<Produtos> example = Example.of(produto);
        List<Produtos> produtos = produtosRepository.findAll(example); //findById(id_categoria);
        List<ProdutosFront> prodFrontL = formatProdutoByFrontEnd(produtos);
        return new ResponseEntity<>(prodFrontL, HttpStatus.OK);
    }


    /**
     * GET  /produtos/:id : get the "id" produtos.
     *
     * @param id the id of the produtos to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the produtos, or with status 404 (Not Found)
     */
    @GetMapping("/produtos/{id}")
    @Timed
    public ResponseEntity<Produtos> getProdutos(@PathVariable Long id) {
        log.debug("REST request to get Produtos : {}", id);
        Optional<Produtos> produtos = produtosRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(produtos);
    }



    @GetMapping("/produtosOpen/{id}")
    @Timed
    public ResponseEntity<Produtos> getProdutosOpen(@PathVariable Long id) {
        log.debug("REST request to get Produtos : {}", id);

        Optional<Produtos> produtos = produtosRepository.findById(id);

       /* String prod = produtosRepository.findByIDProdNoFoto(id);

        System.out.println(prod);

        String array[];

        array = prod.split(",");
        System.out.println(
                "id: " + array[0]
                +" colecaoNome: " + array[1]
                +" colecaoDesc: " + array[2]
                +" categNome: " + array[3]
                +" categDesc: " + array[4]
                +" promocao: " + array[5]
                +" valor_venda: " + array[6]
                +" quantidade: " + array[7]
                +" nome: " + array[8]
                +" descricao: " + array[9]
                +" destaque: " + array[10]
        );

        Optional<Produtos> produtos = Optional.of(new Produtos());
        Colecao col = new Colecao();
        col.setNome(array[1] == null || array[1].equals("") ? " " : array[1]);
        col.setDescricao(array[2] == null || array[2].equals("") ? " " : array[2]);

        Categoria cat = new Categoria();
        cat.setNome(array[3] == null || array[3].equals("") ? " " : array[3]);
        cat.setDescricao(array[4] == null || array[4].equals("") ? " " : array[4]);

        produtos.get().setColecao(col);
        produtos.get().setCategoria(cat);
        produtos.get().setId(Long.parseLong(array[0]));

        produtos.get().setPromocao(Long.parseLong(array[5].equals("null") ? "0" : array[5] ));
        produtos.get().setValorVenda(Long.parseLong(array[6]));
        produtos.get().setQuantidade(Long.parseLong(array[7]));
        produtos.get().setNome(array[8]);
        produtos.get().setDescricao(array[9]);
        produtos.get().setDestaque(Boolean.parseBoolean(array[10])); */

        produtos.get().setValorCusto(null);
        produtos.get().setPromocao(null);
        produtos.get().setFoto0(null);
        produtos.get().setFoto1(null);
        produtos.get().setFoto2(null);
        produtos.get().setFoto3(null);
        produtos.get().setFoto4(null);
        produtos.get().setFoto0ContentType(null);
        produtos.get().setFoto1ContentType(null);
        produtos.get().setFoto2ContentType(null);
        produtos.get().setFoto3ContentType(null);
        produtos.get().setFoto4ContentType(null);
        //TODO : Arrumaar essa gambiarra - Não ficou tao ruim viu
        return ResponseUtil.wrapOrNotFound(produtos);
    }

    @GetMapping("/produtosOpenFoto/{id}")
    @Timed
    public ResponseEntity<Produtos> getProdutosOpenFoto(@PathVariable Long id) {
        log.debug("REST request to get Produtos : {}", id);
        Optional<Produtos> produtos = produtosRepository.findById(id);
        produtos.get().setValorCusto(null);
        produtos.get().setPromocao(null);
        return ResponseUtil.wrapOrNotFound(produtos);
    }

    public Optional<Produtos> getProdutosById(Integer id) {
        Optional<Produtos> produtos = produtosRepository.findById(Long.parseLong(id.toString()));
        return produtos;
    }

    /**
     * DELETE  /produtos/:id : delete the "id" produtos.
     *
     * @param id the id of the produtos to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/produtos/{id}")
    @Timed
    public ResponseEntity<Void> deleteProdutos(@PathVariable Long id) {
        log.debug("REST request to delete Produtos : {}", id);

        produtosRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    private List<ProdutosFront> formatProdutoByFrontEnd(List<Produtos> prodIn) {
        List<ProdutosFront> prodFrontList = new ArrayList<>();
        for (Produtos p : prodIn ) {
            ProdutosFront prodFront = new ProdutosFront();
            prodFront.setId(p.getId());
            prodFront.setNome(p.getNome());
            prodFront.setQuantidade(p.getQuantidade());
            prodFront.setValor(p.getValorVenda());
            prodFront.setImagem(p.getFoto0());
            prodFrontList.add(prodFront);
        }
        return prodFrontList;
    }
}

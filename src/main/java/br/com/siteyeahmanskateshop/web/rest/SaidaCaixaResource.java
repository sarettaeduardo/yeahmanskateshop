package br.com.siteyeahmanskateshop.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.siteyeahmanskateshop.domain.SaidaCaixa;
import br.com.siteyeahmanskateshop.repository.SaidaCaixaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.BadRequestAlertException;
import br.com.siteyeahmanskateshop.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SaidaCaixa.
 */
@RestController
@RequestMapping("/api")
public class SaidaCaixaResource {

    private final Logger log = LoggerFactory.getLogger(SaidaCaixaResource.class);

    private static final String ENTITY_NAME = "saidaCaixa";

    private final SaidaCaixaRepository saidaCaixaRepository;

    public SaidaCaixaResource(SaidaCaixaRepository saidaCaixaRepository) {
        this.saidaCaixaRepository = saidaCaixaRepository;
    }

    /**
     * POST  /saida-caixas : Create a new saidaCaixa.
     *
     * @param saidaCaixa the saidaCaixa to create
     * @return the ResponseEntity with status 201 (Created) and with body the new saidaCaixa, or with status 400 (Bad Request) if the saidaCaixa has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/saida-caixas")
    @Timed
    public ResponseEntity<SaidaCaixa> createSaidaCaixa(@Valid @RequestBody SaidaCaixa saidaCaixa) throws URISyntaxException {
        log.debug("REST request to save SaidaCaixa : {}", saidaCaixa);
        if (saidaCaixa.getId() != null) {
            throw new BadRequestAlertException("A new saidaCaixa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SaidaCaixa result = saidaCaixaRepository.save(saidaCaixa);
        return ResponseEntity.created(new URI("/api/saida-caixas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /saida-caixas : Updates an existing saidaCaixa.
     *
     * @param saidaCaixa the saidaCaixa to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated saidaCaixa,
     * or with status 400 (Bad Request) if the saidaCaixa is not valid,
     * or with status 500 (Internal Server Error) if the saidaCaixa couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/saida-caixas")
    @Timed
    public ResponseEntity<SaidaCaixa> updateSaidaCaixa(@Valid @RequestBody SaidaCaixa saidaCaixa) throws URISyntaxException {
        log.debug("REST request to update SaidaCaixa : {}", saidaCaixa);
        if (saidaCaixa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SaidaCaixa result = saidaCaixaRepository.save(saidaCaixa);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saidaCaixa.getId().toString()))
            .body(result);
    }

    /**
     * GET  /saida-caixas : get all the saidaCaixas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of saidaCaixas in body
     */
    @GetMapping("/saida-caixas")
    @Timed
    public List<SaidaCaixa> getAllSaidaCaixas() {
        log.debug("REST request to get all SaidaCaixas");
        return saidaCaixaRepository.findAll();
    }

    /**
     * GET  /saida-caixas/:id : get the "id" saidaCaixa.
     *
     * @param id the id of the saidaCaixa to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the saidaCaixa, or with status 404 (Not Found)
     */
    @GetMapping("/saida-caixas/{id}")
    @Timed
    public ResponseEntity<SaidaCaixa> getSaidaCaixa(@PathVariable Long id) {
        log.debug("REST request to get SaidaCaixa : {}", id);
        Optional<SaidaCaixa> saidaCaixa = saidaCaixaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(saidaCaixa);
    }

    /**
     * DELETE  /saida-caixas/:id : delete the "id" saidaCaixa.
     *
     * @param id the id of the saidaCaixa to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/saida-caixas/{id}")
    @Timed
    public ResponseEntity<Void> deleteSaidaCaixa(@PathVariable Long id) {
        log.debug("REST request to delete SaidaCaixa : {}", id);

        saidaCaixaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

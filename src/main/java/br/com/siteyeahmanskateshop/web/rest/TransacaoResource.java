package br.com.siteyeahmanskateshop.web.rest;

import br.com.gerencianet.gnsdk.Gerencianet;
import br.com.gerencianet.gnsdk.exceptions.GerencianetException;
import br.com.siteyeahmanskateshop.domain.*;
import br.com.siteyeahmanskateshop.repository.*;
import br.com.siteyeahmanskateshop.service.MailService;
import br.com.siteyeahmanskateshop.service.UserService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * REST controller for managing Transacao.
 */
@RestController
@RequestMapping("/api")
public class TransacaoResource {

    private JSONObject options = new JSONObject();

    private final Logger log = LoggerFactory.getLogger(TransacaoResource.class);

    private final UserRepository userRepository;

    private final ProdutosRepository produtosRepository;

    private final UserService userService;

    private final MailService mailService;

    private JSONArray itemLista = null; //TODO: remover isso

    //private Set<Produtos> prodList = new ArrayList<>();//TODO: remover isso, e transformar o getPRodutoList em Lista

    @Autowired
    private VendaRepository vendaRepository;

    @Autowired
    private EnderecoEntregaRepository enderecoEntregaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public TransacaoResource(UserRepository userRepository, ProdutosRepository produtosRepository, UserService userService, MailService mailService) {

        this.userRepository = userRepository;
        this.produtosRepository = produtosRepository;
        this.userService = userService;
        this.mailService = mailService;

        /* ********* Set credentials parameters DEVELOPER ********/
        // options.put("client_id", "Client_Id_1aa0aa28b6210f694b47ae56dfef709dfa9b0c86");
        // options.put("client_secret", "Client_Secret_e2d27c8e89c8fe15e718b48cd1966861bfe09d4b");
        // options.put("sandbox", true);
        /* ********************************************* */

        /* ********* Set credentials parameters PRODUCAO ********/
        options.put("client_id", "Client_Id_018145b7d0558917103b1abe68a0e450b4dc7cd4");
        options.put("client_secret", "Client_Secret_9890381473621e818ae59ac20b0a6ef24fdd38fe");
        options.put("sandbox", false);
        /* ********************************************* */

    }


    @PostMapping("/pagamentoCartaoCredito")
    public ResponseEntity<String> pagamentoCartaoCredito(@RequestBody CartaoCredito cartaoCredito) {

            System.out.println("DADOS RECEBIDOS CARTAO" + cartaoCredito.toString());

            Long idTransacao = criaTransacaoPrivada(cartaoCredito.getProdutos());
            cartaoCredito.setId_transacao(idTransacao);
            System.out.println( idTransacao );
            if(idTransacao == 0){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }

            HashMap<String, String> params = new HashMap<String, String>();
            params.put("id",  "" + idTransacao);

            Map<String, Object> customer = new HashMap<String, Object>();
            customer.put("name", cartaoCredito.getCliente().getNome() + " " + cartaoCredito.getCliente().getSobrenome());
            customer.put("cpf", cartaoCredito.getCliente().getCpf());
            customer.put("phone_number", cartaoCredito.getCliente().getTelefone());
            customer.put("email", cartaoCredito.getCliente().getEmail());
            customer.put("birth", "1969-01-01");

            Map<String, Object> billingAddress = new HashMap<String, Object>();
            billingAddress.put("street", cartaoCredito.getEndereco().getRua());
            billingAddress.put("number", cartaoCredito.getEndereco().getNumero());
            billingAddress.put("neighborhood", cartaoCredito.getEndereco().getBairro());
            billingAddress.put("zipcode", cartaoCredito.getEndereco().getCep());
            billingAddress.put("city", cartaoCredito.getEndereco().getCidade());
            billingAddress.put("state", cartaoCredito.getEndereco().getUf());

            Map<String, Object> creditCard = new HashMap<String, Object>();
            creditCard.put("installments", cartaoCredito.getParcelas());
            creditCard.put("billing_address", billingAddress);
            creditCard.put("payment_token", cartaoCredito.getPayment_token());
            creditCard.put("customer", customer);

            Map<String, Object> payment = new HashMap<String, Object>();
            payment.put("credit_card", creditCard);

            //Map<String, Object> body = new HashMap<String, Object>();
            //body.put("payment", payment);
            JSONObject bodyNewTransaction = new JSONObject();
            bodyNewTransaction.put("payment", payment);
            JSONObject body = bodyNewTransaction;

            try {
                Gerencianet gn = new Gerencianet(options);
                JSONObject response = gn.call("payCharge", params, body);
                System.out.println(response);
                System.out.println(response.getLong("code"));
                if(response.getLong("code") == 200) {

                    mailService.sendEmailByBuyProduct( cartaoCredito , itemLista);

                    JSONObject data = response.getJSONObject("data");
                    System.out.println(data.getLong("charge_id"));

                    /* --------POPULA DADOS VENDA------*/

                    //Endereco
                    EnderecoEntregaResource endRes = new EnderecoEntregaResource(enderecoEntregaRepository);
                    EnderecoEntrega end = endRes.createEnderecoEntregaLocal(cartaoCredito.getEndereco());
                    if (end.getId() == null) {
                        System.err.println("Erro ao salvar dados de Endereco de Entrega em Venda!");
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                    }

                    //Cliente
                    cartaoCredito.getCliente().setEndereco(end);
                    ClienteResource cliRes = new ClienteResource(clienteRepository);
                    Cliente cli = cliRes.createClienteLocal(cartaoCredito.getCliente());
                    if (cli.getId() == null) {
                        System.err.println("Erro ao salvar dados de Cliente em Venda!");
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                    }

                    VendaResource venRes = new VendaResource(vendaRepository);
                    Venda vendaIn = new Venda();
                    vendaIn.setCodigoTransacao(cartaoCredito.getId_transacao().toString());
                    vendaIn.setDescricao("Venda Online. Cartão: " + cartaoCredito.getNumero_cartao());
                    vendaIn.setCliente(cli);
                    vendaIn.setEndereco(end);
                    vendaIn.setDataGeracao(Instant.now());
                    // vendaIn.setProdutos(prodList);
                    vendaIn.setListaItens(itemLista.toString());

                    Venda v = venRes.createVendaLocal(vendaIn);
                    System.out.println("new venda: " + v.getId() + " - " + v.toString());
                    if (v.getId() != null) {
                        System.out.println("SUCESSO SALVAR VENDA!");
                    } else {
                        System.err.println("FALHA SALVAR VENDA!");
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                    }
                    /* ---End--POPULA DADOS VENDA------*/

                    return ResponseEntity.ok().body("" + data.getLong("charge_id"));
                } else {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                }

            }catch (GerencianetException e){
                System.out.println(e.getCode());
                System.out.println(e.getError());
                System.out.println(e.getErrorDescription());
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping("/pagamentoBoletoBancario")
    public ResponseEntity<String> pagamentoBoletoTransacao(@RequestBody CartaoCredito cartaoCredito){

        System.out.println("DADOS RECEBIDOS BOLETO" + cartaoCredito.toString());

        Long idTransacao = criaTransacaoPrivada(cartaoCredito.getProdutos());
        cartaoCredito.setId_transacao(idTransacao);
        System.out.println( idTransacao );
        if(idTransacao == 0){
            return new ResponseEntity<>("Erro ao gera Id transação para um boleto via API", HttpStatus.INTERNAL_SERVER_ERROR);
        }


        Long id_transacao = cartaoCredito.getId_transacao();
        String nome = cartaoCredito.getCliente().getNome() + " " + cartaoCredito.getCliente().getSobrenome();
        String cpf = cartaoCredito.getCliente().getCpf();
        String telefone = cartaoCredito.getCliente().getTelefone();
        String email = cartaoCredito.getCliente().getEmail();

        Instant validade10String = Instant.now().plus(10, ChronoUnit.DAYS);
        Instant validade5String = Instant.now().plus(5, ChronoUnit.DAYS);
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault());
        String dataValidade = DATE_TIME_FORMATTER.format(validade10String);
        String dataDescontoValidade = DATE_TIME_FORMATTER.format(validade5String);

        Long desconto = (long)1000;

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", "" + id_transacao);

        JSONObject customer = new JSONObject();
        customer.put("name", nome);
        customer.put("cpf", cpf);
        customer.put("phone_number", telefone);
        customer.put("email" , email);

        // Desconto té uma data especifica
        JSONObject conditionalDiscount = new JSONObject();
        conditionalDiscount.put("type", "percentage");
        conditionalDiscount.put("value", desconto);
        conditionalDiscount.put("until_date", dataDescontoValidade);

        JSONObject bankingBillet = new JSONObject();
        bankingBillet.put("expire_at", dataValidade);
        bankingBillet.put("customer", customer);
        bankingBillet.put("conditional_discount", conditionalDiscount);
        // bankingBillet.put("message", mensagemDescritiva);

        JSONObject payment = new JSONObject();
        payment.put("banking_billet", bankingBillet);

        JSONObject body = new JSONObject();
        body.put("payment", payment);

        JSONObject data = null;

        try {
            Gerencianet gn = new Gerencianet(options);
            JSONObject response = gn.call("payCharge", params, body);
            System.out.println("--------------RETURN API-------------------");
            System.out.println(response.toString());
            data = response.getJSONObject("data");
            System.out.println(data.getString("link"));
            System.out.println("---------------------------------");

            //Envia email com Dados da Compra
            mailService.sendEmailByBuyProduct( cartaoCredito , itemLista, data.getString("link"));

            //Salva dados da compra em Base de Dados
            /* --------POPULA DADOS VENDA------*/

            //Endereco
            EnderecoEntregaResource endRes = new EnderecoEntregaResource(enderecoEntregaRepository);
            EnderecoEntrega end = endRes.createEnderecoEntregaLocal(cartaoCredito.getEndereco());
            if (end.getId() == null) {
                System.err.println("Erro ao salvar dados de Endereco de Entrega em Venda!");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }

            //Cliente
            cartaoCredito.getCliente().setEndereco(end);
            ClienteResource cliRes = new ClienteResource(clienteRepository);
            Cliente cli = cliRes.createClienteLocal(cartaoCredito.getCliente());
            if (cli.getId() == null) {
                System.err.println("Erro ao salvar dados de Cliente em Venda!");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }

            VendaResource venRes = new VendaResource(vendaRepository);
            Venda vendaIn = new Venda();
            vendaIn.setCodigoTransacao(cartaoCredito.getId_transacao().toString());
            vendaIn.setDescricao("Venda Online. BOLETO BANCARIO");
            vendaIn.setLink(data.getString("link"));
            vendaIn.setCliente(cli);
            vendaIn.setEndereco(end);
            vendaIn.setDataGeracao(Instant.now());
            vendaIn.setDataVencimento(validade10String);
            vendaIn.setDesconto(10l);
            // vendaIn.setProdutos(prodList);
            vendaIn.setListaItens(itemLista.toString());

            Venda v = venRes.createVendaLocal(vendaIn);
            System.out.println("new venda: " + v.getId() + " - " + v.toString());
            if (v.getId() != null) {
                System.out.println("SUCESSO SALVAR VENDA!");
            } else {
                System.err.println("FALHA SALVAR VENDA!");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
            /* ---End--POPULA DADOS VENDA------*/

            return ResponseEntity.ok().body("{ \"link\" : \"" + data.getString("link") + "\" , \"id\" : " + data.getLong("charge_id") + "}");

        } catch (GerencianetException e) {
            System.out.println(e.getCode());
            System.out.println(e.getError());
            System.out.println(e.getErrorDescription());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }



   /* @GetMapping("/criaTransacao")
    public ResponseEntity<String> criaTransacao() {
        JSONObject body;
        /* ********* Set credentials parameters DEVELOPER *******
        JSONObject options = new JSONObject();
        options.put("client_id", "Client_Id_af1818624eceb725c778a4bb962b5c93d0ea7251");
        options.put("client_secret", "Client_Secret_1ae7e92bc08a7fbaf5e0b594ce7bae0d2b66e40a");
        options.put("sandbox", true);
        /* ********************************************* */

        /*
         * ********* Set credentials parameters PRODUCAO ********
         JSONObject options = new JSONObject();
         options.put("client_id", "Client_Id_8f50f16d47f8c286ff90d443de3a2c6caf22c0ee");
         options.put("client_secret", "Client_Secret_bb78a50cd6801fb3e7230ebe43b490ee3c0d06e5");
         options.put("sandbox", false); /*
         *********************************************


        /*----------------Aqui é um teste-----
        // === Monta JSON nova Transacao ===
        JSONObject bodyNewTransaction = new JSONObject();
        JSONArray itemLista = new JSONArray();
        JSONObject itemX = new JSONObject();
            itemX.put("name", "Primeiro Produto Balance Board");
            itemX.put("amount", 1 );
            itemX.put("value", 1000);
        itemLista.put(itemX);
        bodyNewTransaction.put("items", itemLista);
        body = bodyNewTransaction;


        JSONObject data = null;
        try {
            Gerencianet gn = new Gerencianet(options);
            JSONObject response = gn.call("createCharge", new HashMap<String, String>(), body);
            System.out.println(response);
            System.out.println("--------------RETURN API-------------------");
            System.out.println(response.toString());
            data = response.getJSONObject("data");
            System.out.println(data.getLong("charge_id"));
            System.out.println("---------------------------------");
            return ResponseEntity.ok().body("ID: " + data.getLong("charge_id"));
            //return data.getLong("charge_id");
        } catch (GerencianetException e) {
            System.out.println(e.getCode());
            System.out.println(e.getError());
            System.out.println(e.getErrorDescription());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    } */

    private long criaTransacaoPrivada(ArrayList<Integer> produtos) {

        JSONObject body;

        // === Monta JSON nova Transacao ===
        JSONObject bodyNewTransaction = new JSONObject();

        itemLista = new JSONArray();

        ProdutosResource prod = new ProdutosResource(produtosRepository);
        for (int i : produtos) {
            Optional<Produtos> p = prod.getProdutosById(i);
            JSONObject itemX = new JSONObject();
            itemX.put("name", p.get().getNome());
            itemX.put("amount", 1);
            itemX.put("value", Integer.parseInt(p.get().getValorVenda() + "00"));
            itemLista.put(itemX);
        }

        JSONObject itemY = new JSONObject();
        itemY.put("name", "Custo do Envio");
        itemY.put("amount", 1);
        itemY.put("value", 6800);
        itemLista.put(itemY);

        bodyNewTransaction.put("items", itemLista);
        body = bodyNewTransaction;


        JSONObject data = null;
        try {
            Gerencianet gn = new Gerencianet(options);
            JSONObject response = gn.call("createCharge", new HashMap<String, String>(), body);
            System.out.println(response);
            System.out.println("--------------RETURN API-------------------");
            System.out.println(response.toString());
            data = response.getJSONObject("data");
            System.out.println(data.getLong("charge_id"));
            System.out.println("---------------------------------");

            if(response.getLong("code") == 200){
                return data.getLong("charge_id");
            }
            return 0;


            //return data.getLong("charge_id");
        } catch (GerencianetException e) {
            System.out.println(e.getCode());
            System.out.println(e.getError());
            System.out.println(e.getErrorDescription());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return 0;
    }

}

package br.com.siteyeahmanskateshop.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Base64;

/**
 * REST controller for managing Portfolio.
 */
@RestController
@RequestMapping("/api")
public class UploadResource {

    private final Logger log = LoggerFactory.getLogger(PortfolioResource.class);

    private final String jdbcUrl = "jdbc:postgresql://localhost:5432/siteyeahmanskateshop"; //prod
    // private final String jdbcUrl = "jdbc:postgresql://localhost:5432/siteyeahmanskateshop"; //dev
    private final String username = "postgres";
    // private final String password = "postgres";
    private final String password = "yeahmanskateshop123";


    @PostMapping("/uploadDB")
    @Timed
    public ResponseEntity FileUpload(@RequestParam("file") MultipartFile file) {
        final String sql = "INSERT INTO update_image(NAME,IMAGENAME, imagebyte) VALUES(?,?,?)";
        int IdKey = 0;
        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            try (final PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                stmt.setString(1, "DUDU");
                stmt.setString(2, file.getName());
                // String imagemg4 = gravarImagemBase64(file);

                //Reduz imagem ---------------------
                BufferedImage img = ImageIO.read(file.getInputStream());

                BufferedImage tempJPG = resizeImage(img, img.getWidth(), img.getHeight());

                File afile = new File("fileResize_test");
                ImageIO.write(tempJPG, "jpg", afile);

                byte[] bytesArray = new byte[(int) afile.length()];

                FileInputStream fis = new FileInputStream(afile);
                fis.read(bytesArray); //read file into bytes[]
                fis.close();
                // ---------------------------------


                // byte[] encodedBytes = Base64.getEncoder().encode(file.getBytes());
                byte[] encodedBytes = Base64.getEncoder().encode(bytesArray);


                // String encodedBytes = Base64.getEncoder().encodeToString(bytesArray);
                // System.out.println("encodedBytes - " + new String(encodedBytes));
                System.out.println("encodedBytes - ");
                //byte[] decodedBytes = Base64.getDecoder().decode(encodedBytes);
                //System.out.println("decodedBytes " + new String(decodedBytes));
                //stmt.setBytes(3, encodedBytes);

                String headerBase64 = "data:image/jpg;base64,"; //default
                byte[] header = headerBase64.getBytes();

                /*
                //nao rolo
                if(file.getName().contains(".png")){
                    headerBase64 = "data:image/png;base64,";
                } else if (file.getName().contains(".jpg")) {
                    headerBase64 = "data:image/jpg;base64,";
                } else if (file.getName().contains(".jpeg")) {
                    headerBase64 = "data:image/jpeg;base64,";
                }*/
                ByteArrayOutputStream bytesArrayToDB = new ByteArrayOutputStream();
                bytesArrayToDB.write(header);
                bytesArrayToDB.write(encodedBytes);
                System.out.println("Get BYTRE by BD");
                // System.out.println("BYTRE BD = " + bytesArrayToDB.toByteArray());
                stmt.setBytes(3, bytesArrayToDB.toByteArray()); //nao rolo
                // stmt.setString(3, headerBase64 + encodedBytes); //nao rolo
                // stmt.setBinaryStream(3, file.getInputStream(), file.getSize());

                stmt.executeUpdate();
                System.out.println("Image saved successfully.");

                final ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    IdKey = rs.getInt(1);
                    System.out.println("id Gerado para Imagem: " + IdKey);
                }

                conn.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("ERRO TO SAVE IMAGE");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("ERRO TO CONNECT DB");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        //everything was OK, return HTTP OK status (200) to the client
        //return ResponseEntity.ok().build();
        return new ResponseEntity<>( IdKey , HttpStatus.OK);
    }

    private static BufferedImage resizeImage(final Image image, int width, int height) {


        if(width > 2001 ){
            width = width/5;
            height = height/5;
        } else if(width > 1001 && width < 2000){
            width = width/3;
            height = height/3;
        } else {
            width = width / 2;
            height = height / 2;
        }

        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();

        return bufferedImage;
    }


    @GetMapping("/uploadDB")
    @Timed
    public ResponseEntity FileUpload(@RequestParam("id") int id) {

        final String sql = "SELECT * FROM update_image WHERE ID = ?";
        byte[] decodedBytes = null;

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                //LargeObjectManager lobj = ((org.postgresql.PGConnection)conn).getLargeObjectAPI();
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    int idImage = rs.getInt("ID");
                    String name = rs.getString("NAME");
                    String imagename = rs.getString("IMAGENAME");
                    String imagebyteString = rs.getString("IMAGEBYTE");
                    //InputStream imagebyte = rs.getBinaryStream("IMAGEBYTE");


                    //System.out.println("id: " + idImage + " , name: " + name + " , imageName: " + imagename + " , imagebyteString: " + imagebyteString);
                    //System.out.println("imagebyteBinary: " + imagebyteString);

                    byte[] imagemByte = rs.getBytes("IMAGEBYTE");
                    decodedBytes = imagemByte; //Base64.getDecoder().decode(imagemByte);
                    //System.out.println(" - decodedBytes " + new String(decodedBytes));

/*

                    InputStream blob = null;

                    BufferedImage bi = null;
                    byte[] bbuf = new byte[1024];
                    int bytesRead = 0;

                    blob = rs.getBinaryStream("IMAGEBYTE");
                    //bin = blob.getBinaryStream();
                    //bout = new FileOutputStream("teste.jpg");
				/*while ((bytesRead = bin.read(bbuf)) != -1) {
				     bout.write(bbuf, 0, bytesRead);
				}
                    //image = new File("teste.jpg");
                    bi = ImageIO.read(blob);
                    System.out.println("Image get Blob: " + bi);


                    //ta ok agora tem q retorna um arquivo de imagem!



                    //http://intelectolivre.blogspot.com/2008/07/exibindo-gravando-e-recuperando-imagens.html
*/

                }

                rs.close();
                conn.close();
                System.out.println("Carregou a imagem");
            } catch (Exception e ) {
                e.printStackTrace();
                System.out.println("ERRO TO GET IMAGE by DB");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("ERRO TO CONNECT DB");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        //everything was OK, return HTTP OK status (200) to the client
        //return ResponseEntity.ok().build();
        return new ResponseEntity<>( decodedBytes , HttpStatus.OK);
    }


/*
    public static String encodeImagem(byte[] imageByteArray) {
        return org.postgresql.util.Base64.encodeBytes(imageByteArray);
    }

    /*este método realiza a conversão da imagem recebido e a
      devolve como uma string.
    */
/*
    public String gravarImagemBase64(MultipartFile file){
        String imagemString = "";
        try{
            imagemString = encodeImagem(file.getBytes());
        }catch (IOException e) {
            e.printStackTrace();
        }
        return imagemString;
        //gravarImagemBase64(file).isEmpty()
    }
*/


    public String FileUploadString( int id) {

        final String sql = "SELECT * FROM update_image WHERE ID = ?";
        byte[] decodedBytes = null;

        try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password)) {
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                //LargeObjectManager lobj = ((org.postgresql.PGConnection)conn).getLargeObjectAPI();
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    int idImage = rs.getInt("ID");
                    String name = rs.getString("NAME");
                    String imagename = rs.getString("IMAGENAME");
                    String imagebyteString = rs.getString("IMAGEBYTE");
                    //InputStream imagebyte = rs.getBinaryStream("IMAGEBYTE");

                    System.out.println("id: " + idImage + " , name: " + name + " , imageName: " + imagename + " , imagebyteString: " /*+ imagebyteString*/);
                    //System.out.println("imagebyteBinary: " + imagebyteString);

                    byte[] imagemByte = rs.getBytes("IMAGEBYTE");
                    decodedBytes = imagemByte; //Base64.getDecoder().decode(imagemByte);
                    //System.out.println(" - decodedBytes " + new String(decodedBytes));

                }

                rs.close();
                conn.close();
                System.out.println("Carregou a imagem");
            } catch (Exception e ) {
                e.printStackTrace();
                System.out.println("ERRO TO GET IMAGE by DB");
                return "ERRO";
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("ERRO TO CONNECT DB");
            return "ERRO";
        }
        //everything was OK, return HTTP OK status (200) to the client
        //return ResponseEntity.ok().build();
        return new String(decodedBytes);
    }



}

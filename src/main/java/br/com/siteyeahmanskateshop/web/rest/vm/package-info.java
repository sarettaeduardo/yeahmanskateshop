/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.siteyeahmanskateshop.web.rest.vm;

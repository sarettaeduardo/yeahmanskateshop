import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IEnderecoEntrega } from 'app/shared/model/endereco-entrega.model';
import { getEntities as getEnderecoEntregas } from 'app/entities/endereco-entrega/endereco-entrega.reducer';
import { IHser } from 'app/shared/model/hser.model';
import { getEntities as getHsers } from 'app/entities/hser/hser.reducer';
import { getEntity, updateEntity, createEntity, reset } from './cliente.reducer';
import { ICliente } from 'app/shared/model/cliente.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IClienteUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IClienteUpdateState {
  isNew: boolean;
  enderecoId: number;
  hserId: number;
}

export class ClienteUpdate extends React.Component<IClienteUpdateProps, IClienteUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      enderecoId: 0,
      hserId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getEnderecoEntregas();
    this.props.getHsers();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { clienteEntity } = this.props;
      const entity = {
        ...clienteEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/cliente');
  };

  enderecoUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        enderecoId: -1
      });
    } else {
      for (const i in this.props.enderecoEntregas) {
        if (id === this.props.enderecoEntregas[i].id.toString()) {
          this.setState({
            enderecoId: this.props.enderecoEntregas[i].id
          });
        }
      }
    }
  };

  hserUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        hserId: -1
      });
    } else {
      for (const i in this.props.hsers) {
        if (id === this.props.hsers[i].id.toString()) {
          this.setState({
            hserId: this.props.hsers[i].id
          });
        }
      }
    }
  };

  render() {
    const { clienteEntity, enderecoEntregas, hsers, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.cliente.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.cliente.home.createOrEditLabel">Create or edit a Cliente</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : clienteEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="cliente-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="yeahmanskateshopApp.cliente.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="cliente-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="sobrenomeLabel" for="sobrenome">
                    <Translate contentKey="yeahmanskateshopApp.cliente.sobrenome">Sobrenome</Translate>
                  </Label>
                  <AvField
                    id="cliente-sobrenome"
                    type="text"
                    name="sobrenome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cpfLabel" for="cpf">
                    <Translate contentKey="yeahmanskateshopApp.cliente.cpf">Cpf</Translate>
                  </Label>
                  <AvField
                    id="cliente-cpf"
                    type="text"
                    name="cpf"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rgLabel" for="rg">
                    <Translate contentKey="yeahmanskateshopApp.cliente.rg">Rg</Translate>
                  </Label>
                  <AvField id="cliente-rg" type="text" name="rg" />
                </AvGroup>
                <AvGroup>
                  <Label id="telefoneLabel" for="telefone">
                    <Translate contentKey="yeahmanskateshopApp.cliente.telefone">Telefone</Translate>
                  </Label>
                  <AvField
                    id="cliente-telefone"
                    type="text"
                    name="telefone"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="email">
                    <Translate contentKey="yeahmanskateshopApp.cliente.email">Email</Translate>
                  </Label>
                  <AvField
                    id="cliente-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="endereco.id">
                    <Translate contentKey="yeahmanskateshopApp.cliente.endereco">Endereco</Translate>
                  </Label>
                  <AvInput
                    id="cliente-endereco"
                    type="select"
                    className="form-control"
                    name="endereco.id"
                    onChange={this.enderecoUpdate}
                    value={isNew && enderecoEntregas ? enderecoEntregas[0] && enderecoEntregas[0].id : ''}
                  >
                    <option value="" key="0" />
                    {enderecoEntregas
                      ? enderecoEntregas.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {'Cidade: ' + otherEntity.cidade + ', rua: ' + otherEntity.rua}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="hser.id">
                    <Translate contentKey="yeahmanskateshopApp.cliente.hser">Hser</Translate>
                  </Label>
                  <AvInput
                    id="cliente-hser"
                    type="select"
                    className="form-control"
                    name="hser.id"
                    onChange={this.hserUpdate}
                    value={isNew && hsers ? hsers[0] && hsers[0].id : ''}
                  >
                    <option value="" key="0" />
                    {hsers
                      ? hsers.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/cliente" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  enderecoEntregas: storeState.enderecoEntrega.entities,
  hsers: storeState.hser.entities,
  clienteEntity: storeState.cliente.entity,
  loading: storeState.cliente.loading,
  updating: storeState.cliente.updating
});

const mapDispatchToProps = {
  getEnderecoEntregas,
  getHsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClienteUpdate);

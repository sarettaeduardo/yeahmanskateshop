import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './colecao.reducer';
import { IColecao } from 'app/shared/model/colecao.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IColecaoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ColecaoDetail extends React.Component<IColecaoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { colecaoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.colecao.detail.title">Colecao</Translate> [<b>{colecaoEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nome">
                <Translate contentKey="yeahmanskateshopApp.colecao.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{colecaoEntity.nome}</dd>
            <dt>
              <span id="descricao">
                <Translate contentKey="yeahmanskateshopApp.colecao.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{colecaoEntity.descricao}</dd>
          </dl>
          <Button tag={Link} to="/entity/colecao" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/colecao/${colecaoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ colecao }: IRootState) => ({
  colecaoEntity: colecao.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ColecaoDetail);

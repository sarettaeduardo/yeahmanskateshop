import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IColecao, defaultValue } from 'app/shared/model/colecao.model';

export const ACTION_TYPES = {
  FETCH_COLECAO_LIST: 'colecao/FETCH_COLECAO_LIST',
  FETCH_COLECAO: 'colecao/FETCH_COLECAO',
  CREATE_COLECAO: 'colecao/CREATE_COLECAO',
  UPDATE_COLECAO: 'colecao/UPDATE_COLECAO',
  DELETE_COLECAO: 'colecao/DELETE_COLECAO',
  RESET: 'colecao/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IColecao>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ColecaoState = Readonly<typeof initialState>;

// Reducer

export default (state: ColecaoState = initialState, action): ColecaoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_COLECAO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_COLECAO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_COLECAO):
    case REQUEST(ACTION_TYPES.UPDATE_COLECAO):
    case REQUEST(ACTION_TYPES.DELETE_COLECAO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_COLECAO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_COLECAO):
    case FAILURE(ACTION_TYPES.CREATE_COLECAO):
    case FAILURE(ACTION_TYPES.UPDATE_COLECAO):
    case FAILURE(ACTION_TYPES.DELETE_COLECAO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_COLECAO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_COLECAO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_COLECAO):
    case SUCCESS(ACTION_TYPES.UPDATE_COLECAO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_COLECAO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/colecaos';

// Actions

export const getEntities: ICrudGetAllAction<IColecao> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_COLECAO_LIST,
  payload: axios.get<IColecao>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IColecao> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COLECAO,
    payload: axios.get<IColecao>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IColecao> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COLECAO,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IColecao> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COLECAO,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IColecao> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COLECAO,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

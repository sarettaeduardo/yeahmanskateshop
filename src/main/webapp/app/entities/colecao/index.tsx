import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Colecao from './colecao';
import ColecaoDetail from './colecao-detail';
import ColecaoUpdate from './colecao-update';
import ColecaoDeleteDialog from './colecao-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ColecaoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ColecaoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ColecaoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Colecao} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ColecaoDeleteDialog} />
  </>
);

export default Routes;

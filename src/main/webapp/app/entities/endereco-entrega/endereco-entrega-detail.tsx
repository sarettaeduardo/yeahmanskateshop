import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './endereco-entrega.reducer';
import { IEnderecoEntrega } from 'app/shared/model/endereco-entrega.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEnderecoEntregaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class EnderecoEntregaDetail extends React.Component<IEnderecoEntregaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { enderecoEntregaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.detail.title">EnderecoEntrega</Translate> [<b>
              {enderecoEntregaEntity.id}
            </b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="rua">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.rua">Rua</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.rua}</dd>
            <dt>
              <span id="numero">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.numero">Numero</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.numero}</dd>
            <dt>
              <span id="pais">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.pais">Pais</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.pais}</dd>
            <dt>
              <span id="uf">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.uf">Uf</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.uf}</dd>
            <dt>
              <span id="cidade">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cidade">Cidade</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.cidade}</dd>
            <dt>
              <span id="cep">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cep">Cep</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.cep}</dd>
            <dt>
              <span id="bairro">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.bairro">Bairro</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.bairro}</dd>
            <dt>
              <span id="complemento">
                <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.complemento">Complemento</Translate>
              </span>
            </dt>
            <dd>{enderecoEntregaEntity.complemento}</dd>
          </dl>
          <Button tag={Link} to="/entity/endereco-entrega" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/endereco-entrega/${enderecoEntregaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ enderecoEntrega }: IRootState) => ({
  enderecoEntregaEntity: enderecoEntrega.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnderecoEntregaDetail);

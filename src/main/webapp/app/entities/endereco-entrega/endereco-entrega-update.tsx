import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './endereco-entrega.reducer';
import { IEnderecoEntrega } from 'app/shared/model/endereco-entrega.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IEnderecoEntregaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEnderecoEntregaUpdateState {
  isNew: boolean;
}

export class EnderecoEntregaUpdate extends React.Component<IEnderecoEntregaUpdateProps, IEnderecoEntregaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { enderecoEntregaEntity } = this.props;
      const entity = {
        ...enderecoEntregaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/endereco-entrega');
  };

  render() {
    const { enderecoEntregaEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.enderecoEntrega.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.home.createOrEditLabel">
                Create or edit a EnderecoEntrega
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : enderecoEntregaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="endereco-entrega-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="ruaLabel" for="rua">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.rua">Rua</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-rua"
                    type="text"
                    name="rua"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="numeroLabel" for="numero">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.numero">Numero</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-numero"
                    type="number"
                    className="form-control"
                    name="numero"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="paisLabel" for="pais">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.pais">Pais</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-pais"
                    type="text"
                    name="pais"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="ufLabel" for="uf">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.uf">Uf</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-uf"
                    type="text"
                    name="uf"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cidadeLabel" for="cidade">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cidade">Cidade</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-cidade"
                    type="text"
                    name="cidade"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cepLabel" for="cep">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cep">Cep</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-cep"
                    type="text"
                    name="cep"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="bairroLabel" for="bairro">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.bairro">Bairro</Translate>
                  </Label>
                  <AvField
                    id="endereco-entrega-bairro"
                    type="text"
                    name="bairro"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="complementoLabel" for="complemento">
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.complemento">Complemento</Translate>
                  </Label>
                  <AvField id="endereco-entrega-complemento" type="text" name="complemento" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/endereco-entrega" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  enderecoEntregaEntity: storeState.enderecoEntrega.entity,
  loading: storeState.enderecoEntrega.loading,
  updating: storeState.enderecoEntrega.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnderecoEntregaUpdate);

import isEqual from 'lodash/isEqual';
import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEnderecoEntrega, defaultValue } from 'app/shared/model/endereco-entrega.model';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export const ACTION_TYPES = {
  FETCH_ENDERECOENTREGA_LIST: 'enderecoEntrega/FETCH_ENDERECOENTREGA_LIST',
  FETCH_ENDERECOENTREGA: 'enderecoEntrega/FETCH_ENDERECOENTREGA',
  CREATE_ENDERECOENTREGA: 'enderecoEntrega/CREATE_ENDERECOENTREGA',
  UPDATE_ENDERECOENTREGA: 'enderecoEntrega/UPDATE_ENDERECOENTREGA',
  DELETE_ENDERECOENTREGA: 'enderecoEntrega/DELETE_ENDERECOENTREGA',
  RESET: 'enderecoEntrega/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEnderecoEntrega>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type EnderecoEntregaState = Readonly<typeof initialState>;

// Reducer

export default (state: EnderecoEntregaState = initialState, action): EnderecoEntregaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ENDERECOENTREGA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ENDERECOENTREGA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ENDERECOENTREGA):
    case REQUEST(ACTION_TYPES.UPDATE_ENDERECOENTREGA):
    case REQUEST(ACTION_TYPES.DELETE_ENDERECOENTREGA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ENDERECOENTREGA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ENDERECOENTREGA):
    case FAILURE(ACTION_TYPES.CREATE_ENDERECOENTREGA):
    case FAILURE(ACTION_TYPES.UPDATE_ENDERECOENTREGA):
    case FAILURE(ACTION_TYPES.DELETE_ENDERECOENTREGA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ENDERECOENTREGA_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links, ITEMS_PER_PAGE)
      };
    case SUCCESS(ACTION_TYPES.FETCH_ENDERECOENTREGA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ENDERECOENTREGA):
    case SUCCESS(ACTION_TYPES.UPDATE_ENDERECOENTREGA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ENDERECOENTREGA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/endereco-entregas';

// Actions

export const getEntities: ICrudGetAllAction<IEnderecoEntrega> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ENDERECOENTREGA_LIST,
    payload: axios.get<IEnderecoEntrega>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IEnderecoEntrega> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ENDERECOENTREGA,
    payload: axios.get<IEnderecoEntrega>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IEnderecoEntrega> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ENDERECOENTREGA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEnderecoEntrega> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ENDERECOENTREGA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEnderecoEntrega> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ENDERECOENTREGA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

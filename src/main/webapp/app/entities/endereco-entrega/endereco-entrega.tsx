import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './endereco-entrega.reducer';
import { IEnderecoEntrega } from 'app/shared/model/endereco-entrega.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IEnderecoEntregaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IEnderecoEntregaState = IPaginationBaseState;

export class EnderecoEntrega extends React.Component<IEnderecoEntregaProps, IEnderecoEntregaState> {
  state: IEnderecoEntregaState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  reset = () => {
    this.setState({ activePage: 0 }, () => {
      this.props.reset();
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        activePage: 0,
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.reset()
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { enderecoEntregaList, match } = this.props;
    return (
      <div>
        <h2 id="endereco-entrega-heading">
          <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.home.title">Endereco Entregas</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.home.createLabel">Create new Endereco Entrega</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('rua')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.rua">Rua</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('numero')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.numero">Numero</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('pais')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.pais">Pais</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('uf')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.uf">Uf</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cidade')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cidade">Cidade</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cep')}>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.cep">Cep</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.bairro">Bairro</Translate>
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.enderecoEntrega.complemento">Complemento</Translate>
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {enderecoEntregaList.map((enderecoEntrega, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${enderecoEntrega.id}`} color="link" size="sm">
                        {enderecoEntrega.id}
                      </Button>
                    </td>
                    <td>{enderecoEntrega.rua}</td>
                    <td>{enderecoEntrega.numero}</td>
                    <td>{enderecoEntrega.pais}</td>
                    <td>{enderecoEntrega.uf}</td>
                    <td>{enderecoEntrega.cidade}</td>
                    <td>{enderecoEntrega.cep}</td>
                    <td>{enderecoEntrega.bairro}</td>
                    <td>{enderecoEntrega.complemento}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${enderecoEntrega.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${enderecoEntrega.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${enderecoEntrega.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ enderecoEntrega }: IRootState) => ({
  enderecoEntregaList: enderecoEntrega.entities,
  totalItems: enderecoEntrega.totalItems,
  links: enderecoEntrega.links
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnderecoEntrega);

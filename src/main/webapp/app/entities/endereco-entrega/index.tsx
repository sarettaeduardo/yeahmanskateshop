import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EnderecoEntrega from './endereco-entrega';
import EnderecoEntregaDetail from './endereco-entrega-detail';
import EnderecoEntregaUpdate from './endereco-entrega-update';
import EnderecoEntregaDeleteDialog from './endereco-entrega-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EnderecoEntregaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EnderecoEntregaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EnderecoEntregaDetail} />
      <ErrorBoundaryRoute path={match.url} component={EnderecoEntrega} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={EnderecoEntregaDeleteDialog} />
  </>
);

export default Routes;

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './endereco.reducer';
import { IEndereco } from 'app/shared/model/endereco.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IEnderecoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEnderecoUpdateState {
  isNew: boolean;
}

export class EnderecoUpdate extends React.Component<IEnderecoUpdateProps, IEnderecoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { enderecoEntity } = this.props;
      const entity = {
        ...enderecoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/endereco');
  };

  render() {
    const { enderecoEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="siteLucianaApp.endereco.home.createOrEditLabel">
              <Translate contentKey="siteLucianaApp.endereco.home.createOrEditLabel">Create or edit a Endereco</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : enderecoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="endereco-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="ruaLabel" for="rua">
                    <Translate contentKey="siteLucianaApp.endereco.rua">Rua</Translate>
                  </Label>
                  <AvField
                    id="endereco-rua"
                    type="text"
                    name="rua"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cepLabel" for="cep">
                    <Translate contentKey="siteLucianaApp.endereco.cep">Cep</Translate>
                  </Label>
                  <AvField
                    id="endereco-cep"
                    type="text"
                    name="cep"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cidadeLabel" for="cidade">
                    <Translate contentKey="siteLucianaApp.endereco.cidade">Cidade</Translate>
                  </Label>
                  <AvField
                    id="endereco-cidade"
                    type="text"
                    name="cidade"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="estadoLabel" for="estado">
                    <Translate contentKey="siteLucianaApp.endereco.estado">Estado</Translate>
                  </Label>
                  <AvField id="endereco-estado" type="text" name="estado" />
                </AvGroup>
                <AvGroup>
                  <Label id="paisLabel" for="pais">
                    <Translate contentKey="siteLucianaApp.endereco.pais">Pais</Translate>
                  </Label>
                  <AvField id="endereco-pais" type="text" name="pais" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/endereco" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  enderecoEntity: storeState.endereco.entity,
  loading: storeState.endereco.loading,
  updating: storeState.endereco.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnderecoUpdate);

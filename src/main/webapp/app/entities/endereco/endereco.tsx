import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './endereco.reducer';
import { IEndereco } from 'app/shared/model/endereco.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEnderecoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Endereco extends React.Component<IEnderecoProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { enderecoList, match } = this.props;
    return (
      <div>
        <h2 id="endereco-heading">
          <Translate contentKey="siteLucianaApp.endereco.home.title">Enderecos</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="siteLucianaApp.endereco.home.createLabel">Create new Endereco</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="siteLucianaApp.endereco.rua">Rua</Translate>
                </th>
                <th>
                  <Translate contentKey="siteLucianaApp.endereco.cep">Cep</Translate>
                </th>
                <th>
                  <Translate contentKey="siteLucianaApp.endereco.cidade">Cidade</Translate>
                </th>
                <th>
                  <Translate contentKey="siteLucianaApp.endereco.estado">Estado</Translate>
                </th>
                <th>
                  <Translate contentKey="siteLucianaApp.endereco.pais">Pais</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {enderecoList.map((endereco, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${endereco.id}`} color="link" size="sm">
                      {endereco.id}
                    </Button>
                  </td>
                  <td>{endereco.rua}</td>
                  <td>{endereco.cep}</td>
                  <td>{endereco.cidade}</td>
                  <td>{endereco.estado}</td>
                  <td>{endereco.pais}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${endereco.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${endereco.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${endereco.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ endereco }: IRootState) => ({
  enderecoList: endereco.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

// Para ativar a aplicacao
// mapStateToProps => Tras os valores, valor, que esta dentro do Reduces de VALORES, que esta dentro da STORE
// mapDispatchToProps => importa as acoes do componente, atraves do ACTION-TYPE (componente.reducer)
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Endereco);

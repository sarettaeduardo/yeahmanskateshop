import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './entrada-caixa.reducer';
import { IEntradaCaixa } from 'app/shared/model/entrada-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEntradaCaixaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class EntradaCaixaDetail extends React.Component<IEntradaCaixaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { entradaCaixaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.entradaCaixa.detail.title">EntradaCaixa</Translate> [<b>{entradaCaixaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nome">
                <Translate contentKey="yeahmanskateshopApp.entradaCaixa.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{entradaCaixaEntity.nome}</dd>
            <dt>
              <span id="descricao">
                <Translate contentKey="yeahmanskateshopApp.entradaCaixa.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{entradaCaixaEntity.descricao}</dd>
            <dt>
              <span id="valor">
                <Translate contentKey="yeahmanskateshopApp.entradaCaixa.valor">Valor</Translate>
              </span>
            </dt>
            <dd>{entradaCaixaEntity.valor}</dd>
            <dt>
              <span id="data">
                <Translate contentKey="yeahmanskateshopApp.entradaCaixa.data">Data</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={entradaCaixaEntity.data} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.entradaCaixa.cliente">Cliente</Translate>
            </dt>
            <dd>{entradaCaixaEntity.cliente ? entradaCaixaEntity.cliente.id : ''}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.entradaCaixa.produtos">Produtos</Translate>
            </dt>
            <dd>
              {entradaCaixaEntity.produtos
                ? entradaCaixaEntity.produtos.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.nome}</a>
                      {i === entradaCaixaEntity.produtos.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/entrada-caixa" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/entrada-caixa/${entradaCaixaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ entradaCaixa }: IRootState) => ({
  entradaCaixaEntity: entradaCaixa.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntradaCaixaDetail);

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICliente } from 'app/shared/model/cliente.model';
import { getEntities as getClientes } from 'app/entities/cliente/cliente.reducer';
import { IProdutos } from 'app/shared/model/produtos.model';
import { getEntities as getProdutos } from 'app/entities/produtos/produtos.reducer';
import { getEntity, updateEntity, createEntity, reset } from './entrada-caixa.reducer';
import { IEntradaCaixa } from 'app/shared/model/entrada-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IEntradaCaixaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEntradaCaixaUpdateState {
  isNew: boolean;
  idsprodutos: any[];
  clienteId: number;
}

export class EntradaCaixaUpdate extends React.Component<IEntradaCaixaUpdateProps, IEntradaCaixaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idsprodutos: [],
      clienteId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getClientes();
    this.props.getProdutos();
  }

  saveEntity = (event, errors, values) => {
    values.data = new Date(values.data);

    if (errors.length === 0) {
      const { entradaCaixaEntity } = this.props;
      const entity = {
        ...entradaCaixaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/entrada-caixa');
  };

  clienteUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        clienteId: -1
      });
    } else {
      for (const i in this.props.clientes) {
        if (id === this.props.clientes[i].id.toString()) {
          this.setState({
            clienteId: this.props.clientes[i].id
          });
        }
      }
    }
  };

  produtosUpdate = element => {
    const selected = Array.from(element.target.selectedOptions).map((e: any) => parseInt(e.value, 10));
    this.setState({
      idsprodutos: keysToValues(selected, this.props.produtos, 'id')
    });
  };

  displayprodutos(value: any) {
    if (this.state.idsprodutos && this.state.idsprodutos.length !== 0) {
      const list = [];
      for (const i in this.state.idsprodutos) {
        if (this.state.idsprodutos[i]) {
          list.push(this.state.idsprodutos[i].id);
        }
      }
      return list;
    }
    if (value.produtos && value.produtos.length !== 0) {
      const list = [];
      for (const i in value.produtos) {
        if (value.produtos[i]) {
          list.push(value.produtos[i].id);
        }
      }
      this.setState({
        idsprodutos: keysToValues(list, this.props.produtos, 'id')
      });
      return list;
    }
    return null;
  }

  render() {
    const { entradaCaixaEntity, clientes, produtos, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.entradaCaixa.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.entradaCaixa.home.createOrEditLabel">Create or edit a EntradaCaixa</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : entradaCaixaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="entrada-caixa-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="entrada-caixa-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descricaoLabel" for="descricao">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.descricao">Descricao</Translate>
                  </Label>
                  <AvField id="entrada-caixa-descricao" type="text" name="descricao" />
                </AvGroup>
                <AvGroup>
                  <Label id="valorLabel" for="valor">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.valor">Valor</Translate>
                  </Label>
                  <AvField
                    id="entrada-caixa-valor"
                    type="number"
                    className="form-control"
                    name="valor"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.data">Data</Translate>
                  </Label>
                  <AvInput
                    id="entrada-caixa-data"
                    type="datetime-local"
                    className="form-control"
                    name="data"
                    value={isNew ? null : convertDateTimeFromServer(this.props.entradaCaixaEntity.data)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="cliente.id">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.cliente">Cliente</Translate>
                  </Label>
                  <AvInput
                    id="entrada-caixa-cliente"
                    type="select"
                    className="form-control"
                    name="cliente.id"
                    onChange={this.clienteUpdate}
                    value={isNew && clientes ? clientes[0] && clientes[0].id : ''}
                  >
                    <option value="" key="0" />
                    {clientes
                      ? clientes.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.nome + ' ' + otherEntity.sobrenome}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="produtos">
                    <Translate contentKey="yeahmanskateshopApp.entradaCaixa.produtos">Produtos</Translate>
                  </Label>
                  <AvInput
                    id="entrada-caixa-produtos"
                    type="select"
                    multiple
                    className="form-control"
                    name="fakeprodutos"
                    value={this.displayprodutos(entradaCaixaEntity)}
                    onChange={this.produtosUpdate}
                  >
                    <option value="" key="0" />
                    {produtos
                      ? produtos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.nome}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvInput id="entrada-caixa-produtos" type="hidden" name="produtos" value={this.state.idsprodutos} />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/entrada-caixa" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  clientes: storeState.cliente.entities,
  produtos: storeState.produtos.entities,
  entradaCaixaEntity: storeState.entradaCaixa.entity,
  loading: storeState.entradaCaixa.loading,
  updating: storeState.entradaCaixa.updating
});

const mapDispatchToProps = {
  getClientes,
  getProdutos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntradaCaixaUpdate);

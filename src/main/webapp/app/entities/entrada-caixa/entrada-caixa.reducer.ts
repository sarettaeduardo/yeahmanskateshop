import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEntradaCaixa, defaultValue } from 'app/shared/model/entrada-caixa.model';

export const ACTION_TYPES = {
  FETCH_ENTRADACAIXA_LIST: 'entradaCaixa/FETCH_ENTRADACAIXA_LIST',
  FETCH_ENTRADACAIXA: 'entradaCaixa/FETCH_ENTRADACAIXA',
  CREATE_ENTRADACAIXA: 'entradaCaixa/CREATE_ENTRADACAIXA',
  UPDATE_ENTRADACAIXA: 'entradaCaixa/UPDATE_ENTRADACAIXA',
  DELETE_ENTRADACAIXA: 'entradaCaixa/DELETE_ENTRADACAIXA',
  RESET: 'entradaCaixa/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEntradaCaixa>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type EntradaCaixaState = Readonly<typeof initialState>;

// Reducer

export default (state: EntradaCaixaState = initialState, action): EntradaCaixaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ENTRADACAIXA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ENTRADACAIXA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ENTRADACAIXA):
    case REQUEST(ACTION_TYPES.UPDATE_ENTRADACAIXA):
    case REQUEST(ACTION_TYPES.DELETE_ENTRADACAIXA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ENTRADACAIXA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ENTRADACAIXA):
    case FAILURE(ACTION_TYPES.CREATE_ENTRADACAIXA):
    case FAILURE(ACTION_TYPES.UPDATE_ENTRADACAIXA):
    case FAILURE(ACTION_TYPES.DELETE_ENTRADACAIXA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ENTRADACAIXA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ENTRADACAIXA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ENTRADACAIXA):
    case SUCCESS(ACTION_TYPES.UPDATE_ENTRADACAIXA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ENTRADACAIXA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/entrada-caixas';

// Actions

export const getEntities: ICrudGetAllAction<IEntradaCaixa> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_ENTRADACAIXA_LIST,
  payload: axios.get<IEntradaCaixa>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IEntradaCaixa> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ENTRADACAIXA,
    payload: axios.get<IEntradaCaixa>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IEntradaCaixa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ENTRADACAIXA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEntradaCaixa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ENTRADACAIXA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEntradaCaixa> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ENTRADACAIXA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

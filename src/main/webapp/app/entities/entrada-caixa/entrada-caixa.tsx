import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './entrada-caixa.reducer';
import { IEntradaCaixa } from 'app/shared/model/entrada-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEntradaCaixaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class EntradaCaixa extends React.Component<IEntradaCaixaProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { entradaCaixaList, match } = this.props;
    return (
      <div>
        <h2 id="entrada-caixa-heading">
          <Translate contentKey="yeahmanskateshopApp.entradaCaixa.home.title">Entrada Caixas</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="yeahmanskateshopApp.entradaCaixa.home.createLabel">Create new Entrada Caixa</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.nome">Nome</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.descricao">Descricao</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.valor">Valor</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.data">Data</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.cliente">Cliente</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.entradaCaixa.produtos">Produtos</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {entradaCaixaList.map((entradaCaixa, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${entradaCaixa.id}`} color="link" size="sm">
                      {entradaCaixa.id}
                    </Button>
                  </td>
                  <td>{entradaCaixa.nome}</td>
                  <td>{entradaCaixa.descricao}</td>
                  <td>{entradaCaixa.valor}</td>
                  <td>
                    <TextFormat type="date" value={entradaCaixa.data} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{entradaCaixa.cliente ? <Link to={`cliente/${entradaCaixa.cliente.id}`}>{entradaCaixa.cliente.id}</Link> : ''}</td>
                  <td>
                    {entradaCaixa.produtos
                      ? entradaCaixa.produtos.map((val, j) => (
                          <span key={j}>
                            <Link to={`produtos/${val.id}`}>{val.id}</Link>
                            {j === entradaCaixa.produtos.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${entradaCaixa.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${entradaCaixa.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${entradaCaixa.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ entradaCaixa }: IRootState) => ({
  entradaCaixaList: entradaCaixa.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntradaCaixa);

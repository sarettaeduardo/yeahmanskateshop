import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EntradaCaixa from './entrada-caixa';
import EntradaCaixaDetail from './entrada-caixa-detail';
import EntradaCaixaUpdate from './entrada-caixa-update';
import EntradaCaixaDeleteDialog from './entrada-caixa-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EntradaCaixaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EntradaCaixaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EntradaCaixaDetail} />
      <ErrorBoundaryRoute path={match.url} component={EntradaCaixa} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={EntradaCaixaDeleteDialog} />
  </>
);

export default Routes;

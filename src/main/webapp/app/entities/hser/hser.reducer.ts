import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IHser, defaultValue } from 'app/shared/model/hser.model';

export const ACTION_TYPES = {
  FETCH_HSER_LIST: 'hser/FETCH_HSER_LIST',
  FETCH_HSER: 'hser/FETCH_HSER',
  CREATE_HSER: 'hser/CREATE_HSER',
  UPDATE_HSER: 'hser/UPDATE_HSER',
  DELETE_HSER: 'hser/DELETE_HSER',
  RESET: 'hser/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IHser>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type HserState = Readonly<typeof initialState>;

// Reducer

export default (state: HserState = initialState, action): HserState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_HSER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_HSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_HSER):
    case REQUEST(ACTION_TYPES.UPDATE_HSER):
    case REQUEST(ACTION_TYPES.DELETE_HSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_HSER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_HSER):
    case FAILURE(ACTION_TYPES.CREATE_HSER):
    case FAILURE(ACTION_TYPES.UPDATE_HSER):
    case FAILURE(ACTION_TYPES.DELETE_HSER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_HSER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_HSER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_HSER):
    case SUCCESS(ACTION_TYPES.UPDATE_HSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_HSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/hsers';

// Actions

export const getEntities: ICrudGetAllAction<IHser> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_HSER_LIST,
  payload: axios.get<IHser>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IHser> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_HSER,
    payload: axios.get<IHser>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IHser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_HSER,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IHser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_HSER,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IHser> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_HSER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

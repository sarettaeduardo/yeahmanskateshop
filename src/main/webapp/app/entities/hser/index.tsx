import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Hser from './hser';
import HserDetail from './hser-detail';
import HserUpdate from './hser-update';
import HserDeleteDialog from './hser-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={HserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={HserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={HserDetail} />
      <ErrorBoundaryRoute path={match.url} component={Hser} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={HserDeleteDialog} />
  </>
);

export default Routes;

import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Endereco from './endereco';
import Pessoa from './pessoa';
import Produto from './produto';
import Transacao from './transacao';
import Orcamento from './orcamento';
import Portfolio from './portfolio';
import Produtos from './produtos';
import Categoria from './categoria';
import Colecao from './colecao';
import Venda from './venda';
import SaidaCaixa from './saida-caixa';
import EntradaCaixa from './entrada-caixa';
import Cliente from './cliente';
import EnderecoEntrega from './endereco-entrega';
import Hser from './hser';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      {/* <ErrorBoundaryRoute path={`${match.url}/endereco`} component={Endereco} />
      <ErrorBoundaryRoute path={`${match.url}/pessoa`} component={Pessoa} />
      <ErrorBoundaryRoute path={`${match.url}/produto`} component={Produto} />
      <ErrorBoundaryRoute path={`${match.url}/transacao`} component={Transacao} /> */}
      <ErrorBoundaryRoute path={`${match.url}/orcamento`} component={Orcamento} />
      <ErrorBoundaryRoute path={`${match.url}/portfolio`} component={Portfolio} />
      <ErrorBoundaryRoute path={`${match.url}/produtos`} component={Produtos} />
      <ErrorBoundaryRoute path={`${match.url}/categoria`} component={Categoria} />
      <ErrorBoundaryRoute path={`${match.url}/colecao`} component={Colecao} />
      <ErrorBoundaryRoute path={`${match.url}/venda`} component={Venda} />
      <ErrorBoundaryRoute path={`${match.url}/saida-caixa`} component={SaidaCaixa} />
      <ErrorBoundaryRoute path={`${match.url}/entrada-caixa`} component={EntradaCaixa} />
      <ErrorBoundaryRoute path={`${match.url}/cliente`} component={Cliente} />
      <ErrorBoundaryRoute path={`${match.url}/endereco-entrega`} component={EnderecoEntrega} />
      <ErrorBoundaryRoute path={`${match.url}/hser`} component={Hser} />
      {/* jhipster-needle-add-route-path - JHipster will routes here */}
    </Switch>
  </div>
);

export default Routes;

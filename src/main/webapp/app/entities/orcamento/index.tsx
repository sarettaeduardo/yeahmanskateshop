import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Orcamento from './orcamento';
import OrcamentoDetail from './orcamento-detail';
import OrcamentoUpdate from './orcamento-update';
import OrcamentoDeleteDialog from './orcamento-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={OrcamentoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={OrcamentoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={OrcamentoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Orcamento} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={OrcamentoDeleteDialog} />
  </>
);

export default Routes;

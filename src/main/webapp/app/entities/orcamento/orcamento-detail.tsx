import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './orcamento.reducer';
import { IOrcamento } from 'app/shared/model/orcamento.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOrcamentoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class OrcamentoDetail extends React.Component<IOrcamentoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { orcamentoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="siteLucianaApp.orcamento.detail.title">Orcamento</Translate> [<b>{orcamentoEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="codigoTransacao">
                <Translate contentKey="siteLucianaApp.orcamento.codigoTransacao">Codigo Transacao</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.codigoTransacao}</dd>
            <dt>
              <span id="nome">
                <Translate contentKey="siteLucianaApp.orcamento.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.nome}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="siteLucianaApp.orcamento.email">Email</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.email}</dd>
            <dt>
              <span id="telefone">
                <Translate contentKey="siteLucianaApp.orcamento.telefone">Telefone</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.telefone}</dd>
            <dt>
              <span id="cpf">
                <Translate contentKey="siteLucianaApp.orcamento.cpf">Cpf</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.cpf}</dd>
            <dt>
              <span id="link">
                <Translate contentKey="siteLucianaApp.orcamento.link">Link</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.link}</dd>
            <dt>
              <span id="listaItens">
                <Translate contentKey="siteLucianaApp.orcamento.listaItens">Lista Itens</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.listaItens}</dd>
            <dt>
              <span id="dataGeracao">
                <Translate contentKey="siteLucianaApp.orcamento.dataGeracao">Data Geracao</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={orcamentoEntity.dataGeracao} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="dataVencimento">
                <Translate contentKey="siteLucianaApp.orcamento.dataVencimento">Data Vencimento</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={orcamentoEntity.dataVencimento} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="total">
                <Translate contentKey="siteLucianaApp.orcamento.total">Total</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.total}</dd>
            <dt>
              <span id="desconto">
                <Translate contentKey="siteLucianaApp.orcamento.desconto">Desconto</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.desconto}</dd>
            <dt>
              <span id="area">
                <Translate contentKey="siteLucianaApp.orcamento.area">Area</Translate>
              </span>
            </dt>
            <dd>{orcamentoEntity.area}</dd>
          </dl>
          <Button tag={Link} to="/entity/orcamento" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/orcamento/${orcamentoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ orcamento }: IRootState) => ({
  orcamentoEntity: orcamento.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrcamentoDetail);

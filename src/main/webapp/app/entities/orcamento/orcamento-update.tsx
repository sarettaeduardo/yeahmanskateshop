import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './orcamento.reducer';
import { IOrcamento } from 'app/shared/model/orcamento.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IOrcamentoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IOrcamentoUpdateState {
  isNew: boolean;
}

export class OrcamentoUpdate extends React.Component<IOrcamentoUpdateProps, IOrcamentoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.dataGeracao = new Date(values.dataGeracao);
    values.dataVencimento = new Date(values.dataVencimento);

    if (errors.length === 0) {
      const { orcamentoEntity } = this.props;
      const entity = {
        ...orcamentoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/orcamento');
  };

  render() {
    const { orcamentoEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="siteLucianaApp.orcamento.home.createOrEditLabel">
              <Translate contentKey="siteLucianaApp.orcamento.home.createOrEditLabel">Create or edit a Orcamento</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : orcamentoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="orcamento-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="codigoTransacaoLabel" for="codigoTransacao">
                    <Translate contentKey="siteLucianaApp.orcamento.codigoTransacao">Codigo Transacao</Translate>
                  </Label>
                  <AvField id="orcamento-codigoTransacao" type="number" className="form-control" name="codigoTransacao" />
                </AvGroup>
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="siteLucianaApp.orcamento.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="orcamento-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="email">
                    <Translate contentKey="siteLucianaApp.orcamento.email">Email</Translate>
                  </Label>
                  <AvField
                    id="orcamento-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="telefoneLabel" for="telefone">
                    <Translate contentKey="siteLucianaApp.orcamento.telefone">Telefone</Translate>
                  </Label>
                  <AvField
                    id="orcamento-telefone"
                    type="text"
                    name="telefone"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cpfLabel" for="cpf">
                    <Translate contentKey="siteLucianaApp.orcamento.cpf">Cpf</Translate>
                  </Label>
                  <AvField
                    id="orcamento-cpf"
                    type="text"
                    name="cpf"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="linkLabel" for="link">
                    <Translate contentKey="siteLucianaApp.orcamento.link">Link</Translate>
                  </Label>
                  <AvField id="orcamento-link" type="text" name="link" />
                </AvGroup>
                <AvGroup>
                  <Label id="listaItensLabel" for="listaItens">
                    <Translate contentKey="siteLucianaApp.orcamento.listaItens">Lista Itens</Translate>
                  </Label>
                  <AvField id="orcamento-listaItens" type="text" name="listaItens" />
                </AvGroup>
                <AvGroup>
                  <Label id="dataGeracaoLabel" for="dataGeracao">
                    <Translate contentKey="siteLucianaApp.orcamento.dataGeracao">Data Geracao</Translate>
                  </Label>
                  <AvInput
                    id="orcamento-dataGeracao"
                    type="datetime-local"
                    className="form-control"
                    name="dataGeracao"
                    value={isNew ? null : convertDateTimeFromServer(this.props.orcamentoEntity.dataGeracao)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataVencimentoLabel" for="dataVencimento">
                    <Translate contentKey="siteLucianaApp.orcamento.dataVencimento">Data Vencimento</Translate>
                  </Label>
                  <AvInput
                    id="orcamento-dataVencimento"
                    type="datetime-local"
                    className="form-control"
                    name="dataVencimento"
                    value={isNew ? null : convertDateTimeFromServer(this.props.orcamentoEntity.dataVencimento)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="totalLabel" for="total">
                    <Translate contentKey="siteLucianaApp.orcamento.total">Total</Translate>
                  </Label>
                  <AvField id="orcamento-total" type="number" className="form-control" name="total" />
                </AvGroup>
                <AvGroup>
                  <Label id="descontoLabel" for="desconto">
                    <Translate contentKey="siteLucianaApp.orcamento.desconto">Desconto</Translate>
                  </Label>
                  <AvField id="orcamento-desconto" type="number" className="form-control" name="desconto" />
                </AvGroup>
                <AvGroup>
                  <Label id="areaLabel" for="area">
                    <Translate contentKey="siteLucianaApp.orcamento.area">Area</Translate>
                  </Label>
                  <AvField id="orcamento-area" type="number" className="form-control" name="area" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/orcamento" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  orcamentoEntity: storeState.orcamento.entity,
  loading: storeState.orcamento.loading,
  updating: storeState.orcamento.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrcamentoUpdate);

import isEqual from 'lodash/isEqual';
import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IOrcamento, defaultValue } from 'app/shared/model/orcamento.model';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export const ACTION_TYPES = {
  FETCH_ORCAMENTO_LIST: 'orcamento/FETCH_ORCAMENTO_LIST',
  FETCH_ORCAMENTO: 'orcamento/FETCH_ORCAMENTO',
  CREATE_ORCAMENTO: 'orcamento/CREATE_ORCAMENTO',
  UPDATE_ORCAMENTO: 'orcamento/UPDATE_ORCAMENTO',
  DELETE_ORCAMENTO: 'orcamento/DELETE_ORCAMENTO',
  RESET: 'orcamento/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOrcamento>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type OrcamentoState = Readonly<typeof initialState>;

// Reducer

export default (state: OrcamentoState = initialState, action): OrcamentoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ORCAMENTO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ORCAMENTO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ORCAMENTO):
    case REQUEST(ACTION_TYPES.UPDATE_ORCAMENTO):
    case REQUEST(ACTION_TYPES.DELETE_ORCAMENTO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ORCAMENTO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ORCAMENTO):
    case FAILURE(ACTION_TYPES.CREATE_ORCAMENTO):
    case FAILURE(ACTION_TYPES.UPDATE_ORCAMENTO):
    case FAILURE(ACTION_TYPES.DELETE_ORCAMENTO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ORCAMENTO_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links, ITEMS_PER_PAGE)
      };
    case SUCCESS(ACTION_TYPES.FETCH_ORCAMENTO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ORCAMENTO):
    case SUCCESS(ACTION_TYPES.UPDATE_ORCAMENTO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ORCAMENTO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/orcamentos';

// Actions

export const getEntities: ICrudGetAllAction<IOrcamento> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ORCAMENTO_LIST,
    payload: axios.get<IOrcamento>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IOrcamento> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ORCAMENTO,
    payload: axios.get<IOrcamento>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IOrcamento> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ORCAMENTO,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IOrcamento> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ORCAMENTO,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IOrcamento> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ORCAMENTO,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './orcamento.reducer';
import { IOrcamento } from 'app/shared/model/orcamento.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IOrcamentoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IOrcamentoState = IPaginationBaseState;

export class Orcamento extends React.Component<IOrcamentoProps, IOrcamentoState> {
  state: IOrcamentoState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  reset = () => {
    this.setState({ activePage: 0 }, () => {
      this.props.reset();
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        activePage: 0,
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.reset()
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { orcamentoList, match } = this.props;
    return (
      <div>
        <h2 id="orcamento-heading">
          <Translate contentKey="siteLucianaApp.orcamento.home.title">Orcamentos</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="siteLucianaApp.orcamento.home.createLabel">Create new Orcamento</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('codigoTransacao')}>
                    <Translate contentKey="siteLucianaApp.orcamento.codigoTransacao">Codigo Transacao</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('nome')}>
                    <Translate contentKey="siteLucianaApp.orcamento.nome">Nome</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('email')}>
                    <Translate contentKey="siteLucianaApp.orcamento.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('telefone')}>
                    <Translate contentKey="siteLucianaApp.orcamento.telefone">Telefone</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cpf')}>
                    <Translate contentKey="siteLucianaApp.orcamento.cpf">Cpf</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('link')}>
                    <Translate contentKey="siteLucianaApp.orcamento.link">Link</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('listaItens')}>
                    <Translate contentKey="siteLucianaApp.orcamento.listaItens">Lista Itens</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataGeracao')}>
                    <Translate contentKey="siteLucianaApp.orcamento.dataGeracao">Data Geracao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataVencimento')}>
                    <Translate contentKey="siteLucianaApp.orcamento.dataVencimento">Data Vencimento</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('total')}>
                    <Translate contentKey="siteLucianaApp.orcamento.total">Total</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('desconto')}>
                    <Translate contentKey="siteLucianaApp.orcamento.desconto">Desconto</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('area')}>
                    <Translate contentKey="siteLucianaApp.orcamento.area">Area</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {orcamentoList.map((orcamento, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${orcamento.id}`} color="link" size="sm">
                        {orcamento.id}
                      </Button>
                    </td>
                    <td>{orcamento.codigoTransacao}</td>
                    <td>{orcamento.nome}</td>
                    <td>{orcamento.email}</td>
                    <td>{orcamento.telefone}</td>
                    <td>{orcamento.cpf}</td>
                    <td>{orcamento.link}</td>
                    <td>{orcamento.listaItens}</td>
                    <td>
                      <TextFormat type="date" value={orcamento.dataGeracao} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={orcamento.dataVencimento} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{orcamento.total}</td>
                    <td>{orcamento.desconto}</td>
                    <td>{orcamento.area}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${orcamento.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${orcamento.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${orcamento.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ orcamento }: IRootState) => ({
  orcamentoList: orcamento.entities,
  totalItems: orcamento.totalItems,
  links: orcamento.links
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orcamento);

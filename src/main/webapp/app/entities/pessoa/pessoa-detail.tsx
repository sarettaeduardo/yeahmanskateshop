import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pessoa.reducer';
import { IPessoa } from 'app/shared/model/pessoa.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPessoaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PessoaDetail extends React.Component<IPessoaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { pessoaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="siteLucianaApp.pessoa.detail.title">Pessoa</Translate> [<b>{pessoaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nome">
                <Translate contentKey="siteLucianaApp.pessoa.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{pessoaEntity.nome}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="siteLucianaApp.pessoa.email">Email</Translate>
              </span>
            </dt>
            <dd>{pessoaEntity.email}</dd>
            <dt>
              <span id="senha">
                <Translate contentKey="siteLucianaApp.pessoa.senha">Senha</Translate>
              </span>
            </dt>
            <dd>{pessoaEntity.senha}</dd>
            <dt>
              <Translate contentKey="siteLucianaApp.pessoa.endereco">Endereco</Translate>
            </dt>
            <dd>{pessoaEntity.endereco ? pessoaEntity.endereco.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/pessoa" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/pessoa/${pessoaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pessoa }: IRootState) => ({
  pessoaEntity: pessoa.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PessoaDetail);

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IEndereco } from 'app/shared/model/endereco.model';
import { getEntities as getEnderecos } from 'app/entities/endereco/endereco.reducer';
import { getEntity, updateEntity, createEntity, reset } from './pessoa.reducer';
import { IPessoa } from 'app/shared/model/pessoa.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IPessoaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPessoaUpdateState {
  isNew: boolean;
  enderecoId: number;
}

export class PessoaUpdate extends React.Component<IPessoaUpdateProps, IPessoaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      enderecoId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getEnderecos();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { pessoaEntity } = this.props;
      const entity = {
        ...pessoaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/pessoa');
  };

  enderecoUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        enderecoId: -1
      });
    } else {
      for (const i in this.props.enderecos) {
        if (id === this.props.enderecos[i].id.toString()) {
          this.setState({
            enderecoId: this.props.enderecos[i].id
          });
        }
      }
    }
  };

  render() {
    const { pessoaEntity, enderecos, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="siteLucianaApp.pessoa.home.createOrEditLabel">
              <Translate contentKey="siteLucianaApp.pessoa.home.createOrEditLabel">Create or edit a Pessoa</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : pessoaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="pessoa-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="siteLucianaApp.pessoa.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="pessoa-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="email">
                    <Translate contentKey="siteLucianaApp.pessoa.email">Email</Translate>
                  </Label>
                  <AvField
                    id="pessoa-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="senhaLabel" for="senha">
                    <Translate contentKey="siteLucianaApp.pessoa.senha">Senha</Translate>
                  </Label>
                  <AvField
                    id="pessoa-senha"
                    type="text"
                    name="senha"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="endereco.id">
                    <Translate contentKey="siteLucianaApp.pessoa.endereco">Endereco</Translate>
                  </Label>
                  <AvInput id="pessoa-endereco" type="select" className="form-control" name="endereco.id" onChange={this.enderecoUpdate}>
                    <option value="" key="0" />
                    {enderecos
                      ? enderecos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/pessoa" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  enderecos: storeState.endereco.entities,
  pessoaEntity: storeState.pessoa.entity,
  loading: storeState.pessoa.loading,
  updating: storeState.pessoa.updating
});

const mapDispatchToProps = {
  getEnderecos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PessoaUpdate);

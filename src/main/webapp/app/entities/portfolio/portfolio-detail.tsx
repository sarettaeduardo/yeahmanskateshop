import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './portfolio.reducer';
import { IPortfolio } from 'app/shared/model/portfolio.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPortfolioDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPortfolioDetailState {
  imgprev: string;
}

export class PortfolioDetail extends React.Component<IPortfolioDetailProps, IPortfolioDetailState> {
  state: IPortfolioDetailState = {
    imgprev: ''
  };

  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  uploadImagem = idFoto => {
    fetch('/api/uploadDB?id=' + idFoto, {
      method: 'GET'
    })
      .then(response => {
        response.text().then(result => {
          this.setState({
            imgprev: result
          });
        });
      })
      .catch(err => {
        console.error(err);
      });
  };

  componentWillMount() {
    setTimeout(() => {
      this.uploadImagem(this.props.portfolioEntity.foto);
    }, 3000);
  }

  render() {
    const { portfolioEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.portfolio.detail.title">Portfolio</Translate> [<b>{portfolioEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="foto">
                <Translate contentKey="yeahmanskateshopApp.portfolio.foto">Foto</Translate>
              </span>
            </dt>
            <dd>{portfolioEntity.foto}</dd>

            <img id="previewimg" src={this.state.imgprev} />

            <dt>
              <span id="titulo">
                <Translate contentKey="yeahmanskateshopApp.portfolio.titulo">Titulo</Translate>
              </span>
            </dt>
            <dd>{portfolioEntity.titulo}</dd>
            <dt>
              <span id="mensagem">
                <Translate contentKey="yeahmanskateshopApp.portfolio.mensagem">Mensagem</Translate>
              </span>
            </dt>
            <dd>{portfolioEntity.mensagem}</dd>
            <dt>
              <span id="linkYoutube">
                <Translate contentKey="yeahmanskateshopApp.portfolio.linkYoutube">Link Youtube</Translate>
              </span>
            </dt>
            <dd>{portfolioEntity.linkYoutube}</dd>
            <dt>
              <span id="linkInstagram">
                <Translate contentKey="yeahmanskateshopApp.portfolio.linkInstagram">Link Instagram</Translate>
              </span>
            </dt>
            <dd>{portfolioEntity.linkInstagram}</dd>
          </dl>
          <Button tag={Link} to="/entity/portfolio" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/portfolio/${portfolioEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ portfolio }: IRootState) => ({
  portfolioEntity: portfolio.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PortfolioDetail);

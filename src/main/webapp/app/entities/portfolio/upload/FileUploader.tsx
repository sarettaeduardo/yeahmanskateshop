import React from 'react';

export interface IFileUploaderProps {
  dadoIdImage: Function;
}

class FileUploader extends React.Component<IFileUploaderProps> {
  onChange = e => {
    const data = new FormData();
    // using File API to get chosen file
    const file = e.target.files[0];
    // console.log('Uploading file', e.target.files[0]);
    data.append('file', e.target.files[0]);
    //  data.append('name', 'my_file');
    //  data.append('description', 'this file is uploaded by young padawan');

    const url = '/api/uploadDB';

    fetch(url, {
      body: data,
      method: 'POST'
    })
      .then(response => {
        if (response.status === 200) {
          response
            .text()
            .then(result => {
              // console.log('result ID: ' + result);
              const { dadoIdImage } = this.props;
              dadoIdImage(result);
            })
            .then(() => {
              /// console.log(' === === == Executou agora');
            });
        } else {
          const { dadoIdImage } = this.props;
          dadoIdImage('Erro! Arquivo em formato não compatível. Tamanho máximo de 3MB.');
        }
      })
      .catch(err => {
        console.error('ERRO UPLOAD FILE:', err);
        const { dadoIdImage } = this.props;
        dadoIdImage('Erro! Tamanho máximo de 3MB.');
      });
    // return post(url, formData).then(response=> console.warn("result REST: ", response))
  };

  render() {
    return (
      <div>
        <input type="file" name="file_name_html" id="file_id_html" onChange={this.onChange} />
      </div>
    );
  }
}
export default FileUploader;

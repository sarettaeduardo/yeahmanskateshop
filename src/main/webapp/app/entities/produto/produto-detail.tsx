import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './produto.reducer';
import { IProduto } from 'app/shared/model/produto.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProdutoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProdutoDetail extends React.Component<IProdutoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { produtoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="siteLucianaApp.produto.detail.title">Produto</Translate> [<b>{produtoEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nome">
                <Translate contentKey="siteLucianaApp.produto.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{produtoEntity.nome}</dd>
            <dt>
              <span id="descricao">
                <Translate contentKey="siteLucianaApp.produto.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{produtoEntity.descricao}</dd>
            <dt>
              <span id="valor">
                <Translate contentKey="siteLucianaApp.produto.valor">Valor</Translate>
              </span>
            </dt>
            <dd>{produtoEntity.valor}</dd>
            <dt>
              <span id="disponivel">
                <Translate contentKey="siteLucianaApp.produto.disponivel">Disponivel</Translate>
              </span>
            </dt>
            <dd>{produtoEntity.disponivel ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/produto" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/produto/${produtoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ produto }: IRootState) => ({
  produtoEntity: produto.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProdutoDetail);

import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Produtos from './produtos';
import ProdutosDetail from './produtos-detail';
import ProdutosUpdate from './produtos-update';
import ProdutosDeleteDialog from './produtos-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProdutosUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProdutosUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProdutosDetail} />
      <ErrorBoundaryRoute path={match.url} component={Produtos} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProdutosDeleteDialog} />
  </>
);

export default Routes;

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './produtos.reducer';
import { IProdutos } from 'app/shared/model/produtos.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProdutosDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProdutosDetail extends React.Component<IProdutosDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { produtosEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.produtos.detail.title">Produtos</Translate> [<b>{produtosEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="foto0">
                <Translate contentKey="yeahmanskateshopApp.produtos.foto0">Foto 0</Translate>
              </span>
            </dt>
            <dd>
              {produtosEntity.foto0 ? (
                <div>
                  <img src={'data:' + produtosEntity.foto0ContentType + ';base64,' + produtosEntity.foto0} />
                  <a onClick={openFile(produtosEntity.foto0ContentType, produtosEntity.foto0)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                  <span>
                    {produtosEntity.foto0ContentType}, {byteSize(produtosEntity.foto0)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="foto1">
                <Translate contentKey="yeahmanskateshopApp.produtos.foto1">Foto 1</Translate>
              </span>
            </dt>
            <dd>
              {produtosEntity.foto1 ? (
                <div>
                  <img src={'data:' + produtosEntity.foto1ContentType + ';base64,' + produtosEntity.foto1} />
                  <a onClick={openFile(produtosEntity.foto1ContentType, produtosEntity.foto1)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                  <span>
                    {produtosEntity.foto1ContentType}, {byteSize(produtosEntity.foto1)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="foto2">
                <Translate contentKey="yeahmanskateshopApp.produtos.foto2">Foto 2</Translate>
              </span>
            </dt>
            <dd>
              {produtosEntity.foto2 ? (
                <div>
                  <img src={'data:' + produtosEntity.foto2ContentType + ';base64,' + produtosEntity.foto2} />
                  <a onClick={openFile(produtosEntity.foto2ContentType, produtosEntity.foto2)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                  <span>
                    {produtosEntity.foto2ContentType}, {byteSize(produtosEntity.foto2)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="foto3">
                <Translate contentKey="yeahmanskateshopApp.produtos.foto3">Foto 3</Translate>
              </span>
            </dt>
            <dd>
              {produtosEntity.foto3 ? (
                <div>
                  <img src={'data:' + produtosEntity.foto3ContentType + ';base64,' + produtosEntity.foto3} />
                  <a onClick={openFile(produtosEntity.foto3ContentType, produtosEntity.foto3)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                  <span>
                    {produtosEntity.foto3ContentType}, {byteSize(produtosEntity.foto3)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="foto4">
                <Translate contentKey="yeahmanskateshopApp.produtos.foto4">Foto 4</Translate>
              </span>
            </dt>
            <dd>
              {produtosEntity.foto4 ? (
                <div>
                  <img src={'data:' + produtosEntity.foto4ContentType + ';base64,' + produtosEntity.foto4} />
                  <a onClick={openFile(produtosEntity.foto4ContentType, produtosEntity.foto4)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                  <span>
                    {produtosEntity.foto4ContentType}, {byteSize(produtosEntity.foto4)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="valorVenda">
                <Translate contentKey="yeahmanskateshopApp.produtos.valorVenda">Valor Venda</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.valorVenda}</dd>
            <dt>
              <span id="valorCusto">
                <Translate contentKey="yeahmanskateshopApp.produtos.valorCusto">Valor Custo</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.valorCusto}</dd>
            <dt>
              <span id="quantidade">
                <Translate contentKey="yeahmanskateshopApp.produtos.quantidade">Quantidade</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.quantidade}</dd>
            <dt>
              <span id="nome">
                <Translate contentKey="yeahmanskateshopApp.produtos.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.nome}</dd>
            <dt>
              <span id="descricao">
                <Translate contentKey="yeahmanskateshopApp.produtos.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.descricao}</dd>
            <dt>
              <span id="promocao">
                <Translate contentKey="yeahmanskateshopApp.produtos.promocao">Promocao</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.promocao}</dd>
            <dt>
              <span id="destaque">
                <Translate contentKey="yeahmanskateshopApp.produtos.destaque">Destaque</Translate>
              </span>
            </dt>
            <dd>{produtosEntity.destaque ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.produtos.categoria">Categoria</Translate>
            </dt>
            <dd>{produtosEntity.categoria ? produtosEntity.categoria.nome : ''}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.produtos.colecao">Colecao</Translate>
            </dt>
            <dd>{produtosEntity.colecao ? produtosEntity.colecao.nome : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/produtos" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/produtos/${produtosEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ produtos }: IRootState) => ({
  produtosEntity: produtos.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProdutosDetail);

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICategoria } from 'app/shared/model/categoria.model';
import { getEntities as getCategorias } from 'app/entities/categoria/categoria.reducer';
import { IColecao } from 'app/shared/model/colecao.model';
import { getEntities as getColecaos } from 'app/entities/colecao/colecao.reducer';
import { IVenda } from 'app/shared/model/venda.model';
import { getEntities as getVendas } from 'app/entities/venda/venda.reducer';
import { IEntradaCaixa } from 'app/shared/model/entrada-caixa.model';
import { getEntities as getEntradaCaixas } from 'app/entities/entrada-caixa/entrada-caixa.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './produtos.reducer';
import { IProdutos } from 'app/shared/model/produtos.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IProdutosUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProdutosUpdateState {
  isNew: boolean;
  categoriaId: number;
  colecaoId: number;
  vendaId: number;
  entradaCaixaId: number;
}

export class ProdutosUpdate extends React.Component<IProdutosUpdateProps, IProdutosUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      categoriaId: 0,
      colecaoId: 0,
      vendaId: 0,
      entradaCaixaId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getCategorias();
    this.props.getColecaos();
    this.props.getVendas();
    this.props.getEntradaCaixas();
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { produtosEntity } = this.props;
      const entity = {
        ...produtosEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/produtos');
  };

  categoriaUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        categoriaId: -1
      });
    } else {
      for (const i in this.props.categorias) {
        if (id === this.props.categorias[i].id.toString()) {
          this.setState({
            categoriaId: this.props.categorias[i].id
          });
        }
      }
    }
  };

  colecaoUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        colecaoId: -1
      });
    } else {
      for (const i in this.props.colecaos) {
        if (id === this.props.colecaos[i].id.toString()) {
          this.setState({
            colecaoId: this.props.colecaos[i].id
          });
        }
      }
    }
  };

  render() {
    const { produtosEntity, categorias, colecaos, vendas, entradaCaixas, loading, updating } = this.props;
    const { isNew } = this.state;

    const {
      foto0,
      foto0ContentType,
      foto1,
      foto1ContentType,
      foto2,
      foto2ContentType,
      foto3,
      foto3ContentType,
      foto4,
      foto4ContentType
    } = produtosEntity;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.produtos.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.produtos.home.createOrEditLabel">Create or edit a Produtos</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : produtosEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="produtos-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <AvGroup>
                    <Label id="foto0Label" for="foto0">
                      <Translate contentKey="yeahmanskateshopApp.produtos.foto0">Foto 0</Translate>
                    </Label>
                    <br />
                    {foto0 ? (
                      <div>
                        <a onClick={openFile(foto0ContentType, foto0)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {foto0ContentType}, {byteSize(foto0)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('foto0')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_foto0" type="file" onChange={this.onBlobChange(false, 'foto0')} />
                    <AvInput
                      type="hidden"
                      name="foto0"
                      value={foto0}
                      validate={{
                        required: { value: true, errorMessage: translate('entity.validation.required') }
                      }}
                    />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="foto1Label" for="foto1">
                      <Translate contentKey="yeahmanskateshopApp.produtos.foto1">Foto 1</Translate>
                    </Label>
                    <br />
                    {foto1 ? (
                      <div>
                        <a onClick={openFile(foto1ContentType, foto1)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {foto1ContentType}, {byteSize(foto1)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('foto1')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_foto1" type="file" onChange={this.onBlobChange(false, 'foto1')} />
                    <AvInput type="hidden" name="foto1" value={foto1} validate={{}} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="foto2Label" for="foto2">
                      <Translate contentKey="yeahmanskateshopApp.produtos.foto2">Foto 2</Translate>
                    </Label>
                    <br />
                    {foto2 ? (
                      <div>
                        <a onClick={openFile(foto2ContentType, foto2)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {foto2ContentType}, {byteSize(foto2)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('foto2')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_foto2" type="file" onChange={this.onBlobChange(false, 'foto2')} />
                    <AvInput type="hidden" name="foto2" value={foto2} validate={{}} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="foto3Label" for="foto3">
                      <Translate contentKey="yeahmanskateshopApp.produtos.foto3">Foto 3</Translate>
                    </Label>
                    <br />
                    {foto3 ? (
                      <div>
                        <a onClick={openFile(foto3ContentType, foto3)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {foto3ContentType}, {byteSize(foto3)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('foto3')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_foto3" type="file" onChange={this.onBlobChange(false, 'foto3')} />
                    <AvInput type="hidden" name="foto3" value={foto3} validate={{}} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <AvGroup>
                    <Label id="foto4Label" for="foto4">
                      <Translate contentKey="yeahmanskateshopApp.produtos.foto4">Foto 4</Translate>
                    </Label>
                    <br />
                    {foto4 ? (
                      <div>
                        <a onClick={openFile(foto4ContentType, foto4)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                        <br />
                        <Row>
                          <Col md="11">
                            <span>
                              {foto4ContentType}, {byteSize(foto4)}
                            </span>
                          </Col>
                          <Col md="1">
                            <Button color="danger" onClick={this.clearBlob('foto4')}>
                              <FontAwesomeIcon icon="times-circle" />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ) : null}
                    <input id="file_foto4" type="file" onChange={this.onBlobChange(false, 'foto4')} />
                    <AvInput type="hidden" name="foto4" value={foto4} validate={{}} />
                  </AvGroup>
                </AvGroup>
                <AvGroup>
                  <Label id="valorVendaLabel" for="valorVenda">
                    <Translate contentKey="yeahmanskateshopApp.produtos.valorVenda">Valor Venda</Translate>
                  </Label>
                  <AvField
                    id="produtos-valorVenda"
                    type="number"
                    className="form-control"
                    name="valorVenda"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="valorCustoLabel" for="valorCusto">
                    <Translate contentKey="yeahmanskateshopApp.produtos.valorCusto">Valor Custo</Translate>
                  </Label>
                  <AvField
                    id="produtos-valorCusto"
                    type="number"
                    className="form-control"
                    name="valorCusto"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="quantidadeLabel" for="quantidade">
                    <Translate contentKey="yeahmanskateshopApp.produtos.quantidade">Quantidade</Translate>
                  </Label>
                  <AvField
                    id="produtos-quantidade"
                    type="number"
                    className="form-control"
                    name="quantidade"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="yeahmanskateshopApp.produtos.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="produtos-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descricaoLabel" for="descricao">
                    <Translate contentKey="yeahmanskateshopApp.produtos.descricao">Descricao</Translate>
                  </Label>
                  <AvField
                    id="produtos-descricao"
                    type="text"
                    name="descricao"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="promocaoLabel" for="promocao">
                    <Translate contentKey="yeahmanskateshopApp.produtos.promocao">Promocao</Translate>
                  </Label>
                  <AvField id="produtos-promocao" type="number" className="form-control" name="promocao" />
                </AvGroup>
                <AvGroup>
                  <Label id="destaqueLabel" check>
                    <AvInput id="produtos-destaque" type="checkbox" className="form-control" name="destaque" />
                    <Translate contentKey="yeahmanskateshopApp.produtos.destaque">Destaque</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="categoria.id">
                    <Translate contentKey="yeahmanskateshopApp.produtos.categoria">Categoria</Translate>
                  </Label>
                  <AvInput
                    id="produtos-categoria"
                    type="select"
                    className="form-control"
                    name="categoria.id"
                    onChange={this.categoriaUpdate}
                    value={isNew && categorias ? categorias[0] && categorias[0].id : ''}
                  >
                    <option value="" key="0" />
                    {categorias
                      ? categorias.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.nome}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="colecao.id">
                    <Translate contentKey="yeahmanskateshopApp.produtos.colecao">Colecao</Translate>
                  </Label>
                  <AvInput
                    id="produtos-colecao"
                    type="select"
                    className="form-control"
                    name="colecao.id"
                    onChange={this.colecaoUpdate}
                    value={isNew && colecaos ? colecaos[0] && colecaos[0].id : ''}
                  >
                    <option value="" key="0" />
                    {colecaos
                      ? colecaos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.nome}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/produtos" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  categorias: storeState.categoria.entities,
  colecaos: storeState.colecao.entities,
  vendas: storeState.venda.entities,
  entradaCaixas: storeState.entradaCaixa.entities,
  produtosEntity: storeState.produtos.entity,
  loading: storeState.produtos.loading,
  updating: storeState.produtos.updating
});

const mapDispatchToProps = {
  getCategorias,
  getColecaos,
  getVendas,
  getEntradaCaixas,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProdutosUpdate);

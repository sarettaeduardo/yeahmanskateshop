import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { openFile, byteSize, Translate, ICrudGetAllAction, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './produtos.reducer';
import { IProdutos } from 'app/shared/model/produtos.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProdutosProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProdutosState = IPaginationBaseState;

export class Produtos extends React.Component<IProdutosProps, IProdutosState> {
  state: IProdutosState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  reset = () => {
    this.setState({ activePage: 0 }, () => {
      this.props.reset();
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        activePage: 0,
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.reset()
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { produtosList, match } = this.props;
    return (
      <div>
        <h2 id="produtos-heading">
          <Translate contentKey="yeahmanskateshopApp.produtos.home.title">Produtos</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="yeahmanskateshopApp.produtos.home.createLabel">Create new Produtos</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('foto0')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.foto0">Foto 0</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('foto1')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.foto1">Foto 1</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('foto2')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.foto2">Foto 2</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('foto3')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.foto3">Foto 3</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('foto4')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.foto4">Foto 4</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('valorVenda')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.valorVenda">Valor Venda</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('valorCusto')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.valorCusto">Valor Custo</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('quantidade')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.quantidade">Quantidade</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('nome')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.nome">Nome</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('descricao')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.descricao">Descricao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('promocao')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.promocao">Promocao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('destaque')}>
                    <Translate contentKey="yeahmanskateshopApp.produtos.destaque">Destaque</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.produtos.categoria">Categoria</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.produtos.colecao">Colecao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {produtosList.map((produtos, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${produtos.id}`} color="link" size="sm">
                        {produtos.id}
                      </Button>
                    </td>
                    <td>
                      {produtos.foto0 ? (
                        <div>
                          <a onClick={openFile(produtos.foto0ContentType, produtos.foto0)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                          <span>
                            {produtos.foto0ContentType}, {byteSize(produtos.foto0)}
                          </span>
                        </div>
                      ) : null}
                    </td>
                    <td>
                      {produtos.foto1 ? (
                        <div>
                          <a onClick={openFile(produtos.foto1ContentType, produtos.foto1)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                          <span>
                            {produtos.foto1ContentType}, {byteSize(produtos.foto1)}
                          </span>
                        </div>
                      ) : null}
                    </td>
                    <td>
                      {produtos.foto2 ? (
                        <div>
                          <a onClick={openFile(produtos.foto2ContentType, produtos.foto2)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                          <span>
                            {produtos.foto2ContentType}, {byteSize(produtos.foto2)}
                          </span>
                        </div>
                      ) : null}
                    </td>
                    <td>
                      {produtos.foto3 ? (
                        <div>
                          <a onClick={openFile(produtos.foto3ContentType, produtos.foto3)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                          <span>
                            {produtos.foto3ContentType}, {byteSize(produtos.foto3)}
                          </span>
                        </div>
                      ) : null}
                    </td>
                    <td>
                      {produtos.foto4 ? (
                        <div>
                          <a onClick={openFile(produtos.foto4ContentType, produtos.foto4)}>
                            <Translate contentKey="entity.action.open">Open</Translate>
                            &nbsp;
                          </a>
                          <span>
                            {produtos.foto4ContentType}, {byteSize(produtos.foto4)}
                          </span>
                        </div>
                      ) : null}
                    </td>
                    <td>{produtos.valorVenda}</td>
                    <td>{produtos.valorCusto}</td>
                    <td>{produtos.quantidade}</td>
                    <td>{produtos.nome}</td>
                    <td>{produtos.descricao}</td>
                    <td>{produtos.promocao}</td>
                    <td>{produtos.destaque ? 'true' : 'false'}</td>
                    <td>{produtos.categoria ? <Link to={`categoria/${produtos.categoria.id}`}>{produtos.categoria.id}</Link> : ''}</td>
                    <td>{produtos.colecao ? <Link to={`colecao/${produtos.colecao.id}`}>{produtos.colecao.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${produtos.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${produtos.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${produtos.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ produtos }: IRootState) => ({
  produtosList: produtos.entities,
  totalItems: produtos.totalItems,
  links: produtos.links
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Produtos);

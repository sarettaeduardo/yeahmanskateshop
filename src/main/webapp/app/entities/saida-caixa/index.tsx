import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import SaidaCaixa from './saida-caixa';
import SaidaCaixaDetail from './saida-caixa-detail';
import SaidaCaixaUpdate from './saida-caixa-update';
import SaidaCaixaDeleteDialog from './saida-caixa-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SaidaCaixaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SaidaCaixaUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SaidaCaixaDetail} />
      <ErrorBoundaryRoute path={match.url} component={SaidaCaixa} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SaidaCaixaDeleteDialog} />
  </>
);

export default Routes;

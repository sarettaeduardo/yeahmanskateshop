import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './saida-caixa.reducer';
import { ISaidaCaixa } from 'app/shared/model/saida-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISaidaCaixaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SaidaCaixaDetail extends React.Component<ISaidaCaixaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { saidaCaixaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.saidaCaixa.detail.title">SaidaCaixa</Translate> [<b>{saidaCaixaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nome">
                <Translate contentKey="yeahmanskateshopApp.saidaCaixa.nome">Nome</Translate>
              </span>
            </dt>
            <dd>{saidaCaixaEntity.nome}</dd>
            <dt>
              <span id="tipo">
                <Translate contentKey="yeahmanskateshopApp.saidaCaixa.tipo">Tipo</Translate>
              </span>
            </dt>
            <dd>{saidaCaixaEntity.tipo}</dd>
            <dt>
              <span id="descricao">
                <Translate contentKey="yeahmanskateshopApp.saidaCaixa.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{saidaCaixaEntity.descricao}</dd>
            <dt>
              <span id="valor">
                <Translate contentKey="yeahmanskateshopApp.saidaCaixa.valor">Valor</Translate>
              </span>
            </dt>
            <dd>{saidaCaixaEntity.valor}</dd>
            <dt>
              <span id="data">
                <Translate contentKey="yeahmanskateshopApp.saidaCaixa.data">Data</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={saidaCaixaEntity.data} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/saida-caixa" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/saida-caixa/${saidaCaixaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ saidaCaixa }: IRootState) => ({
  saidaCaixaEntity: saidaCaixa.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaidaCaixaDetail);

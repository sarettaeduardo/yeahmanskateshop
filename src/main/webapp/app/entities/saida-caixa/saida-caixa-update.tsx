import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './saida-caixa.reducer';
import { ISaidaCaixa } from 'app/shared/model/saida-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface ISaidaCaixaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISaidaCaixaUpdateState {
  isNew: boolean;
}

export class SaidaCaixaUpdate extends React.Component<ISaidaCaixaUpdateProps, ISaidaCaixaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.data = new Date(values.data);

    if (errors.length === 0) {
      const { saidaCaixaEntity } = this.props;
      const entity = {
        ...saidaCaixaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/saida-caixa');
  };

  render() {
    const { saidaCaixaEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.saidaCaixa.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.saidaCaixa.home.createOrEditLabel">Create or edit a SaidaCaixa</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : saidaCaixaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="saida-caixa-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nomeLabel" for="nome">
                    <Translate contentKey="yeahmanskateshopApp.saidaCaixa.nome">Nome</Translate>
                  </Label>
                  <AvField
                    id="saida-caixa-nome"
                    type="text"
                    name="nome"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="tipoLabel" for="tipo">
                    <Translate contentKey="yeahmanskateshopApp.saidaCaixa.tipo">Tipo</Translate>
                  </Label>
                  <AvField id="saida-caixa-tipo" type="text" name="tipo" />
                </AvGroup>
                <AvGroup>
                  <Label id="descricaoLabel" for="descricao">
                    <Translate contentKey="yeahmanskateshopApp.saidaCaixa.descricao">Descricao</Translate>
                  </Label>
                  <AvField id="saida-caixa-descricao" type="text" name="descricao" />
                </AvGroup>
                <AvGroup>
                  <Label id="valorLabel" for="valor">
                    <Translate contentKey="yeahmanskateshopApp.saidaCaixa.valor">Valor</Translate>
                  </Label>
                  <AvField
                    id="saida-caixa-valor"
                    type="number"
                    className="form-control"
                    name="valor"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataLabel" for="data">
                    <Translate contentKey="yeahmanskateshopApp.saidaCaixa.data">Data</Translate>
                  </Label>
                  <AvInput
                    id="saida-caixa-data"
                    type="datetime-local"
                    className="form-control"
                    name="data"
                    value={isNew ? null : convertDateTimeFromServer(this.props.saidaCaixaEntity.data)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/saida-caixa" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  saidaCaixaEntity: storeState.saidaCaixa.entity,
  loading: storeState.saidaCaixa.loading,
  updating: storeState.saidaCaixa.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaidaCaixaUpdate);

import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ISaidaCaixa, defaultValue } from 'app/shared/model/saida-caixa.model';

export const ACTION_TYPES = {
  FETCH_SAIDACAIXA_LIST: 'saidaCaixa/FETCH_SAIDACAIXA_LIST',
  FETCH_SAIDACAIXA: 'saidaCaixa/FETCH_SAIDACAIXA',
  CREATE_SAIDACAIXA: 'saidaCaixa/CREATE_SAIDACAIXA',
  UPDATE_SAIDACAIXA: 'saidaCaixa/UPDATE_SAIDACAIXA',
  DELETE_SAIDACAIXA: 'saidaCaixa/DELETE_SAIDACAIXA',
  RESET: 'saidaCaixa/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ISaidaCaixa>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type SaidaCaixaState = Readonly<typeof initialState>;

// Reducer

export default (state: SaidaCaixaState = initialState, action): SaidaCaixaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SAIDACAIXA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SAIDACAIXA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SAIDACAIXA):
    case REQUEST(ACTION_TYPES.UPDATE_SAIDACAIXA):
    case REQUEST(ACTION_TYPES.DELETE_SAIDACAIXA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SAIDACAIXA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SAIDACAIXA):
    case FAILURE(ACTION_TYPES.CREATE_SAIDACAIXA):
    case FAILURE(ACTION_TYPES.UPDATE_SAIDACAIXA):
    case FAILURE(ACTION_TYPES.DELETE_SAIDACAIXA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SAIDACAIXA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SAIDACAIXA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SAIDACAIXA):
    case SUCCESS(ACTION_TYPES.UPDATE_SAIDACAIXA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SAIDACAIXA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/saida-caixas';

// Actions

export const getEntities: ICrudGetAllAction<ISaidaCaixa> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SAIDACAIXA_LIST,
  payload: axios.get<ISaidaCaixa>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<ISaidaCaixa> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SAIDACAIXA,
    payload: axios.get<ISaidaCaixa>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ISaidaCaixa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SAIDACAIXA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ISaidaCaixa> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SAIDACAIXA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ISaidaCaixa> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SAIDACAIXA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

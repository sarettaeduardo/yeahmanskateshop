import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './saida-caixa.reducer';
import { ISaidaCaixa } from 'app/shared/model/saida-caixa.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISaidaCaixaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class SaidaCaixa extends React.Component<ISaidaCaixaProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { saidaCaixaList, match } = this.props;
    return (
      <div>
        <h2 id="saida-caixa-heading">
          <Translate contentKey="yeahmanskateshopApp.saidaCaixa.home.title">Saida Caixas</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="yeahmanskateshopApp.saidaCaixa.home.createLabel">Create new Saida Caixa</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.saidaCaixa.nome">Nome</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.saidaCaixa.tipo">Tipo</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.saidaCaixa.descricao">Descricao</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.saidaCaixa.valor">Valor</Translate>
                </th>
                <th>
                  <Translate contentKey="yeahmanskateshopApp.saidaCaixa.data">Data</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {saidaCaixaList.map((saidaCaixa, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${saidaCaixa.id}`} color="link" size="sm">
                      {saidaCaixa.id}
                    </Button>
                  </td>
                  <td>{saidaCaixa.nome}</td>
                  <td>{saidaCaixa.tipo}</td>
                  <td>{saidaCaixa.descricao}</td>
                  <td>{saidaCaixa.valor}</td>
                  <td>
                    <TextFormat type="date" value={saidaCaixa.data} format={APP_DATE_FORMAT} />
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${saidaCaixa.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${saidaCaixa.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${saidaCaixa.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ saidaCaixa }: IRootState) => ({
  saidaCaixaList: saidaCaixa.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SaidaCaixa);

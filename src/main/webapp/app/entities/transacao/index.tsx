import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Transacao from './transacao';
import TransacaoDetail from './transacao-detail';
import TransacaoUpdate from './transacao-update';
import TransacaoDeleteDialog from './transacao-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TransacaoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TransacaoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TransacaoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Transacao} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={TransacaoDeleteDialog} />
  </>
);

export default Routes;

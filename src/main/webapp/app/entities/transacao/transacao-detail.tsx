import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './transacao.reducer';
import { ITransacao } from 'app/shared/model/transacao.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITransacaoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class TransacaoDetail extends React.Component<ITransacaoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { transacaoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="siteLucianaApp.transacao.detail.title">Transacao</Translate> [<b>{transacaoEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="tipo">
                <Translate contentKey="siteLucianaApp.transacao.tipo">Tipo</Translate>
              </span>
            </dt>
            <dd>{transacaoEntity.tipo}</dd>
            <dt>
              <span id="dataGeracao">
                <Translate contentKey="siteLucianaApp.transacao.dataGeracao">Data Geracao</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={transacaoEntity.dataGeracao} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="dataValidade">
                <Translate contentKey="siteLucianaApp.transacao.dataValidade">Data Validade</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={transacaoEntity.dataValidade} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="dataPagamento">
                <Translate contentKey="siteLucianaApp.transacao.dataPagamento">Data Pagamento</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={transacaoEntity.dataPagamento} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="atualizado">
                <Translate contentKey="siteLucianaApp.transacao.atualizado">Atualizado</Translate>
              </span>
            </dt>
            <dd>{transacaoEntity.atualizado}</dd>
            <dt>
              <span id="valorTotal">
                <Translate contentKey="siteLucianaApp.transacao.valorTotal">Valor Total</Translate>
              </span>
            </dt>
            <dd>{transacaoEntity.valorTotal}</dd>
            <dt>
              <Translate contentKey="siteLucianaApp.transacao.pessoa">Pessoa</Translate>
            </dt>
            <dd>{transacaoEntity.pessoa ? transacaoEntity.pessoa.id : ''}</dd>
            <dt>
              <Translate contentKey="siteLucianaApp.transacao.produto">Produto</Translate>
            </dt>
            <dd>{transacaoEntity.produto ? transacaoEntity.produto.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/transacao" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/transacao/${transacaoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ transacao }: IRootState) => ({
  transacaoEntity: transacao.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransacaoDetail);

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPessoa } from 'app/shared/model/pessoa.model';
import { getEntities as getPessoas } from 'app/entities/pessoa/pessoa.reducer';
import { IProduto } from 'app/shared/model/produto.model';
import { getEntities as getProdutos } from 'app/entities/produto/produto.reducer';
import { getEntity, updateEntity, createEntity, reset } from './transacao.reducer';
import { ITransacao } from 'app/shared/model/transacao.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface ITransacaoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ITransacaoUpdateState {
  isNew: boolean;
  pessoaId: number;
  produtoId: number;
}

export class TransacaoUpdate extends React.Component<ITransacaoUpdateProps, ITransacaoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      pessoaId: 0,
      produtoId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPessoas();
    this.props.getProdutos();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { transacaoEntity } = this.props;
      const entity = {
        ...transacaoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/transacao');
  };

  pessoaUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        pessoaId: -1
      });
    } else {
      for (const i in this.props.pessoas) {
        if (id === this.props.pessoas[i].id.toString()) {
          this.setState({
            pessoaId: this.props.pessoas[i].id
          });
        }
      }
    }
  };

  produtoUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        produtoId: -1
      });
    } else {
      for (const i in this.props.produtos) {
        if (id === this.props.produtos[i].id.toString()) {
          this.setState({
            produtoId: this.props.produtos[i].id
          });
        }
      }
    }
  };

  render() {
    const { transacaoEntity, pessoas, produtos, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="siteLucianaApp.transacao.home.createOrEditLabel">
              <Translate contentKey="siteLucianaApp.transacao.home.createOrEditLabel">Create or edit a Transacao</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : transacaoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="transacao-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tipoLabel" for="tipo">
                    <Translate contentKey="siteLucianaApp.transacao.tipo">Tipo</Translate>
                  </Label>
                  <AvField id="transacao-tipo" type="text" name="tipo" />
                </AvGroup>
                <AvGroup>
                  <Label id="dataGeracaoLabel" for="dataGeracao">
                    <Translate contentKey="siteLucianaApp.transacao.dataGeracao">Data Geracao</Translate>
                  </Label>
                  <AvField
                    id="transacao-dataGeracao"
                    type="date"
                    className="form-control"
                    name="dataGeracao"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataValidadeLabel" for="dataValidade">
                    <Translate contentKey="siteLucianaApp.transacao.dataValidade">Data Validade</Translate>
                  </Label>
                  <AvField
                    id="transacao-dataValidade"
                    type="date"
                    className="form-control"
                    name="dataValidade"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataPagamentoLabel" for="dataPagamento">
                    <Translate contentKey="siteLucianaApp.transacao.dataPagamento">Data Pagamento</Translate>
                  </Label>
                  <AvField id="transacao-dataPagamento" type="date" className="form-control" name="dataPagamento" />
                </AvGroup>
                <AvGroup>
                  <Label id="atualizadoLabel" for="atualizado">
                    <Translate contentKey="siteLucianaApp.transacao.atualizado">Atualizado</Translate>
                  </Label>
                  <AvField id="transacao-atualizado" type="text" name="atualizado" />
                </AvGroup>
                <AvGroup>
                  <Label id="valorTotalLabel" for="valorTotal">
                    <Translate contentKey="siteLucianaApp.transacao.valorTotal">Valor Total</Translate>
                  </Label>
                  <AvField id="transacao-valorTotal" type="number" className="form-control" name="valorTotal" />
                </AvGroup>
                <AvGroup>
                  <Label for="pessoa.id">
                    <Translate contentKey="siteLucianaApp.transacao.pessoa">Pessoa</Translate>
                  </Label>
                  <AvInput id="transacao-pessoa" type="select" className="form-control" name="pessoa.id" onChange={this.pessoaUpdate}>
                    <option value="" key="0" />
                    {pessoas
                      ? pessoas.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="produto.id">
                    <Translate contentKey="siteLucianaApp.transacao.produto">Produto</Translate>
                  </Label>
                  <AvInput id="transacao-produto" type="select" className="form-control" name="produto.id" onChange={this.produtoUpdate}>
                    <option value="" key="0" />
                    {produtos
                      ? produtos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/transacao" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  pessoas: storeState.pessoa.entities,
  produtos: storeState.produto.entities,
  transacaoEntity: storeState.transacao.entity,
  loading: storeState.transacao.loading,
  updating: storeState.transacao.updating
});

const mapDispatchToProps = {
  getPessoas,
  getProdutos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransacaoUpdate);

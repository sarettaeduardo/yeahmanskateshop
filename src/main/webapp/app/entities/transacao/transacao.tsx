import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './transacao.reducer';
import { ITransacao } from 'app/shared/model/transacao.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ITransacaoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type ITransacaoState = IPaginationBaseState;

export class Transacao extends React.Component<ITransacaoProps, ITransacaoState> {
  state: ITransacaoState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  reset = () => {
    this.setState({ activePage: 0 }, () => {
      this.props.reset();
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        activePage: 0,
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.reset()
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage, itemsPerPage, `${sort},${order}`);
  };

  download() {
    // fake server request, getting the file url as response
    setTimeout(() => {
      const response = {
        // file: 'https://localhost:8443/api/trans/downloadCSV',
        file: 'https://localhost:8443/api/downloadPDF'
      };
      // server sent the url to the file!
      // now, let's download:
      // window.location.href = response.file;
      // you could also do:
      window.open(response.file);
    }, 100);
  }

  render() {
    const { transacaoList, match } = this.props;
    return (
      <div>
        <h2 id="transacao-heading">
          <Translate contentKey="siteLucianaApp.transacao.home.title">Transacaos</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="siteLucianaApp.transacao.home.createLabel">Create new Transacao</Translate>
          </Link>
        </h2>
        {/*<Button tag={Link} to={`${match.url}/downloadCSV`} color="link" size="sm">
          CSVV2
        </Button>
        <button onClick={this.download}>Download files </button> */}
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('tipo')}>
                    <Translate contentKey="siteLucianaApp.transacao.tipo">Tipo</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataGeracao')}>
                    <Translate contentKey="siteLucianaApp.transacao.dataGeracao">Data Geracao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataValidade')}>
                    <Translate contentKey="siteLucianaApp.transacao.dataValidade">Data Validade</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataPagamento')}>
                    <Translate contentKey="siteLucianaApp.transacao.dataPagamento">Data Pagamento</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('atualizado')}>
                    <Translate contentKey="siteLucianaApp.transacao.atualizado">Atualizado</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('valorTotal')}>
                    <Translate contentKey="siteLucianaApp.transacao.valorTotal">Valor Total</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="siteLucianaApp.transacao.pessoa">Pessoa</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="siteLucianaApp.transacao.produto">Produto</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {transacaoList.map((transacao, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`copiaHTMLtoFile`} color="link" size="sm">
                        PDFV2
                      </Button>
                    </td>
                    <td>{transacao.tipo}</td>
                    <td>
                      <TextFormat type="date" value={transacao.dataGeracao} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={transacao.dataValidade} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={transacao.dataPagamento} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>{transacao.atualizado}</td>
                    <td>{transacao.valorTotal}</td>
                    <td>{transacao.pessoa ? <Link to={`pessoa/${transacao.pessoa.id}`}>{transacao.pessoa.id}</Link> : ''}</td>
                    <td>{transacao.produto ? <Link to={`produto/${transacao.produto.id}`}>{transacao.produto.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${transacao.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${transacao.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${transacao.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ transacao }: IRootState) => ({
  transacaoList: transacao.entities,
  totalItems: transacao.totalItems,
  links: transacao.links
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Transacao);

import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './venda.reducer';
import { IVenda } from 'app/shared/model/venda.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVendaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class VendaDetail extends React.Component<IVendaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { vendaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="yeahmanskateshopApp.venda.detail.title">Venda</Translate> [<b>{vendaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="descricao">
                <Translate contentKey="yeahmanskateshopApp.venda.descricao">Descricao</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.descricao}</dd>
            <dt>
              <span id="codigoTransacao">
                <Translate contentKey="yeahmanskateshopApp.venda.codigoTransacao">Codigo Transacao</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.codigoTransacao}</dd>
            <dt>
              <span id="link">
                <Translate contentKey="yeahmanskateshopApp.venda.link">Link</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.link}</dd>
            <dt>
              <span id="listaItens">
                <Translate contentKey="yeahmanskateshopApp.venda.listaItens">Lista Itens</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.listaItens}</dd>
            <dt>
              <span id="dataGeracao">
                <Translate contentKey="yeahmanskateshopApp.venda.dataGeracao">Data Geracao</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={vendaEntity.dataGeracao} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="dataVencimento">
                <Translate contentKey="yeahmanskateshopApp.venda.dataVencimento">Data Vencimento</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={vendaEntity.dataVencimento} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="total">
                <Translate contentKey="yeahmanskateshopApp.venda.total">Total</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.total}</dd>
            <dt>
              <span id="desconto">
                <Translate contentKey="yeahmanskateshopApp.venda.desconto">Desconto</Translate>
              </span>
            </dt>
            <dd>{vendaEntity.desconto}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.venda.cliente">Cliente</Translate>
            </dt>
            <dd>{vendaEntity.cliente ? vendaEntity.cliente.id : ''}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.venda.endereco">Endereco</Translate>
            </dt>
            <dd>{vendaEntity.endereco ? vendaEntity.endereco.id : ''}</dd>
            <dt>
              <Translate contentKey="yeahmanskateshopApp.venda.produtos">Produtos</Translate>
            </dt>
            <dd>
              {vendaEntity.produtos
                ? vendaEntity.produtos.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.nome}</a>
                      {i === vendaEntity.produtos.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/venda" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/venda/${vendaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ venda }: IRootState) => ({
  vendaEntity: venda.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendaDetail);

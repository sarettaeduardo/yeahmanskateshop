import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICliente } from 'app/shared/model/cliente.model';
import { getEntities as getClientes } from 'app/entities/cliente/cliente.reducer';
import { IEnderecoEntrega } from 'app/shared/model/endereco-entrega.model';
import { getEntities as getEnderecoEntregas } from 'app/entities/endereco-entrega/endereco-entrega.reducer';
import { IProdutos } from 'app/shared/model/produtos.model';
import { getEntities as getProdutos } from 'app/entities/produtos/produtos.reducer';
import { getEntity, updateEntity, createEntity, reset } from './venda.reducer';
import { IVenda } from 'app/shared/model/venda.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IVendaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IVendaUpdateState {
  isNew: boolean;
  idsprodutos: any[];
  clienteId: number;
  enderecoId: number;
}

export class VendaUpdate extends React.Component<IVendaUpdateProps, IVendaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idsprodutos: [],
      clienteId: 0,
      enderecoId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getClientes();
    this.props.getEnderecoEntregas();
    this.props.getProdutos();
  }

  saveEntity = (event, errors, values) => {
    values.dataGeracao = new Date(values.dataGeracao);
    values.dataVencimento = new Date(values.dataVencimento);

    if (errors.length === 0) {
      const { vendaEntity } = this.props;
      const entity = {
        ...vendaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/venda');
  };

  clienteUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        clienteId: -1
      });
    } else {
      for (const i in this.props.clientes) {
        if (id === this.props.clientes[i].id.toString()) {
          this.setState({
            clienteId: this.props.clientes[i].id
          });
        }
      }
    }
  };

  enderecoUpdate = element => {
    const id = element.target.value.toString();
    if (id === '') {
      this.setState({
        enderecoId: -1
      });
    } else {
      for (const i in this.props.enderecoEntregas) {
        if (id === this.props.enderecoEntregas[i].id.toString()) {
          this.setState({
            enderecoId: this.props.enderecoEntregas[i].id
          });
        }
      }
    }
  };

  produtosUpdate = element => {
    const selected = Array.from(element.target.selectedOptions).map((e: any) => parseInt(e.value, 10));
    this.setState({
      idsprodutos: keysToValues(selected, this.props.produtos, 'id')
    });
  };

  displayprodutos(value: any) {
    if (this.state.idsprodutos && this.state.idsprodutos.length !== 0) {
      const list = [];
      for (const i in this.state.idsprodutos) {
        if (this.state.idsprodutos[i]) {
          list.push(this.state.idsprodutos[i].id);
        }
      }
      return list;
    }
    if (value.produtos && value.produtos.length !== 0) {
      const list = [];
      for (const i in value.produtos) {
        if (value.produtos[i]) {
          list.push(value.produtos[i].id);
        }
      }
      this.setState({
        idsprodutos: keysToValues(list, this.props.produtos, 'id')
      });
      return list;
    }
    return null;
  }

  render() {
    const { vendaEntity, clientes, enderecoEntregas, produtos, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="yeahmanskateshopApp.venda.home.createOrEditLabel">
              <Translate contentKey="yeahmanskateshopApp.venda.home.createOrEditLabel">Create or edit a Venda</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : vendaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="venda-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="descricaoLabel" for="descricao">
                    <Translate contentKey="yeahmanskateshopApp.venda.descricao">Descricao</Translate>
                  </Label>
                  <AvField id="venda-descricao" type="text" name="descricao" />
                </AvGroup>
                <AvGroup>
                  <Label id="codigoTransacaoLabel" for="codigoTransacao">
                    <Translate contentKey="yeahmanskateshopApp.venda.codigoTransacao">Codigo Transacao</Translate>
                  </Label>
                  <AvField
                    id="venda-codigoTransacao"
                    type="text"
                    name="codigoTransacao"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="linkLabel" for="link">
                    <Translate contentKey="yeahmanskateshopApp.venda.link">Link</Translate>
                  </Label>
                  <AvField id="venda-link" type="text" name="link" />
                </AvGroup>
                <AvGroup>
                  <Label id="listaItensLabel" for="listaItens">
                    <Translate contentKey="yeahmanskateshopApp.venda.listaItens">Lista Itens</Translate>
                  </Label>
                  <AvField id="venda-listaItens" type="text" name="listaItens" />
                </AvGroup>
                <AvGroup>
                  <Label id="dataGeracaoLabel" for="dataGeracao">
                    <Translate contentKey="yeahmanskateshopApp.venda.dataGeracao">Data Geracao</Translate>
                  </Label>
                  <AvInput
                    id="venda-dataGeracao"
                    type="datetime-local"
                    className="form-control"
                    name="dataGeracao"
                    value={isNew ? null : convertDateTimeFromServer(this.props.vendaEntity.dataGeracao)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="dataVencimentoLabel" for="dataVencimento">
                    <Translate contentKey="yeahmanskateshopApp.venda.dataVencimento">Data Vencimento</Translate>
                  </Label>
                  <AvInput
                    id="venda-dataVencimento"
                    type="datetime-local"
                    className="form-control"
                    name="dataVencimento"
                    value={isNew ? null : convertDateTimeFromServer(this.props.vendaEntity.dataVencimento)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="totalLabel" for="total">
                    <Translate contentKey="yeahmanskateshopApp.venda.total">Total</Translate>
                  </Label>
                  <AvField id="venda-total" type="number" className="form-control" name="total" />
                </AvGroup>
                <AvGroup>
                  <Label id="descontoLabel" for="desconto">
                    <Translate contentKey="yeahmanskateshopApp.venda.desconto">Desconto</Translate>
                  </Label>
                  <AvField id="venda-desconto" type="number" className="form-control" name="desconto" />
                </AvGroup>
                <AvGroup>
                  <Label for="cliente.id">
                    <Translate contentKey="yeahmanskateshopApp.venda.cliente">Cliente</Translate>
                  </Label>
                  <AvInput
                    id="venda-cliente"
                    type="select"
                    className="form-control"
                    name="cliente.id"
                    onChange={this.clienteUpdate}
                    value={isNew && clientes ? clientes[0] && clientes[0].id : ''}
                  >
                    <option value="" key="0" />
                    {clientes
                      ? clientes.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="endereco.id">
                    <Translate contentKey="yeahmanskateshopApp.venda.endereco">Endereco</Translate>
                  </Label>
                  <AvInput
                    id="venda-endereco"
                    type="select"
                    className="form-control"
                    name="endereco.id"
                    onChange={this.enderecoUpdate}
                    value={isNew && enderecoEntregas ? enderecoEntregas[0] && enderecoEntregas[0].id : ''}
                  >
                    <option value="" key="0" />
                    {enderecoEntregas
                      ? enderecoEntregas.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="produtos">
                    <Translate contentKey="yeahmanskateshopApp.venda.produtos">Produtos</Translate>
                  </Label>
                  <AvInput
                    id="venda-produtos"
                    type="select"
                    multiple
                    className="form-control"
                    name="fakeprodutos"
                    value={this.displayprodutos(vendaEntity)}
                    onChange={this.produtosUpdate}
                  >
                    <option value="" key="0" />
                    {produtos
                      ? produtos.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.nome}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvInput id="venda-produtos" type="hidden" name="produtos" value={this.state.idsprodutos} />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/venda" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  clientes: storeState.cliente.entities,
  enderecoEntregas: storeState.enderecoEntrega.entities,
  produtos: storeState.produtos.entities,
  vendaEntity: storeState.venda.entity,
  loading: storeState.venda.loading,
  updating: storeState.venda.updating
});

const mapDispatchToProps = {
  getClientes,
  getEnderecoEntregas,
  getProdutos,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendaUpdate);

import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './venda.reducer';
import { IVenda } from 'app/shared/model/venda.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IVendaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IVendaState = IPaginationBaseState;

export class Venda extends React.Component<IVendaProps, IVendaState> {
  state: IVendaState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  reset = () => {
    this.setState({ activePage: 0 }, () => {
      this.props.reset();
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        activePage: 0,
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.reset()
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { vendaList, match } = this.props;
    return (
      <div>
        <h2 id="venda-heading">
          <Translate contentKey="yeahmanskateshopApp.venda.home.title">Vendas</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="yeahmanskateshopApp.venda.home.createLabel">Create new Venda</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('descricao')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.descricao">Descricao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('codigoTransacao')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.codigoTransacao">Codigo Transacao</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('link')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.link">Link</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('listaItens')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.listaItens">Lista Itens</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataGeracao')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.dataGeracao">Data Geracao</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('dataVencimento')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.dataVencimento">Data Vencimento</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('total')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.total">Total</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('desconto')}>
                    <Translate contentKey="yeahmanskateshopApp.venda.desconto">Desconto</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.venda.cliente">Cliente</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="yeahmanskateshopApp.venda.endereco">Endereco</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {vendaList.map((venda, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${venda.id}`} color="link" size="sm">
                        {venda.id}
                      </Button>
                    </td>
                    <td>{venda.descricao}</td>
                    <td>{venda.codigoTransacao}</td>
                    <td>{venda.link}</td>
                    <td>{venda.listaItens}</td>
                    <td>
                      <TextFormat type="date" value={venda.dataGeracao} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={venda.dataVencimento} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{venda.total}</td>
                    <td>{venda.desconto}</td>
                    <td>{venda.cliente ? <Link to={`cliente/${venda.cliente.id}`}>{venda.cliente.id}</Link> : ''}</td>
                    <td>{venda.endereco ? <Link to={`enderecoEntrega/${venda.endereco.id}`}>{venda.endereco.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${venda.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${venda.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${venda.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ venda }: IRootState) => ({
  vendaList: venda.entities,
  totalItems: venda.totalItems,
  links: venda.links
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Venda);

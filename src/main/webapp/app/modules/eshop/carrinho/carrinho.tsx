import './carrinho.css';
import React from 'react';
import { Row, Fade, Alert, Button } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export interface ICarrinhoProp {
  produtoList: any[];
  removeItenCarrinho: Function;
}

export interface ICarrinhoState {
  // imagem: string;
  isManagingFocus: boolean;
  carrinhoCompras: string;
  totalCarrinho: number;
}

export class Carrinho extends React.Component<ICarrinhoProp, ICarrinhoState> {
  state: ICarrinhoState = {
    isManagingFocus: false,
    carrinhoCompras: 'carrinhoComprasSmall',
    totalCarrinho: 0
  };

  _onBlur = () => {
    if (this.state.isManagingFocus) {
      setTimeout(() => {
        this.setState({
          isManagingFocus: false,
          carrinhoCompras: 'carrinhoComprasSmall'
        });
      }, 5000);
    }
  };

  _openCar = () => {
    setTimeout(() => {
      this.setState({
        isManagingFocus: !this.state.isManagingFocus,
        carrinhoCompras: this.state.isManagingFocus ? 'carrinhoComprasSmall' : 'carrinhoCompras'
      });
    }, 0);
  };

  removeItem = idKey => () => {
    // console.log(JSON.stringify(idKey));
    const { removeItenCarrinho } = this.props;
    removeItenCarrinho(idKey);
  };

  createListaItens() {
    const gridProd = [];
    gridProd.push(<br />);
    this.state.totalCarrinho = 0;
    if (this.props.produtoList.length === 0) {
      gridProd.push(<Alert color="success">Carrinho esta vazio</Alert>);
    } else {
      for (const i of this.props.produtoList) {
        // Não rolo, compilador não deixou
        // for (let i = 0; i < this.props.produtoList.length; i++) {
        gridProd.push(
          <Row className="cart-item">
            <div>
              <button onClick={this.removeItem(i.id)} className="btn btn-danger buttonSmall">
                X
              </button>
              <span className="cart-item__name">{i.nome.length <= 14 ? i.nome : i.nome.substring(0, 14).concat('...')}</span>
            </div>
            <div className="cart-item__price">R$ {i.valor},00</div>
          </Row>
        );
        this.state.totalCarrinho += i.valor;
      }
    }
    return gridProd;
  }

  componentWillMount() {
    // console.log('RECARREGOU A PAGINA! [componentWillMount()] ');
  }

  componentDidMount() {
    // console.log('CHAMA PARA RECARREGAR NO FIM A PAGINA! [componentDidMount()] ');
  }

  goTop = () => {
    window.scrollTo(0, 0);
  };

  render() {
    return (
      <div className="carrinhoIndex">
        <div className={this.state.carrinhoCompras} id="carrinhoDiv">
          <Fade
            in={this.props.produtoList.length === 0 ? false : true}
            className={this.state.isManagingFocus ? 'noneDiv notificationItens' : 'showDiv notificationItens'}
          >
            {this.props.produtoList.length}
          </Fade>
          <button className="btn btn-success pcarrinho" onClick={this._openCar}>
            <img src="content/images/cart_icon.png" width="25" height="25" />
            {this.state.isManagingFocus ? 'Meu Carrinho' : ''}
          </button>
          <div className={this.state.isManagingFocus ? 'showDiv' : 'noneDiv'}>
            {this.createListaItens()}

            <br />
            <div className="cart__total">Total: R$ {this.state.totalCarrinho},00</div>
            <Button tag={Link} to="/compra" onClick={this.goTop} className="btn btn-success">
              Ir ao Carrinho
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
export default Carrinho;

import './carrinho-frete.css';
import React from 'react';
import { Row, Col, Alert, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

export interface ICarrinhoFreteProp {
  setStep: Function;
}

export interface ICarrinhoFreteState {
  carrinhoLista: any[];
  produtoList: any[];
  totalCarrinho: number;
  valorFrete: number;
}

export class CarrinhoFrete extends React.Component<ICarrinhoFreteProp, ICarrinhoFreteState> {
  state: ICarrinhoFreteState = {
    carrinhoLista: [],
    totalCarrinho: 0,
    produtoList: [],
    valorFrete: 68
  };

  createListaItens() {
    const gridProd = [];
    this.state.totalCarrinho = 0;
    if (this.state.produtoList.length === 0) {
      gridProd.push(
        <tr>
          <br />
          <Alert color="success">Carrinho esta vazio</Alert>
        </tr>
      );
    } else {
      for (const i of this.state.produtoList) {
        // Não rolo, compilador não deixou
        // for (let i = 0; i < this.props.produtoList.length; i++) {
        gridProd.push(
          <Row className="rowProd">
            <Col xs="12" sm="6" md="6">
              <div className="media">
                <Link className="thumbnail pull-left" to={'/produto/' + i.id}>
                  <img className="imgListCompra" src={'data:image/jpeg;base64,' + i.imagem} />
                </Link>
                <div className="media-body">
                  <h4 className="media-heading">{i.nome}</h4>
                  <span>Status: </span>
                  <span className="text-success">
                    <strong>Em Estoque</strong>
                  </span>
                </div>
              </div>
            </Col>
            <Col xs="3" sm="2" className="unidades">
              {/* style="text-align: center">*/}
              Unidades:
              <input disabled type="email" className="form-control text-center" value="1" />
            </Col>
            <Col xs="6" sm="2" className="text-center font14">
              Valor:<br />
              <strong className="font16">R${i.valor},00</strong>
            </Col>
            <Col xs="3" sm="2">
              <Button type="button" onClick={this.removeProdutoCarrinho(i.id)} className="btn btn-danger fontButton18 font14">
                <strong className="showRemove">Remover</strong> <img src="content/images/excluir_lixo.png" width="16" height="15" />
              </Button>
            </Col>
          </Row>
        );
        this.state.totalCarrinho += i.valor;
      }
    }
    return gridProd;
  }

  removeProdutoCarrinho = produtoListKey => () => {
    if (this.state.carrinhoLista.length > 0) {
      for (let key = 0; key < this.state.carrinhoLista.length; key++) {
        if (this.state.carrinhoLista[key].id === produtoListKey) {
          this.state.carrinhoLista.splice(key, 1);
          this.setState({
            carrinhoLista: this.state.carrinhoLista
          });
          localStorage.setItem('myCartList', JSON.stringify(this.state.carrinhoLista));
          break;
        }
      }
    }
  };

  componentWillMount() {
    if (localStorage.getItem('myCartList') !== null) {
      this.state.carrinhoLista = JSON.parse(localStorage.getItem('myCartList'));
      this.setState({
        produtoList: this.state.carrinhoLista
      });
    }
  }

  nextStep = step => () => {
    this.props.setStep(step);
    window.scrollTo(0, 0);
  };

  render() {
    return (
      <div>
        <br />
        <br />
        <div className="containerCompras">
          <Row>
            <div className="col-sm-12 col-md-10 col-md-offset-1 tabelaCompras">
              {this.createListaItens()}
              <Row>
                <Col />
                <Col>
                  <h5>Subtotal</h5>
                </Col>
                <Col className="text-right">
                  <h5>
                    <strong>R${this.state.totalCarrinho},00</strong>
                  </h5>
                </Col>
                <Col />
              </Row>
              <Row>
                <Col />
                <Col>
                  <h5>Valor de Envio (Valor fixo)</h5>
                </Col>
                <Col className="text-right">
                  <h5>
                    <strong>R${this.state.valorFrete},00</strong>
                  </h5>
                </Col>
                <Col />
              </Row>
              <Row>
                <Col />
                <Col>
                  <h3>Total</h3>
                </Col>
                <Col className="text-right">
                  <h3>
                    <strong>R${this.state.totalCarrinho + this.state.valorFrete},00</strong>
                  </h3>
                </Col>
                <Col />
              </Row>
              <Row>
                <Col>
                  <Button tag={Link} to="/eshop" onClick={window.scrollTo(0, 0)} className="btn btn-default fontButton18">
                    <img src="content/images/cart_icon_wite.png" width="20" height="20" /> Continuar Comprando
                  </Button>
                </Col>
                <Col>
                  <Button
                    onClick={this.nextStep(2)}
                    className="btn btn-success fontButton18"
                    disabled={this.state.totalCarrinho === 0 ? true : false}
                  >
                    Finalizar Compra <span className="glyphicon glyphicon-play" />
                  </Button>
                </Col>
              </Row>
            </div>
          </Row>
        </div>
      </div>
    );
  }
}
export default CarrinhoFrete;

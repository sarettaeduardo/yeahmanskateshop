import './compra.css';
import React from 'react';
import { Row, Alert, Button } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import { CarrinhoFrete } from 'app/modules/eshop/compra/carrinhoFrete/carrinho-frete';
import { DadosComprador } from 'app/modules/eshop/compra/dadosComprador/dados-comprador';
import { DadosEnvio } from 'app/modules/eshop/compra/dadosEnvio/dados-envio';
import { DadosTransacao } from 'app/modules/eshop/compra/dadosTransacao/dados-transacao';
import { CompraSucesso } from 'app/modules/eshop/compra/compraSucesso/compra-sucesso';

export interface ICompraState {
  step: number;
  cliente: {
    nome: string;
    sobrenome: string;
    telefone: string;
    email: string;
    cpf: string;
  };
  id_transacao: string;
  link_boleto: string;
}

export class Compra extends React.Component<ICompraState> {
  state: ICompraState = {
    step: 1,
    cliente: {
      nome: 'Bob',
      sobrenome: 'Marley',
      telefone: '(51) 9802-4455',
      email: 'asdasdasd@gmail.com',
      cpf: '012.345.678-90'
    },
    id_transacao: '0000',
    link_boleto: ''
  };

  stepControl = novoStep => {
    this.setState({
      step: novoStep
    });
  };

  setCliente = (clienteIn, id_transacaoIn, link_boletoIn) => {
    this.setState({
      cliente: clienteIn,
      id_transacao: id_transacaoIn,
      link_boleto: link_boletoIn
    });
  };

  componentWillMount() {}

  render() {
    document.title = 'Carrinho de Compras - Loja Balance Board';
    const _desc =
      'Carrinho de compras, sua lista de produtos, frete e acessorios. Aceitamos cartão de crédito em parcelamento até 12x sem juros.';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        {this.state.step === 1 && <CarrinhoFrete setStep={this.stepControl} />}
        {this.state.step === 2 && <DadosComprador setStep={this.stepControl} />}
        {this.state.step === 3 && <DadosEnvio setStep={this.stepControl} />}
        {this.state.step === 4 && <DadosTransacao setStep={this.stepControl} setCliente={this.setCliente} />}
        {this.state.step === 5 && (
          <CompraSucesso
            setStep={this.stepControl}
            cliente={this.state.cliente}
            id_transacao={this.state.id_transacao}
            link_boleto={this.state.link_boleto}
          />
        )}
      </div>
    );
  }
}
export default Compra;
// https://bootsnipp.com/snippets/0ByGK

import './compra-sucesso.css';
import React from 'react';
import { translate } from 'react-jhipster';
import { Button, Alert, Badge, Row, Col } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export interface ICompraSucessoProp {
  setStep: Function;
  cliente: {
    nome: string;
    sobrenome: string;
    telefone: string;
    email: string;
    cpf: string;
  };
  id_transacao: string;
  link_boleto: string;
}

export interface ICompraSucessoState {
  cpfForm: string;
}

export class CompraSucesso extends React.Component<ICompraSucessoProp, ICompraSucessoState> {
  state: ICompraSucessoState = {
    cpfForm: ''
  };

  componentWillMount() {}

  render() {
    return (
      <div className="containerSucessoCompra textoSucesso">
        <br />
        <Row className="alingCenter">
          <Col>
            <Alert color="primary" className="tituloSucesso">
              Pedido nº {this.props.id_transacao} confirmado.
            </Alert>
          </Col>
        </Row>
        <Row>
          <b>{this.props.cliente.nome + ' ' + this.props.cliente.sobrenome},</b>
        </Row>
        <Row>
          <Col>
            Obrigado por comprar com YeahMan Skate Shop, seu pedido de nº <b>{this.props.id_transacao}</b> foi recebido e logo será
            despachado.
            <br />Entraremos em contato através do WhatsApp e E-mail.
          </Col>
        </Row>
        <Row className="alingLeft">
          <Col sm="2">WhatsApp:</Col>
          <Col>
            <b> {this.props.cliente.telefone}</b>
          </Col>
        </Row>
        <Row className="alingLeft">
          <Col sm="2">E-Mail:</Col>
          <Col>
            <b>{this.props.cliente.email}</b>
          </Col>
        </Row>
        <hr className="my-2" />
        <Row className="alingCenter">
          <Col>
            <Alert color="primary" className="tituloSucessoRodape">
              Enviaremos e-mails com status da sua compra!
            </Alert>
            <Alert color="warning">
              Um usuário foi criado para você acompanhar seu pedido. As credênciais serão enviadas para o seu e-mail.
            </Alert>
            {this.props.link_boleto !== '' && (
              <Alert className="tituloSucessoRodape20" color="success">
                Imprimir Boleto:{' '}
                <a href={this.props.link_boleto} target="_blank">
                  {this.props.link_boleto}
                </a>
              </Alert>
            )}
          </Col>
        </Row>
        <Row className="alingCenter">
          <Col>
            <Button tag={Link} to="/home" color="primary">
              Concluído
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default CompraSucesso;

import React from 'react';
import { translate } from 'react-jhipster';
import { Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { NavLink as Link } from 'react-router-dom';

export interface IDadosCompradorProp {
  setStep: Function;
}

export interface IDadosCompradorState {
  cliente: {
    nome: string;
    sobrenome: string;
    telefone: string;
    email: string;
    cpf: string;
  };
  telefoneForm: string;
  cpfForm: string;
}

export class DadosComprador extends React.Component<IDadosCompradorProp, IDadosCompradorState> {
  state: IDadosCompradorState = {
    cliente: {
      nome: '',
      sobrenome: '',
      telefone: '',
      email: '',
      cpf: ''
    },
    telefoneForm: '',
    cpfForm: ''
  };

  handleSubmit = (event, errors, { nomeForm, sobrenomeForm, emailForm, telefoneForm, cpfForm }) => {
    if (errors.length > 0) {
      console.error('erro: ' + errors);
    } else if (!this.regExp(telefoneForm)) {
      alert('Telefone inválido');
    } else if (this.TestaCPF(cpfForm)) {
      const dados = {
        nome: nomeForm,
        sobrenome: sobrenomeForm,
        email: emailForm,
        telefone: telefoneForm,
        cpf: cpfForm
      };

      // console.log(dados);

      if (this.salvaDadosCliente(dados)) {
        this.props.setStep(3);
        window.scrollTo(0, 0);
      } else {
        alert('Erro ao salvar seus dados!');
      }
    } else {
      alert('CPF inválido');
    }
  };

  salvaDadosCliente(dados) {
    localStorage.setItem('myClient', JSON.stringify(dados));
    return true;
  }

  TestaCPF(cpf) {
    cpf = cpf.replace('.', '');
    cpf = cpf.replace('.', '');
    cpf = cpf.replace('-', '');
    // console.log(cpf);
    let Soma = 0;
    let Resto;
    if (
      cpf === '00000000000' ||
      cpf === '11111111111' ||
      cpf === '22222222222' ||
      cpf === '33333333333' ||
      cpf === '44444444444' ||
      cpf === '55555555555' ||
      cpf === '66666666666' ||
      cpf === '77777777777' ||
      cpf === '88888888888' ||
      cpf === '99999999999'
    ) {
      return false;
    }
    for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);
    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(cpf.substring(9, 10), 10)) return false;
    Soma = 0;
    for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);
    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(cpf.substring(10, 11), 10)) return false;
    return true;
  }

  componentWillMount() {
    if (localStorage.getItem('myClient') !== null) {
      const auxIn = JSON.parse(localStorage.getItem('myClient'));
      this.setState({
        cliente: auxIn,
        telefoneForm: auxIn.telefone,
        cpfForm: auxIn.cpf
      });
    }
  }

  nextStep = step => () => {
    this.props.setStep(step);
    window.scrollTo(0, 0);
  };

  regExp(telefone) {
    // const RegExp = /\(\d{2}\)\s\d{4,5}-?\d{4}/g;
    let RegExp = /^\([1-9]{2}\) [2-9][0-9]{3,4}\-[0-9]{4}$/;
    if (telefone.length > 14) {
      RegExp = /^\([1-9]{2}\) [9][0-9]{3,4}\-[0-9]{4}$/;
    }

    // const RegExpDois = /^[1-9]{2}9?[0-9]{8}/;
    const t = telefone;
    // console.log('RegExp2: ' + RegExpDois.test(t));
    return RegExp.test(t) ? true : false;
  }

  maskTelefone(event, val) {
    let resultTel = val;
    resultTel = resultTel.replace(/\D/g, ''); // Remove tudo o que não é dígito
    resultTel = resultTel.replace(/^(\d\d)(\d)/g, '($1) $2'); // Coloca parênteses em volta dos dois primeiros dígitos
    resultTel.length < 14
      ? (resultTel = resultTel.replace(/(\d{4})(\d)/, '$1-$2')) // Número com 8 dígitos. Formato: (99) 9999-9999
      : (resultTel = resultTel.replace(/(\d{5})(\d)/, '$1-$2')); // Número com 9 dígitos. Formato: (99) 99999-9999
    this.setState({ telefoneForm: resultTel });
  }

  maskCPF(event, valCPF) {
    let result = valCPF;
    result = result.replace(/\D/g, '');
    result = result.replace(/(\d{3})(\d)/, '$1.$2');
    result = result.replace(/(\d{3})(\d)/, '$1.$2');
    result = result.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
    this.setState({ cpfForm: result });
  }

  render() {
    this.maskTelefone = this.maskTelefone.bind(this);
    this.maskCPF = this.maskCPF.bind(this);

    return (
      <div>
        <br />
        <AvForm id="register-form" className="formContainer formClientePaddin" onSubmit={this.handleSubmit}>
          <AvField
            value={this.state.cliente.nome}
            name="nomeForm"
            label="Nome"
            placeholder="Seu primeiro nome"
            validate={{
              required: { value: true, errorMessage: 'Nome é obrigatório!' },
              minLength: { value: 1, errorMessage: 'Nome pequeno demais!' },
              maxLength: { value: 70, errorMessage: 'Nome muito grande!' }
            }}
          />
          <AvField
            value={this.state.cliente.sobrenome}
            name="sobrenomeForm"
            label="Sobrenome"
            placeholder="Seu sobrenome"
            validate={{
              required: { value: true, errorMessage: 'Sobrenome é obrigatório!' },
              minLength: { value: 1, errorMessage: 'Sobrenome pequeno demais!' },
              maxLength: { value: 100, errorMessage: 'Sobrenome muito grande!' }
            }}
          />
          <AvField
            value={this.state.cliente.email}
            name="emailForm"
            label={translate('global.form.email')}
            placeholder={translate('global.form.email.placeholder')}
            type="email"
            validate={{
              required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
              minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
              maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') }
            }}
          />
          <AvField
            value={this.state.cpfForm}
            name="cpfForm"
            id="cpf"
            label="CPF"
            placeholder="Seu CPF"
            onChange={this.maskCPF}
            validate={{
              required: { value: true, errorMessage: 'O CPF é obrigatório' },
              minLength: { value: 14, errorMessage: 'O CPF é obrigatório' },
              maxLength: { value: 14, errorMessage: 'O CPF é obrigatório' }
            }}
          />
          <AvField
            value={this.state.telefoneForm}
            name="telefoneForm"
            label="Telefone"
            placeholder="Seu telefone"
            onChange={this.maskTelefone}
            validate={{
              required: { value: true, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' },
              minLength: { value: 14, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' },
              maxLength: { value: 15, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' }
            }}
          />
          <Button onClick={this.nextStep(1)} className="btn btn-cancel fontButton18">
            Voltar
          </Button>{' '}
          <Button type="submit" className="btn btn-success fontButton18">
            Concluído<span className="glyphicon glyphicon-play" />
          </Button>
        </AvForm>
      </div>
    );
  }
}
export default DadosComprador;

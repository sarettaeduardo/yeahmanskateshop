import React from 'react';
import { Row, Col, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { NavLink as Link } from 'react-router-dom';

export interface IDadosEnvioProp {
  setStep: Function;
}

export interface IDadosEnvioState {
  endereco: {
    rua: string;
    numero: string;
    bairro: string;
    complemento: string;
    cidade: string;
    cep: string;
    estado: string;
    pais: string;
    uf: string;
  };
  cepForm: string;
  ufForm: string;
}

export class DadosEnvio extends React.Component<IDadosEnvioProp, IDadosEnvioState> {
  state: IDadosEnvioState = {
    endereco: {
      rua: '',
      numero: '',
      bairro: '',
      complemento: '',
      cidade: '',
      cep: '',
      estado: '',
      pais: '',
      uf: ''
    },
    cepForm: '',
    ufForm: ''
  };

  handleSubmit = (
    event,
    errors,
    { ruaForm, numeroForm, bairroForm, complementoForm, cidadeForm, cepForm, estadoForm, paisForm, ufForm }
  ) => {
    if (errors.length > 0) {
      // console.log('erro: ' + errors);
    } else {
      const dados = {
        rua: ruaForm,
        numero: numeroForm,
        bairro: bairroForm,
        complemento: complementoForm,
        cidade: cidadeForm,
        cep: cepForm,
        estado: estadoForm,
        pais: paisForm,
        uf: ufForm
      };

      // console.log(dados);

      if (this.salvaDadosEndereco(dados)) {
        this.props.setStep(4);
        window.scrollTo(0, 0);
      }
    }
  };

  salvaDadosEndereco(dados) {
    localStorage.setItem('myEndereco', JSON.stringify(dados));
    return true;
  }

  componentWillMount() {
    if (localStorage.getItem('myEndereco') !== null) {
      const end = JSON.parse(localStorage.getItem('myEndereco'));
      this.setState({
        endereco: end,
        cepForm: end.cep,
        ufForm: end.uf
      });
    }
  }

  nextStep = step => () => {
    this.props.setStep(step);
    window.scrollTo(0, 0);
  };

  maskCep(event, val) {
    let cep = val;
    cep = cep.replace(/\D/g, '');
    cep = cep.replace(/^(\d{2})(\d)/, '$1.$2');
    cep = cep.replace(/\.(\d{3})(\d)/, '.$1-$2');
    this.setState({ cepForm: cep });
  }

  maskUF(event, val) {
    let res = val;
    res = res.toUpperCase();
    this.setState({ ufForm: res });
  }

  render() {
    this.maskCep = this.maskCep.bind(this);
    this.maskUF = this.maskUF.bind(this);

    return (
      <div>
        <br />
        <AvForm id="register-form" className="formContainer" onSubmit={this.handleSubmit}>
          <Row>
            <Col md="9">
              <AvField
                value={this.state.endereco.rua}
                name="ruaForm"
                label="Endereço"
                placeholder="Nome da sua rua"
                validate={{
                  required: { value: true, errorMessage: 'Rua é obrigatório!' },
                  minLength: { value: 1, errorMessage: 'Rua pequeno demais!' },
                  maxLength: { value: 150, errorMessage: 'Rua muito grande!' }
                }}
              />
            </Col>
            <Col md="3">
              <AvField
                value={this.state.endereco.numero}
                name="numeroForm"
                label="Numero"
                type="number"
                placeholder="Numero do seu endereço"
                validate={{
                  required: { value: true, errorMessage: 'Numero é obrigatório!' }
                }}
              />
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <AvField
                value={this.state.endereco.bairro}
                name="bairroForm"
                label="Bairro"
                placeholder="Seu Bairro"
                validate={{
                  required: { value: true, errorMessage: 'Bairro é obrigatório!' },
                  minLength: { value: 1, errorMessage: 'Bairro pequeno demais!' },
                  maxLength: { value: 200, errorMessage: 'Bairro muito grande!' }
                }}
              />
            </Col>
            <Col md="6">
              <AvField
                value={this.state.endereco.complemento}
                name="complementoForm"
                label="Complemento"
                placeholder="Nº apartamento, casa, condomínio..."
              />
            </Col>
          </Row>
          <Row>
            <Col md="8">
              <AvField
                value={this.state.endereco.cidade}
                name="cidadeForm"
                label="Cidade"
                placeholder="Sua Cidade"
                validate={{
                  required: { value: true, errorMessage: 'Cidade é obrigatório!' },
                  minLength: { value: 1, errorMessage: 'Cidade pequeno demais!' },
                  maxLength: { value: 250, errorMessage: 'Cidade muito grande!' }
                }}
              />
            </Col>
            <Col md="4">
              <AvField
                value={this.state.cepForm}
                name="cepForm"
                onChange={this.maskCep}
                label="CEP"
                placeholder="Seu CEP"
                validate={{
                  required: { value: true, errorMessage: 'CEP é obrigatório!' },
                  minLength: { value: 10, errorMessage: 'CEP pequeno demais!' },
                  maxLength: { value: 10, errorMessage: 'CEP muito grande!' }
                }}
              />
            </Col>
          </Row>
          <Row>
            <Col md="8">
              <AvField
                value={this.state.ufForm}
                onChange={this.maskUF}
                name="ufForm"
                label="Estado (UF)"
                placeholder="UF do seu estado"
                validate={{
                  required: { value: true, errorMessage: 'UF é obrigatório!' },
                  minLength: { value: 2, errorMessage: 'UF pequeno demais!' },
                  maxLength: { value: 2, errorMessage: 'UF muito grande!' }
                }}
              />
            </Col>
            <Col ms="4">
              <AvField name="paisForm" label="Pais" placeholder="Pais" disabled value="Brasil" />
            </Col>
          </Row>
          <Button onClick={this.nextStep(2)} className="btn btn-cancel fontButton18">
            Voltar
          </Button>{' '}
          <Button type="submit" className="btn btn-success fontButton18">
            Concluído<span className="glyphicon glyphicon-play" />
          </Button>
        </AvForm>
      </div>
    );
  }
}
export default DadosEnvio;

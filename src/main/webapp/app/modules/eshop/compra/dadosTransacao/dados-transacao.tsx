import './dados-transacao.css';
import React from 'react';
import {
  Row,
  DropdownMenu,
  ButtonDropdown,
  DropdownToggle,
  DropdownItem,
  Col,
  Button,
  ListGroupItem,
  ListGroup,
  InputGroupAddon,
  Label,
  InputGroup,
  Input
} from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import './paymetToken.js';
import endereco from 'app/entities/endereco/endereco';

declare global {
  interface Window {
    myPaymentToken: Function;
    myInstallments: Function;
  }
}

export interface IDadosTransacaoProp {
  setStep: Function;
  setCliente: Function;
}

export interface IDadosTransacaoState {
  card_mask: string;
  payment_token: string;
  totalCarrinho: number;
  tipoTransacao: string;
  maxNumeroCartao: number;
  maxdigitoCartao: number;
  cssIcard: string;
  endereco: {
    rua: string;
    numero: string;
    bairro: string;
    complemento: string;
    cidade: string;
    cep: string;
    estado: string;
    pais: string;
    uf: string;
  };
  cliente: {
    nome: string;
    sobrenome: string;
    telefone: string;
    email: string;
    cpf: string;
  };
  carrinhoLista: any[];
  carrinhoListaIds: any[];
  cartForm: string;
  nomeForm: string;
  desabilitaBotao: boolean;
  exibeBotaoParcelas: boolean;
  exibeTransacionando: boolean;
  valorFrete: number;
  buttonDropDow: any[];
  parcelasSelect: number;
}

export class DadosTransacao extends React.Component<IDadosTransacaoProp, IDadosTransacaoState> {
  state: IDadosTransacaoState = {
    card_mask: '',
    payment_token: '',
    totalCarrinho: 0,
    tipoTransacao: 'boleto',
    maxNumeroCartao: 16,
    maxdigitoCartao: 3,
    cssIcard: 'iconCard fa fa-credit-card',
    endereco: {
      rua: '',
      numero: '',
      bairro: '',
      complemento: '',
      cidade: '',
      cep: '',
      estado: '',
      pais: '',
      uf: ''
    },
    cliente: {
      nome: '',
      sobrenome: '',
      telefone: '',
      email: '',
      cpf: ''
    },
    carrinhoLista: [],
    carrinhoListaIds: [],
    cartForm: '',
    nomeForm: '',
    desabilitaBotao: false,
    exibeBotaoParcelas: false,
    exibeTransacionando: false,
    valorFrete: 68,
    buttonDropDow: [],
    parcelasSelect: 1
  };

  handleSubmit = (event, errors, { numeroCartao, nomeCartao, validadeMes, validadeAno, cardDigito }) => {
    if (errors.length > 0) {
      // console.log('erro: ' + errors);
    } else {
      this.setState({
        desabilitaBotao: true,
        exibeTransacionando: true
      });
      this.getPackToken(numeroCartao, nomeCartao, validadeMes, validadeAno, cardDigito);
    }
  };

  componentWillMount() {
    if (localStorage.getItem('myEndereco') !== null) {
      this.setState({
        endereco: JSON.parse(localStorage.getItem('myEndereco'))
      });
    }

    if (localStorage.getItem('myClient') !== null) {
      this.setState({
        cliente: JSON.parse(localStorage.getItem('myClient'))
      });
    }

    if (localStorage.getItem('myCartList') !== null) {
      this.state.carrinhoLista = JSON.parse(localStorage.getItem('myCartList'));
      for (const i of this.state.carrinhoLista) {
        this.state.totalCarrinho += i.valor;
        this.state.carrinhoListaIds.push(i.id);
      }
      if (this.state.totalCarrinho === 0) {
        this.props.setStep(1);
      }
    }
  }

  nextStep = step => () => {
    // console.log('nextStep ' + step);
    this.props.setStep(step);
    window.scrollTo(0, 0);
  };

  defineTransacao = (tipo, maxNumero, digito) => () => {
    this.setState({
      tipoTransacao: tipo,
      maxNumeroCartao: maxNumero,
      maxdigitoCartao: digito
    });
  };

  blur(event, value, erro) {
    let frase = 'iconCard fa fa-credit-card';
    if (value.length !== this.state.maxNumeroCartao) {
      frase = 'icardErro fa fa-credit-card';
    }
    this.setState({
      cssIcard: frase
    });
  }

  getBoletoTransaction() {
    // console.log('inicia transacao Boleto!!');

    let cpfAux = this.state.cliente.cpf;
    cpfAux = cpfAux.replace('.', '');
    cpfAux = cpfAux.replace('.', '');
    cpfAux = cpfAux.replace('-', '');

    let telefoneAux = this.state.cliente.telefone;
    telefoneAux = telefoneAux.replace('(', '');
    telefoneAux = telefoneAux.replace(')', '');
    telefoneAux = telefoneAux.replace(' ', '');
    telefoneAux = telefoneAux.replace('-', '');

    this.state.endereco.cep = this.state.endereco.cep.replace('.', '');
    this.state.endereco.cep = this.state.endereco.cep.replace('-', '');

    const body = {
      cliente: {
        cpf: cpfAux,
        email: this.state.cliente.email,
        nome: this.state.cliente.nome,
        rg: '',
        sobrenome: this.state.cliente.sobrenome,
        telefone: telefoneAux
      },
      endereco: {
        cep: this.state.endereco.cep,
        cidade: this.state.endereco.cidade,
        numero: this.state.endereco.numero,
        pais: this.state.endereco.pais,
        rua: this.state.endereco.rua,
        uf: this.state.endereco.uf,
        bairro: this.state.endereco.bairro,
        complemento: this.state.endereco.complemento
      },
      parcelas: 1,
      payment_token: this.state.payment_token,
      produtos: this.state.carrinhoListaIds,
      numero_cartao: this.state.card_mask
    };

    const data = JSON.stringify(body);
    // console.log('DATA SEND BOL: ' + data);

    fetch('/api/pagamentoBoletoBancario', {
      body: data,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    })
      .then(response => {
        // console.log('-------------------------');
        // console.log(response.status);
        // console.log('-------------------------');

        response.text().then(result => {
          // console.log('result1: ' + result);

          if (response.status === 200) {
            // console.log('limpou os dados');
            const resultJson = JSON.parse(result);

            this.props.setCliente(this.state.cliente, resultJson.id, resultJson.link);

            this.props.setStep(5);
            localStorage.clear();
            window.scrollTo(0, 0);
          } else {
            alert('Falha ao transacionar com Cartão de Crédito!');
          }
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  getPackToken(numeroCartao, nomeCartao, validadeMes, validadeAno, cardDigito) {
    numeroCartao = numeroCartao.replace(' ', '');
    numeroCartao = numeroCartao.replace(' ', '');
    numeroCartao = numeroCartao.replace(' ', '');
    numeroCartao = numeroCartao.replace(' ', '');
    // console.log(
    // 'dados: ' + this.state.tipoTransacao + ' , ' + numeroCartao + ' , ' + cardDigito + ' , ' + validadeMes + ' , ' + validadeAno);

    window.parent.myPaymentToken(
      this.state.tipoTransacao,
      numeroCartao,
      cardDigito,
      validadeMes,
      validadeAno,
      (this.state.totalCarrinho + this.state.valorFrete) * 100
    );

    if (localStorage.getItem('paymentToken') === null || localStorage.getItem('paymentToken') === '') {
      setTimeout(() => {
        // console.log('Travo1');

        if (localStorage.getItem('paymentToken') === null || localStorage.getItem('paymentToken') === '') {
          setTimeout(() => {
            // console.log('Travo2');

            if (localStorage.getItem('paymentToken') === null || localStorage.getItem('paymentToken') === '') {
              setTimeout(() => {
                // console.log('Travo3');
                if (localStorage.getItem('paymentToken') === null || localStorage.getItem('paymentToken') === '') {
                  alert('Erro com Cartão de Crédito. Serviço de Pagamento não Encontrado. Tente Novamente!');
                  location.reload();
                } else {
                  this.getObjeto();
                }
              }, 5000);
            } else {
              this.getObjeto();
            }
          }, 3000);
        } else {
          this.getObjeto();
        }
      }, 3000);
    } else {
      this.getObjeto();
    }
  }

  getObjeto() {
    // console.log('Travo Agora Foi!');
    const objetoResp = JSON.parse(localStorage.getItem('paymentToken'));
    const objetoResp2 = JSON.parse(localStorage.getItem('paymentParcelas')); // aqui tem que chamar um negocio para exibir as parcelas
    this.salvaDados(objetoResp);
    this.montaBotaoParcelas(objetoResp2);
  }

  montaBotaoParcelas = dadosParcelas => {
    const itens = [];
    if (dadosParcelas.code === 200) {
      // buttonDropDow.push(<DropdownMenu>);
      for (let i = 0; i < dadosParcelas.data.installments.length; i++) {
        itens.push(
          <option value={i + 1}>
            ({i + 1}x) - {i + 1} parcelas de R$ {dadosParcelas.data.installments[i].currency}
          </option>
        );
      }

      this.setState({
        buttonDropDow: itens,
        exibeBotaoParcelas: true,
        exibeTransacionando: false
      });
    } else {
      alert('Erro ao calcular parcelas do cartão: ' + dadosParcelas.error_description);
      location.reload();
    }
  };

  selecionado(event) {
    this.setState({
      parcelasSelect: event.target.value
    });
  }

  salvaDados = resposta => {
    if (resposta.code === 200) {
      this.setState({
        card_mask: resposta.data.card_mask,
        payment_token: resposta.data.payment_token
      });
      /* ----- Tudo certo Token válido ------------ */
      // cahama Transacao REST
      // this.montaTransacaoCartao();
    } else {
      alert('Erro. Cartão inválido: ' + resposta.error_description);
      location.reload();
    }

    // localStorage.setItem('paymentToken', '');
    localStorage.removeItem('paymentToken');
  };

  montaTransacaoCartao() {
    // console.log('inicia transacao!!');
    this.setState({
      exibeTransacionando: true
    });

    let cpfAux = this.state.cliente.cpf;
    cpfAux = cpfAux.replace('.', '');
    cpfAux = cpfAux.replace('.', '');
    cpfAux = cpfAux.replace('-', '');

    let telefoneAux = this.state.cliente.telefone;
    telefoneAux = telefoneAux.replace('(', '');
    telefoneAux = telefoneAux.replace(')', '');
    telefoneAux = telefoneAux.replace(' ', '');
    telefoneAux = telefoneAux.replace('-', '');

    this.state.endereco.cep = this.state.endereco.cep.replace('.', '');
    this.state.endereco.cep = this.state.endereco.cep.replace('-', '');

    const body = {
      cliente: {
        cpf: cpfAux,
        email: this.state.cliente.email,
        nome: this.state.cliente.nome,
        rg: '',
        sobrenome: this.state.cliente.sobrenome,
        telefone: telefoneAux
      },
      endereco: {
        cep: this.state.endereco.cep,
        cidade: this.state.endereco.cidade,
        numero: this.state.endereco.numero,
        pais: this.state.endereco.pais,
        rua: this.state.endereco.rua,
        uf: this.state.endereco.uf,
        bairro: this.state.endereco.bairro,
        complemento: this.state.endereco.complemento
      },
      parcelas: this.state.parcelasSelect,
      payment_token: this.state.payment_token,
      produtos: this.state.carrinhoListaIds,
      numero_cartao: this.state.card_mask
    };

    const data = JSON.stringify(body);
    // console.log('DATA SEND: ' + data);

    fetch('/api/pagamentoCartaoCredito', {
      body: data,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    })
      .then(response => {
        // console.log('-------------------------');
        // console.log(response.status);
        // console.log('-------------------------');

        response
          .text()
          .then(result => {
            // console.log('result1: ' + result);

            if (response.status === 200) {
              localStorage.clear();
              // console.log('limpou os dados');

              this.props.setCliente(this.state.cliente, result, '');
              this.props.setStep(5);
              window.scrollTo(0, 0);
            } else {
              alert('Falha ao transacionar com Cartão de Crédito!');
            }
          })
          .then(dois => {
            // console.log(" === === == So executou agora");
          });
      })
      .catch(err => {
        console.error(err);
      });
  }

  maskCard(event, val) {
    let v = val;
    if (this.state.tipoTransacao === 'diners' || this.state.tipoTransacao === 'amex') {
      v = v.replace(/\D/g, '');
      v = v.replace(/^(\d{4})(\d)/g, '$1 $2');
      v = v.replace(/^(\d{4})\s(\d{6})(\d)/g, '$1 $2 $3');
    } else {
      v = v.replace(/\D/g, '');
      v = v.replace(/^(\d{4})(\d)/g, '$1 $2');
      v = v.replace(/^(\d{4})\s(\d{4})(\d)/g, '$1 $2 $3');
      v = v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g, '$1 $2 $3 $4');
    }
    this.setState({ cartForm: v });
  }

  maskNome(event, val) {
    let res = val;
    res = res.toUpperCase();
    this.setState({ nomeForm: res });
  }

  render() {
    this.blur = this.blur.bind(this);
    this.salvaDados = this.salvaDados.bind(this);
    this.getPackToken = this.getPackToken.bind(this);
    this.maskCard = this.maskCard.bind(this);
    this.maskNome = this.maskNome.bind(this);
    this.getBoletoTransaction = this.getBoletoTransaction.bind(this);
    this.montaTransacaoCartao = this.montaTransacaoCartao.bind(this);
    this.selecionado = this.selecionado.bind(this);

    return (
      <div className="transacaoDiv">
        <br />
        <Row className="BandeirasContainer dadosCompra">
          <h5>Dados da Compra</h5>
          <br />Confira seus dados de comprador:<br />
          <Row>
            <Col>
              <h5 className="dadosTitulo">Dados Comprador</h5>
              <br />Cliente: {this.state.cliente.nome + ' ' + this.state.cliente.sobrenome}
              <br />CPF: {this.state.cliente.cpf}
              <br />Email: {this.state.cliente.email}
              <br />Telefone: {this.state.cliente.telefone}
              <br />
              <Button onClick={this.nextStep(2)} className="btn btn-cancel fontButton18 fontButtonEditDados">
                Editar
              </Button>
            </Col>

            <Col>
              <h5 className="dadosTitulo">Dados Entrega</h5>
              <br />Endereco: {this.state.endereco.rua}
              <br />Número: {this.state.endereco.numero}
              <br />Complemento: {this.state.endereco.complemento}
              <br />Bairro: {this.state.endereco.bairro}
              <br />CEP: {this.state.endereco.cep}
              <br />Cidade: {this.state.endereco.cidade}
              <br />Estado(UF): {this.state.endereco.uf + ' - ' + this.state.endereco.pais}
              <br />
              <Button onClick={this.nextStep(3)} className="btn btn-cancel fontButton18 fontButtonEditDados">
                Editar
              </Button>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5 className="dadosTotalTransacao">Valor Total da Compra: R$ {this.state.totalCarrinho + this.state.valorFrete},00</h5>
            </Col>
          </Row>
        </Row>

        <Row className="BandeirasContainer">
          <h5>Formas de Pagamento</h5>
        </Row>
        <Row className="BandeirasContainer">
          <Button
            className={this.state.tipoTransacao === 'boleto' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('boleto', 0, 0)}
          >
            <span className="fa fa-barcode iconBoleto" />
            <br />Boleto Bancário
          </Button>
          <Button
            className={this.state.tipoTransacao === 'visa' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('visa', 19, 3)}
          >
            <img src="content/images/visaBandeira.png" width="65" height="40" />
            <br />
            Cartão Visa
          </Button>
          <Button
            className={this.state.tipoTransacao === 'mastercard' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('mastercard', 19, 3)}
          >
            <img src="content/images/mastercarBandeira.png" width="65" height="40" />
            <br />Mastercard
          </Button>
          <Button
            className={this.state.tipoTransacao === 'diners' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('diners', 16, 3)}
          >
            <img src="content/images/dinersBandeira.png" width="65" height="40" />
            <br />Cartão Diners
          </Button>
          <Button
            className={this.state.tipoTransacao === 'elo' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('elo', 19, 3)}
          >
            <img src="content/images/eloBandeira.png" width="70" height="28" />
            <br />Cartão Elo
          </Button>
          <Button
            className={this.state.tipoTransacao === 'hipercard' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('hipercard', 19, 3)}
          >
            <img src="content/images/hipercardBandeira.png" width="70" height="28" />
            <br />Hipercard
          </Button>
          <Button
            className={this.state.tipoTransacao === 'amex' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('amex', 17, 4)}
          >
            <img src="content/images/amexBandeira.png" width="60" height="40" />
            <br />Cartão Amex
          </Button>
          <Button
            className={this.state.tipoTransacao === 'transferencia' ? 'selecionadoBandeira' : 'formasDePagamento'}
            onClick={this.defineTransacao('transferencia', 0, 0)}
          >
            <span className="fa fa-money iconMoney" />
            <br />Transferência
          </Button>
        </Row>

        <br />

        {this.state.tipoTransacao === 'boleto' && (
          <div id="boletoPagamento" className="boletoPagamento">
            <ListGroup>
              <ListGroupItem active action>
                Pagamento com Boleto
              </ListGroupItem>
              <ListGroupItem action>Imprima o boleto e pague no banco ou lotérica</ListGroupItem>
              <ListGroupItem action>Também, você pode pagar pelo App do seu Banco utilizando o código de barras do boleto</ListGroupItem>
              <ListGroupItem action>Em até 2 dias úteis o boleto será validado</ListGroupItem>
            </ListGroup>
            <br />
            <div className="center">
              Valor Total: R$ {this.state.totalCarrinho + this.state.valorFrete},00
              <br />
              <Button className="btn btn-success" onClick={this.getBoletoTransaction}>
                <span className="fa fa-barcode iconBoletoButton" /> Gerar Boleto
              </Button>
              <br />
              10% Desconto
            </div>
          </div>
        )}

        {this.state.tipoTransacao === 'transferencia' && (
          <div id="boletoPagamento" className="boletoPagamento">
            <ListGroup>
              <ListGroupItem active action>
                Pagamento via Transferência Bancárea
              </ListGroupItem>
              <ListGroupItem action>Trabalhamos com o banco: Banco do Brasil</ListGroupItem>
              <ListGroupItem action>
                Você deve fazer uma transferência para:<br />Conta: 535532<br />Agência: 01725
                <br />Nome: Eduardo Saretta<br />Valor R$ XXX,XX
              </ListGroupItem>
              <ListGroupItem action>Enviar o comprovante por E-mail ou Whatzap</ListGroupItem>
              <ListGroupItem action>A aprovação do seu pedido é feita em até 24 horas</ListGroupItem>
            </ListGroup>
            <br />
            <div className="center">
              Valor Total: R$ {this.state.totalCarrinho + this.state.valorFrete},00
              <br />
              <Button className="btn btn-success">
                <span className="fa fa-money iconMoney" /> Pagar via Transferência Bancárea
              </Button>
            </div>
          </div>
        )}

        {this.state.tipoTransacao !== 'boleto' &&
          this.state.tipoTransacao !== 'transferencia' && (
            <div id="boletoPagamento" className="boletoPagamento">
              <AvForm id="register-form" className="formContainer" onSubmit={this.handleSubmit}>
                <Row>
                  <Col md="12">
                    <AvField
                      value={this.state.cartForm}
                      onChange={this.maskCard}
                      className="numberCard"
                      name="numeroCartao"
                      label="Número do Cartão"
                      placeholder="Número impresso no Cartão"
                      validate={{
                        required: { value: true, errorMessage: 'Número é obrigatório!' },
                        minLength: { value: this.state.maxNumeroCartao, errorMessage: 'Número incompleto!' },
                        maxLength: { value: this.state.maxNumeroCartao, errorMessage: 'Número grande!' }
                      }}
                      // onChange={this.getValue}
                      onBlur={this.blur}
                    />
                    <i className={this.state.cssIcard} />
                  </Col>
                </Row>
                <Row>
                  <Col md="12">
                    <AvField
                      value={this.state.nomeForm}
                      onChange={this.maskNome}
                      name="nomeCartao"
                      label="Nome Impresso no Cartão"
                      placeholder="Nome Sobrenome"
                      validate={{
                        required: { value: true, errorMessage: 'Nome é obrigatório!' },
                        minLength: { value: 5, errorMessage: 'Nome incompleto!' },
                        maxLength: { value: 100, errorMessage: 'Nome grande!' }
                      }}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md="4">
                    <AvField
                      name="validadeMes"
                      label="Mês de Validade"
                      type="select"
                      validate={{
                        required: { value: true, errorMessage: 'Mês de Validade é obrigatório!' },
                        maxLength: { value: 2, errorMessage: 'Selecione o Mês de Validade!' }
                      }}
                    >
                      <option>Mês</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                      <option>05</option>
                      <option>06</option>
                      <option>07</option>
                      <option>08</option>
                      <option>09</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                    </AvField>
                  </Col>
                  <Col md="4">
                    <AvField
                      name="validadeAno"
                      label="Ano de Validade"
                      type="select"
                      validate={{
                        required: { value: true, errorMessage: 'Data de Validade é obrigatório!' },
                        minLength: { value: 4, errorMessage: 'Selecione o Ano de Validade' }
                      }}
                    >
                      <option>Ano</option>
                      <option>2018</option>
                      <option>2019</option>
                      <option>2020</option>
                      <option>2021</option>
                      <option>2022</option>
                      <option>2023</option>
                      <option>2024</option>
                      <option>2026</option>
                      <option>2027</option>
                      <option>2028</option>
                      <option>2029</option>
                      <option>2030</option>
                      <option>2031</option>
                      <option>2032</option>
                      <option>2033</option>
                      <option>2034</option>
                    </AvField>
                  </Col>
                </Row>
                <Row>
                  <Col md="3">
                    <AvField
                      name="cardDigito"
                      label="Código de Segurança"
                      placeholder="Digitos de segurança"
                      validate={{
                        required: { value: true, errorMessage: 'Código de Segurança é obrigatório!' },
                        minLength: { value: this.state.maxdigitoCartao, errorMessage: 'Código de Segurança incorreto!' },
                        maxLength: { value: this.state.maxdigitoCartao, errorMessage: 'Código de Segurança grande!' }
                      }}
                    />
                  </Col>
                </Row>
                <Row className="margimRow">
                  <Col md="6" className="alingLeft">
                    Valor Total: R$ {this.state.totalCarrinho + this.state.valorFrete},00
                  </Col>
                  <Col md="6" className="alingRight">
                    {!this.state.desabilitaBotao && (
                      <Button type="sybmit" className="btn btn-success fontButton18" disabled={this.state.desabilitaBotao}>
                        Calcular Parcelas
                      </Button>
                    )}

                    {this.state.exibeBotaoParcelas && (
                      <Input
                        className="btn btn-success fontButton18"
                        onChange={this.selecionado}
                        disabled={this.state.exibeTransacionando}
                        type="select"
                        name="select"
                        id="exampleSelect"
                      >
                        {this.state.buttonDropDow}
                      </Input>
                    )}
                    {'  '}
                    {this.state.desabilitaBotao && (
                      <Button
                        onClick={this.montaTransacaoCartao}
                        disabled={this.state.exibeTransacionando}
                        className="btn btn-success fontButton18"
                      >
                        Pagar no Cartão
                      </Button>
                    )}
                    <br />
                    {this.state.exibeTransacionando && 'Transacionando'}
                    {this.state.exibeTransacionando && <img src="content/images/loadingGif.gif" width="40" height="25" />}
                  </Col>
                </Row>
              </AvForm>
            </div>
          )}

        <br />
        <Row className="RodapeContainer">
          <Button onClick={this.nextStep(3)} className="btn btn-cancel fontButton18">
            Voltar
          </Button>
        </Row>
      </div>
    );
  }
}
export default DadosTransacao;

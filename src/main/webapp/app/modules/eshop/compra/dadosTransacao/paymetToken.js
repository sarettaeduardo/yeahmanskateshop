window.myPaymentToken = (brandFun , numberFun, cvvFun, expiration_monthFun, expiration_yearFun, valorTotal) => {

    const s = document.createElement('script');
    s.type = 'text/javascript';
    const v = Math.random() * 1000000;
    // s.src = 'https://sandbox.gerencianet.com.br/v1/cdn/4dd034b3cc815c8f961ca937d8845e9c/' + v;
    s.src = 'https://api.gerencianet.com.br/v1/cdn/4dd034b3cc815c8f961ca937d8845e9c/' + v;
    s.async = false; s.id = '4dd034b3cc815c8f961ca937d8845e9c';
    if (!document.getElementById('4dd034b3cc815c8f961ca937d8845e9c')) {
      document.getElementsByTagName('head')[0].appendChild(s);
    }

    $gn = { validForm: true, processed: false, done: {},
        ready: fn => {
        $gn.done = fn;
    }};

    $gn.ready(function (checkout) {
        callback = function (error, response) {
            if (error) {
                // Trata o erro ocorrido
                console.error(error);
                localStorage.setItem('paymentToken', JSON.stringify(error));
                // return erro;
            } else {
                // Trata a resposta
                // console.warn('data: ' + JSON.stringify(response.data));
                localStorage.setItem('paymentToken', JSON.stringify(response));
                // return response;
            }
        };

        checkout.getInstallments(valorTotal, brandFun, function(error, response) {
          if (error) {
            // Trata o erro ocorrido
            console.error(error);
          } else {
            // Insere o parcelamento no site
            // console.warn('dataP: ' + JSON.stringify(response.data));
            localStorage.setItem('paymentParcelas', JSON.stringify(response));
          }
        });

        checkout.getPaymentToken({
            brand: brandFun, // 'visa' bandeira do cartão
            number: numberFun, // '4012001038443335' número do cartão
            cvv: cvvFun, // '123' código de segurança
            expiration_month: expiration_monthFun , // '05' mês de vencimento
            expiration_year: expiration_yearFun // '2018' ano de vencimento
        }, callback);

    });

};

window.myInstallments = (brandFun , valorTotal) => {

  const s = document.createElement('script');
  s.type = 'text/javascript';
  const v = Math.random() * 1000000;
  // s.src = 'https://sandbox.gerencianet.com.br/v1/cdn/4dd034b3cc815c8f961ca937d8845e9c/' + v;
  s.src = 'https://api.gerencianet.com.br/v1/cdn/4dd034b3cc815c8f961ca937d8845e9c/' + v;
  s.async = false; s.id = '4dd034b3cc815c8f961ca937d8845e9c';
  if (!document.getElementById('4dd034b3cc815c8f961ca937d8845e9c')) {
    document.getElementsByTagName('head')[0].appendChild(s);
  }

  $gn = { validForm: true, processed: false, done: {},
    ready: fn => {
    $gn.done = fn;
  }};

  $gn.ready(function(checkout) {
    checkout.getInstallments(valorTotal, brandFun, function(error, response) {
      if (error) {
        // Trata o erro ocorrido
        console.error(error);
      } else {
        // Insere o parcelamento no site
        // console.warn(response);
      }
    });

  });
};

import './eshop.css';
import React from 'react';
import { Row, Col, Alert } from 'reactstrap';
import { ProdutoLista } from 'app/modules/eshop/produto/produto-list';
import { Carrinho } from 'app/modules/eshop/carrinho/carrinho';
import { FiltroProduto } from 'app/modules/eshop/filtro-produto/filtro-produto';
import { Link } from 'react-router-dom';

const dados1 = {
  id: 69,
  nome: 'Balance Board',
  valor: 15,
  quantidade: 1,
  imagem: ''
};

let dadoLista = [];
// dadoLista.push(dados1);
// dadoLista.push(dados1);
// dadoLista.push(dados1);

export interface IEshopProp {
  vai: string;
}

export interface IEshopState {
  // imagem: string;
  isManagingFocus: boolean;
  carrinhoCompras: string;
  nomeProdutoTransacional: string;
  produtoList: {
    id: number;
    nome: string;
    valor: number;
    quantidade: number;
    imagem: string;
  };
  carrinhoLista: any[];
  ProdutoLista: any[];
}

export class Eshop extends React.Component<IEshopProp, IEshopState> {
  state: IEshopState = {
    isManagingFocus: false,
    carrinhoCompras: 'carrinhoCompras',
    nomeProdutoTransacional: '',
    produtoList: {
      id: 0,
      nome: '',
      valor: 0,
      quantidade: 0,
      imagem: ''
    },
    carrinhoLista: [],
    ProdutoLista: []
  };

  _onBlur = () => {
    if (this.state.isManagingFocus) {
      setTimeout(() => {
        this.setState({
          isManagingFocus: true,
          carrinhoCompras: 'carrinhoComprasSmall'
        });
      }, 5000);
    }
  };

  _openCar = () => {
    setTimeout(() => {
      this.setState({
        isManagingFocus: !this.state.isManagingFocus,
        carrinhoCompras: this.state.isManagingFocus ? 'carrinhoComprasSmall' : 'carrinhoCompras'
      });
    }, 0);
  };

  adicionaProdutoCarrinho = produtoListF => {
    // console.log('Cliclou ADD Produto');
    this.state.carrinhoLista.push(produtoListF);
    this.setState({
      carrinhoLista: this.state.carrinhoLista
    });
    // setter
    // localStorage.setItem('myData', JSON.stringify(this.state.produtoList));

    // setter
    localStorage.setItem('myCartList', JSON.stringify(this.state.carrinhoLista));

    // getter
    // sessionStorage.getItem('myData');
  };

  removeProdutoCarrinho = produtoListKey => {
    if (this.state.carrinhoLista.length > 0) {
      for (let key = 0; key < this.state.carrinhoLista.length; key++) {
        if (this.state.carrinhoLista[key].id === produtoListKey) {
          this.state.carrinhoLista.splice(key, 1);
          this.setState({
            carrinhoLista: this.state.carrinhoLista
          });
          localStorage.setItem('myCartList', JSON.stringify(this.state.carrinhoLista));
          break;
        }
      }
    }
  };

  createProdutos = (lista, first) => {
    // console.log('ENTROU EM CREATE PROD');
    const gridProd = [];
    this.state.ProdutoLista = []; // limpa lista anterior
    if (lista.length === 0) {
      // console.log('vazio');
      if (first) {
        gridProd.push(
          <Alert color="primary" className="alertProdutos">
            <img className="centerImg" src="content/images/loadingGif.gif" width="60" height="50" alt="conceito" />Carregando Produtos
          </Alert>
        );
      } else {
        gridProd.push(
          <Alert className="alertProdutos" color="warning">
            Nenhum Produto Encontrado
          </Alert>
        );
      }
    } else {
      // for (let i = 0; i < dadoLista.length; i++) {
      for (const i of lista) {
        gridProd.push(
          <Col className="colProduto">
            <ProdutoLista addItenCarrinho={this.adicionaProdutoCarrinho} removeItenCarrinho={this.removeProdutoCarrinho} produtoList={i} />
          </Col>
        );
      }
    }
    this.state.ProdutoLista.push(gridProd);
    this.setState({
      ProdutoLista: this.state.ProdutoLista
    });
  };

  /*carregaProdutos = a => () => {
    if (a === 1) {
      this.createProdutos(dadoLista1);
    }
    if (a === 2) {
      this.createProdutos(dadoLista2);
    }
    if (a === 3) {
      this.createProdutos(dadoLista3);
    }
    if (a === 4) {
      this.createProdutos(dadoLista4);
    }
    if (a === 5) {
      this.createProdutos(dadoLista5);
    }
  };*/

  componentWillMount() {
    this.createProdutos(dadoLista, true);
    this.carregaProdutoDB();
    // getter
    // console.log('Get Storage: ' + localStorage.getItem('myCartList'));
    if (localStorage.getItem('myCartList') !== null) {
      this.state.carrinhoLista = JSON.parse(localStorage.getItem('myCartList'));
    }
  }

  carregaProdutoDB = () => {
    // if (this.state.controleChamadaAPI) {
    // console.log('=========================ENTROU================================================');
    let listJson;
    dadoLista = [];
    fetch('/api/produtosOpen', {
      method: 'GET'
    })
      .then(response => {
        response
          .text()
          .then(result => {
            // console.log("result: " + result);
            listJson = JSON.parse(result);
          })
          .then(() => {
            // console.log(" === === == So executou agora");
            // this.createPortfolio(false);

            for (const i of listJson) {
              dadoLista.push(i);
              // console.log('produto: ' + i);
            }
            this.createProdutos(dadoLista, false);
          });
      })
      .catch(err => {
        console.error(err);
      });
    // console.log('=================================ACABOU========================================');
    // }
  };

  render() {
    document.title = 'Loja Virtual - Produtos e Acessórios de Balance Board';
    const _desc =
      'E-commerce, Loja virtual, aceitamos cartão de crédito em parcelamento até 12x sem juros. Produtos e acessórios para prática balance board.';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div className="eshop">
        <br />
        <br />

        <Row>
          <Col sm="2" className="indiceShopFiltro">
            Produtos<br />Skate Shop
          </Col>
          <Col sm="10" className="indiceShopProdutos">
            <Link to="/">YeahMan Skate Shop</Link> >
            <Link to="/eshop">Todos Produtos</Link>
          </Col>
        </Row>
        <Row>
          <Col sm="2">
            <FiltroProduto listaProdutosFiltro={this.createProdutos} />
          </Col>
          <Col sm="10">
            <Row>{this.state.ProdutoLista}</Row>
          </Col>
        </Row>

        <Carrinho produtoList={this.state.carrinhoLista} removeItenCarrinho={this.removeProdutoCarrinho} />
      </div>
    );
  }
}
export default Eshop;

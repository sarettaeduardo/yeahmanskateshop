import './filtro-produto.css';
import React from 'react';
import { ListGroup, ListGroupItem, Button } from 'reactstrap';
import ItemLista from './itemLista';
import { NavLink as Link } from 'react-router-dom';

let produtosListaDados = [];

export interface IFiltroProdutoProp {
  listaProdutosFiltro: Function;
}

export interface IFiltroProdutoState {
  listaColecao: any[];
  listaCategoria: any[];
}

export class FiltroProduto extends React.Component<IFiltroProdutoProp, IFiltroProdutoState> {
  state: IFiltroProdutoState = {
    listaColecao: [],
    listaCategoria: []
  };

  carregaCategoria = () => {
    // console.log('=========================ENTROU================================================');
    let listJson;
    fetch('/api/categoriasOpen', {
      method: 'GET'
    })
      .then(response => {
        response.text().then(result => {
          // console.log("result: " + result);
          listJson = JSON.parse(result);
          this.setState({
            listaCategoria: listJson
          });
        });
      })
      .catch(err => {
        console.error(err);
      });
    // console.log('=================================ACABOU========================================');
  };

  carregaColecao = () => {
    // console.log('=========================ENTROU================================================');
    let listJson;
    fetch('/api/colecaosOpen', {
      method: 'GET'
    })
      .then(response => {
        response.text().then(result => {
          // console.log("result: " + result);
          listJson = JSON.parse(result);
          this.setState({
            listaColecao: listJson
          });
        });
      })
      .catch(err => {
        console.error(err);
      });
    // console.log('=================================ACABOU========================================');
  };

  componentWillMount() {
    this.carregaColecao();
    this.carregaCategoria();
  }

  carregaProdutoCategoria = id_categoria => {
    let listJson;
    produtosListaDados = [];
    fetch('/api/produtosOpenCategoria/' + id_categoria, {
      method: 'GET'
    })
      .then(response => {
        response
          .text()
          .then(result => {
            // console.log("result: " + result);
            listJson = JSON.parse(result);
          })
          .then(() => {
            for (const i of listJson) {
              produtosListaDados.push(i);
            }
            this.props.listaProdutosFiltro(produtosListaDados);
          });
      })
      .catch(err => {
        console.error(err);
      });
  };

  carregaProdutoColecao = id_colecao => {
    let listJson;
    produtosListaDados = [];
    fetch('/api/produtosOpenColecao/' + id_colecao, {
      method: 'GET'
    })
      .then(response => {
        response
          .text()
          .then(result => {
            // console.log("result: " + result);
            listJson = JSON.parse(result);
          })
          .then(() => {
            for (const i of listJson) {
              produtosListaDados.push(i);
            }
            this.props.listaProdutosFiltro(produtosListaDados);
          });
      })
      .catch(err => {
        console.error(err);
      });
  };

  carregaProdutoDestaque = () => () => {
    let listJson;
    produtosListaDados = [];
    fetch('/api/produtosOpenDestaque', {
      method: 'GET'
    })
      .then(response => {
        response
          .text()
          .then(result => {
            // console.log("result: " + result);
            listJson = JSON.parse(result);
          })
          .then(() => {
            for (const i of listJson) {
              produtosListaDados.push(i);
            }
            this.props.listaProdutosFiltro(produtosListaDados);
          });
      })
      .catch(err => {
        console.error(err);
      });
  };

  render() {
    return (
      <div>
        <ListGroup>
          <ItemLista nomeGrupo="Coleção" listaIntens={this.state.listaColecao} filtroPesquisa={this.carregaProdutoColecao} />
        </ListGroup>
        <ListGroup>
          <ItemLista nomeGrupo="Segmento" listaIntens={this.state.listaCategoria} filtroPesquisa={this.carregaProdutoCategoria} />
        </ListGroup>

        <ListGroupItem className="smallCaps list-group-item-titulo " tag="button" action>
          Promoção
        </ListGroupItem>

        <ListGroupItem onClick={this.carregaProdutoDestaque()} className="smallCaps list-group-item-titulo" tag="button" action>
          Destaque
        </ListGroupItem>
        <br />
        <Button tag={Link} to="/compra" className="btn btn-success buttonFiltroCompra">
          Finalizar Compra
        </Button>
        <br />
      </div>
    );
  }
}
export default FiltroProduto;

import './filtro-produto.css';
import React from 'react';
import { ListGroupItem, Button } from 'reactstrap';

export interface IItemListaProp {
  nomeGrupo: String;
  listaIntens: any[];
  filtroPesquisa: Function;
}

export interface IItemListaState {
  divShow: boolean;
}

export class ItemLista extends React.Component<IItemListaProp, IItemListaState> {
  state: IItemListaState = {
    divShow: true
  };

  showMore = () => {
    this.setState({
      divShow: !this.state.divShow
    });
  };

  carregaProdutos = id => () => {
    this.props.filtroPesquisa(id);
  };

  carregaListaItens = lista => {
    const gridItens = [];
    /* gridItens.push(
      <ListGroupItem onClick={this.carregaProdutos(1)} tag="button" action>
        - Todos de {this.props.nomeGrupo}
      </ListGroupItem>
    ); */
    for (const iten of lista) {
      gridItens.push(
        <ListGroupItem onClick={this.carregaProdutos(iten.id)} tag="button" action>
          ▢ {iten.nome}
        </ListGroupItem>
      );
    }
    return gridItens;
  };

  render() {
    return (
      <div>
        <ListGroupItem className="smallCaps list-group-item-titulo" action>
          {this.props.nomeGrupo}
          <Button className="btn btn-primary buttonList" onClick={this.showMore}>
            {this.state.divShow ? '-' : '+'}
          </Button>
        </ListGroupItem>
        <div className={this.state.divShow ? 'showDiv' : 'noneDiv'}>{this.carregaListaItens(this.props.listaIntens)}</div>
      </div>
    );
  }
}
export default ItemLista;

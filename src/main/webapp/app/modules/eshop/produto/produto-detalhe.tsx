import './produto.css';

import React from 'react';
import { Button, Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';

export interface IProdutoState {
  imagemSelecionada: string;
  carrinhoLista: any[];
  dadosPRoduto: {
    id: number;
    foto0: string;
    foto0ContentType: string;
    foto1: string;
    foto1ContentType: string;
    foto2: string;
    foto2ContentType: string;
    foto3: string;
    foto3ContentType: string;
    foto4: string;
    foto4ContentType: string;
    valorVenda: number;
    valorCusto: number;
    quantidade: number;
    nome: string;
    descricao: string;
    promocao: string;
    destaque: string;
    categoria: {
      id: number;
      nome: string;
      descricao: string;
    };
    colecao: {
      id: number;
      nome: string;
      descricao: string;
    };
  };
  parcelas: number;
}

export interface IProdutoProp extends RouteComponentProps<{ id: string }> {}

export class Produto extends React.Component<IProdutoProp, IProdutoState> {
  state: IProdutoState = {
    carrinhoLista: [],
    dadosPRoduto: {
      id: 0,
      foto0: '',
      foto0ContentType: '',
      foto1: '',
      foto1ContentType: '',
      foto2: '',
      foto2ContentType: '',
      foto3: '',
      foto3ContentType: '',
      foto4: '',
      foto4ContentType: '',
      valorVenda: 0,
      valorCusto: 0,
      quantidade: 0,
      nome: '',
      descricao: '',
      promocao: '',
      destaque: '',
      categoria: {
        id: 0,
        nome: '',
        descricao: ''
      },
      colecao: {
        id: 0,
        nome: '',
        descricao: ''
      }
    },
    imagemSelecionada: '',
    parcelas: 0
  };

  componentDidMount() {
    this.carregaProdutoId(this.props.match.params.id);
  }

  carregaProdutoId = id_produto => {
    // let objJson;
    fetch('/api/produtosOpen/' + id_produto, {
      method: 'GET'
    })
      .then(response => {
        response
          .text()
          .then(result => {
            // console.log('result: ' + result);
            this.state.dadosPRoduto = JSON.parse(result);
            this.setState({
              dadosPRoduto: this.state.dadosPRoduto,
              imagemSelecionada: this.state.dadosPRoduto.foto0 === null ? '' : this.state.dadosPRoduto.foto0,
              parcelas: parseInt('' + this.state.dadosPRoduto.valorVenda / 12, 10) + 1
            });
            this.state.dadosPRoduto = JSON.parse(result);
            // this.state.imagemSelecionada = this.state.dadosPRoduto.foto0;
            // console.log('resultObj: ' + objJson);
          })
          .then(dois => {
            // console.log(" === === == So executou agora");
            this.carregaFotosProdutoId(id_produto);
          });
      })
      .catch(err => {
        console.error(err);
      });
  };

  carregaFotosProdutoId = id_produto => {
    // let objJson;
    fetch('/api/produtosOpenFoto/' + id_produto, {
      method: 'GET'
    })
      .then(response => {
        response.text().then(result => {
          // console.log('result: ' + result);
          this.state.dadosPRoduto = JSON.parse(result);
          this.setState({
            dadosPRoduto: this.state.dadosPRoduto,
            imagemSelecionada: this.state.dadosPRoduto.foto0 === null ? '' : this.state.dadosPRoduto.foto0,
            parcelas: parseInt('' + this.state.dadosPRoduto.valorVenda / 12, 10) + 1
          });
          this.state.dadosPRoduto = JSON.parse(result);
        });
      })
      .catch(err => {
        console.error(err);
      });
  };

  exibeImagem = imagem => () => {
    this.setState({
      imagemSelecionada: imagem
    });
  };

  componentWillMount() {
    if (localStorage.getItem('myCartList') !== null) {
      this.state.carrinhoLista = JSON.parse(localStorage.getItem('myCartList'));
      this.setState({
        carrinhoLista: this.state.carrinhoLista
      });
    }
  }

  adicionaProdutoCarrinh = () => {
    const produtoListF = {
      id: this.state.dadosPRoduto.id,
      nome: this.state.dadosPRoduto.nome,
      valor: this.state.dadosPRoduto.valorVenda,
      quantidade: this.state.dadosPRoduto.quantidade,
      imagem: this.state.dadosPRoduto.foto0
    };

    this.state.carrinhoLista.push(produtoListF);
    this.setState({
      carrinhoLista: this.state.carrinhoLista
    });
    localStorage.setItem('myCartList', JSON.stringify(this.state.carrinhoLista));
  };

  render() {
    document.title = 'Produto ' + this.state.dadosPRoduto.nome + ' - ' + this.state.dadosPRoduto.categoria.nome;
    const _desc =
      'Nome do Produto: ' +
      this.state.dadosPRoduto.nome +
      ' - Categoria: ' +
      this.state.dadosPRoduto.categoria.nome +
      ' - Preço: R$' +
      this.state.dadosPRoduto.valorVenda +
      ',00 - Detalhes: ' +
      this.state.dadosPRoduto.descricao;
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);

    return (
      <div className="container">
        <Row>
          <Col xs="12">
            <Link to="/">YeahMan Skate Shop</Link> > <Link to="/eshop">Todos Produtos</Link> >{' '}
            <Link to="/eshop">{this.state.dadosPRoduto.categoria.nome}</Link>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs="12" md="1">
            {this.state.dadosPRoduto.foto0 && (
              <img
                onClick={this.exibeImagem(this.state.dadosPRoduto.foto0)}
                src={'data:image/jpeg;base64,' + this.state.dadosPRoduto.foto0}
                className="produtoImageMini"
              />
            )}
            {this.state.dadosPRoduto.foto1 && (
              <img
                onClick={this.exibeImagem(this.state.dadosPRoduto.foto1)}
                src={'data:image/jpeg;base64,' + this.state.dadosPRoduto.foto1}
                className="produtoImageMini"
              />
            )}
            {this.state.dadosPRoduto.foto2 && (
              <img
                onClick={this.exibeImagem(this.state.dadosPRoduto.foto2)}
                src={'data:image/jpeg;base64,' + this.state.dadosPRoduto.foto2}
                className="produtoImageMini"
              />
            )}
            {this.state.dadosPRoduto.foto3 && (
              <img
                onClick={this.exibeImagem(this.state.dadosPRoduto.foto3)}
                src={'data:image/jpeg;base64,' + this.state.dadosPRoduto.foto3}
                className="produtoImageMini"
              />
            )}
            {this.state.dadosPRoduto.foto4 && (
              <img
                onClick={this.exibeImagem(this.state.dadosPRoduto.foto4)}
                src={'data:image/jpeg;base64,' + this.state.dadosPRoduto.foto4}
                className="produtoImageMini"
              />
            )}
          </Col>
          <Col xs="12" md="7">
            <div className="produtoDetalhe">
              {this.state.imagemSelecionada && (
                <img src={'data:image/jpeg;base64,' + this.state.imagemSelecionada} className="produtoImageDetalhe" />
              )}
              {this.state.imagemSelecionada === '' && (
                <img className="centerImg" src="content/images/loadingGif.gif" width="60" height="50" />
              )}
              {this.state.imagemSelecionada === '' && 'Carregando Imagens'}
            </div>
          </Col>
          <Col xs="12" md="4">
            <h3 className="tituloDetalheProduto">{this.state.dadosPRoduto.nome}</h3>
            <br />
            {this.state.dadosPRoduto.categoria.nome}
            <br />
            {this.state.dadosPRoduto.categoria.descricao}
            <br />
            {this.state.dadosPRoduto.colecao && <Col>Coleção: {this.state.dadosPRoduto.colecao.nome}</Col>}
            {this.state.dadosPRoduto.colecao && <Col>{this.state.dadosPRoduto.colecao.descricao}</Col>}
            <br />
            <br />
            <br />
            <h3 className="tituloValorProduto">R${this.state.dadosPRoduto.valorVenda},00</h3>
            <br />
            <h5 className="tituloDetalheProduto">em 12x R${this.state.parcelas},00</h5>
            <br />
            <br />
            <Button onClick={this.adicionaProdutoCarrinh} tag={Link} to="/compra" className="btn btn-success buttonFiltroCompra">
              Comprar
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs="12">
            <h5 className="tituloDetalheProduto">Detalhes de {this.state.dadosPRoduto.nome}</h5>
            <br />
            {this.state.dadosPRoduto.descricao}
          </Col>
        </Row>
      </div>
    );
  }
}
export default Produto;

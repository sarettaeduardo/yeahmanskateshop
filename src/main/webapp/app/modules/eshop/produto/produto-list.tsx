import './produto.css';
import React from 'react';
import { Fade, Col, Img } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export interface IProdutoListaProp {
  // saida
  addItenCarrinho: Function;
  removeItenCarrinho: Function;

  // entrada
  produtoList: {
    id: number;
    nome: string;
    valor: number;
    quantidade: number;
    imagem: string;
  };
}

export interface IProdutoListaState {
  isInCart: boolean;
  moreOne: string;
  contadorProdutos: number;
  fadeIn: boolean;
}

export class ProdutoLista extends React.Component<IProdutoListaProp, IProdutoListaState> {
  state: IProdutoListaState = {
    isInCart: false,
    moreOne: '',
    contadorProdutos: 0,
    fadeIn: false
  };

  goTop = () => {
    window.scrollTo(0, 0);
  };

  handleClick = () => {
    // adiciona item
    this.setState({ isInCart: true });
    const { addItenCarrinho } = this.props;
    addItenCarrinho(this.props.produtoList);
    this.state.contadorProdutos++;
    this.setState({ moreOne: '+' + this.state.contadorProdutos });
    if (!this.state.fadeIn) {
      setTimeout(() => {
        this.setState({ fadeIn: false });
      }, 6000);
      setTimeout(() => {
        this.setState({ moreOne: '' });
        this.state.contadorProdutos = 0;
      }, 6100);
    }
    this.setState({ fadeIn: true });
  };

  produtoDetalhe() {
    window.scrollTo(0, 200);
    // window.location.href = '/produto/' + ;
  }

  // tag={Link} to="/eshop" onClick={this.goTop}
  render() {
    this.produtoDetalhe = this.produtoDetalhe.bind(this);

    return (
      <div>
        <div className="produto">
          <Link className="thumbnail pull-left" to={'/produto/' + this.props.produtoList.id}>
            <img onClick={this.produtoDetalhe} src={'data:image/jpeg;base64,' + this.props.produtoList.imagem} className="produtoImage" />
          </Link>
          <div className="caption">
            <h3 className="produtoTitulo">{this.props.produtoList.nome}</h3>
            <div className="produtoValor">R$ {this.props.produtoList.valor},00</div>
            <Fade in={this.state.fadeIn} className="infoMoreOne">
              {this.state.moreOne}
            </Fade>
            <div className="produtoButton">
              <button className="btn btn-primary" onClick={this.handleClick}>
                Add Carrinho
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ProdutoLista;

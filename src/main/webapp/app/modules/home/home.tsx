import './home.css';

import React from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';
import Carousel from 'app/shared/layout/carousel/carousel';
import Portfolio from 'app/modules/home/portfolio/portfolio';
import { getSession } from 'app/shared/reducers/authentication';
import { render } from 'react-dom';

/*
const dado1 = {
  linkYoutube: 'qLCQNDyOPQs',
  foto: 'https://uploaddeimagens.com.br/images/001/725/465/full/riogrande.png?1542137088',
  titulo: 'Exercicio Funcional',
  mensagem: 'A prancha de equilíbrio é um instrumento largamente utilizado em procedimentos fisioterapêuticos'
};

const dado0 = {
  linkYoutube: '',
  foto: 'content/images/portfolio_4.jpg',
  titulo: '',
  mensagem: 'A prancha de equilíbrio é um instrumento largamente '
};
const dado2 = {
  linkYoutube: 'nwIqTTRSjKg',
  foto: 'content/images/portfolio_9.jpg',
  titulo: '',
  mensagem: ''
};*/

let listJson = [];

export interface IHomeProp extends StateProps, DispatchProps {}

export interface IHomeState {
  collapse: boolean;
  modal: boolean;
  controleChamadaAPI: boolean;
  controleChamadaImageAPI: boolean;
  imgprev: string;
}

export class Home extends React.Component<IHomeProp, IHomeState> {
  state: IHomeState = {
    collapse: false,
    modal: false,
    controleChamadaAPI: true,
    controleChamadaImageAPI: true,
    imgprev: ''
  };

  carregaImage = () => {
    if (this.state.controleChamadaImageAPI) {
      // console.log('=========================ENTROU================================================');
      fetch('/api/uploadDB', {
        method: 'GET'
      })
        .then(response => {
          response
            .text()
            .then(result => {
              // console.log('result: ' + result);
              // listJson = JSON.parse(result);
            })
            .then(() => {
              // console.log(" === === == So executou agora");
            });
        })
        .catch(err => {
          console.error(err);
        });
      // console.log('=================================ACABOU========================================');
    }
  };

  carregaPortfolio = () => {
    if (this.state.controleChamadaAPI) {
      // console.log('=========================ENTROU================================================');
      fetch('/api/portfoliosOpen', {
        method: 'GET'
      })
        .then(response => {
          response
            .text()
            .then(result => {
              // console.log("result: " + result);
              listJson = JSON.parse(result);
            })
            .then(() => {
              // console.log(" === === == So executou agora");
              this.createPortfolio(false);
            });
        })
        .catch(err => {
          console.error(err);
        });
      // console.log('=================================ACABOU========================================');
    }
  };

  /*uploadImagem = () => {
    if (this.state.controleChamadaAPI) {
      fetch('/api/uploadDB?id=6', {
        method: 'GET'
      })
        .then(response => {
          response.text().then(result => {
            // console.log('result: ' + result);
            // listJson = JSON.parse(result);
            this.setState({
              // imgprev: window.atob(result)
              imgprev: result
            });
            // this.state.imgprev = JSON.parse(result);
          });
        })
        .catch(err => {
          console.error(err);
        });
    }
  };*/

  createPortfolio = imparPar => {
    if (listJson.length === 0) {
      // console.log('vazio');
    } else {
      const gridTrue = [];
      const gridFalse = [];
      // for (let i in listJson) // Não rolo, compilador não deixou
      for (let i = 0; i < listJson.length; i++) {
        if (i < listJson.length / 2) {
          gridTrue.push(
            <Portfolio
              linkYoutube={listJson[i].linkYoutube}
              foto={listJson[i].foto}
              titulo={listJson[i].titulo}
              mensagem={listJson[i].mensagem}
            />
          );
        } else {
          gridFalse.push(
            <Portfolio
              linkYoutube={listJson[i].linkYoutube}
              foto={listJson[i].foto}
              titulo={listJson[i].titulo}
              mensagem={listJson[i].mensagem}
            />
          );
        }
      }
      return imparPar ? gridTrue : gridFalse;
    }
  };

  componentWillMount() {
    this.carregaPortfolio();
    // this.uploadImagem();
  }

  componentDidMount() {
    this.createPortfolio(false);
    this.createPortfolio(true);
  }

  render() {
    document.title = 'YeahMan Skat Shop | Loja Virtual';
    const _desc =
      'YeahMan Skate Shop. Loja Online. Compre roupas, tênis e equipamentos de skate skateboard em Guaporé/RS.' +
      'Compre em até 12x no Cartão de Crédito. Skate | Shape | Truk | Roda | Rolamento';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    const { account } = this.props;
    return (
      <div className="styleHomeTop">
        <Carousel />
        <section id="servicos">
          <div className="colCenter">
            <Row>
              <Col md="6" className="colPortfolio">
                {this.createPortfolio(true)}
              </Col>
              <Col md="6" className="colPortfolio">
                {this.createPortfolio(false)}
              </Col>
            </Row>
            <br />
            <br />
            <br />
            <br />
            <br />
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

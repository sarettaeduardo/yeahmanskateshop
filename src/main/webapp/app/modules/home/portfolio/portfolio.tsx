import './portfolio.css';

import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

export interface IPortfolioProp {
  titulo: string;
  mensagem: string;
  foto: string;
  linkYoutube: string;
}

export interface IPortfolioState {
  modal: boolean;
  iframeYoutube: string;
}

export class Portfolio extends React.Component<IPortfolioProp, IPortfolioState> {
  state: IPortfolioState = {
    modal: false,
    iframeYoutube: 'https://www.youtube.com/embed/' + this.props.linkYoutube
  };

  toggleM() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    const closeBtn = (
      <button className="close" onClick={this.toggleM}>
        &times;
      </button>
    );
    this.toggleM = this.toggleM.bind(this);
    const linkYoutube = (
      <iframe
        id="ytplayer"
        typeof="text/html"
        width="100%"
        height="auto"
        className="playerYoutube"
        src={this.state.iframeYoutube}
        frameBorder="0"
        allowFullScreen
      />
    );

    return (
      <div>
        <a>
          {/*<div className="imagemBack" onClick={this.toggleM}></div>
          <img src={this.props.foto} onClick={this.toggleM} />
          <img src={require('../../../../../../../../../../../Imagens/java/riogrande.png')} onClick={this.toggleM} /> */}
          <img src={this.props.foto} onClick={this.toggleM} />
        </a>
        <Modal isOpen={this.state.modal} toggle={this.toggleM}>
          <ModalHeader className="ModalHeader" toggle={this.toggleM} close={closeBtn}>
            <img className="imgModal" src={this.props.foto} />
            <p className="textTitle">
              <b>{this.props.titulo}</b>
            </p>
          </ModalHeader>
          <ModalBody className="ModalBody">
            {this.props.mensagem}
            <br />
            {this.props.linkYoutube && linkYoutube}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" className="BotaoVoltar" onClick={this.toggleM}>
              Voltar
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default Portfolio;

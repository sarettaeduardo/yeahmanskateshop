import React from 'react';
import { Translate, translate } from 'react-jhipster';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Alert, Row, Col } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Link } from 'react-router-dom';

export interface IOrcamentoClienteProps {
  mostrarCliente: boolean;
  handleClose: Function;
  dadoCliente: Function;
}

class OrcamentoModal extends React.Component<IOrcamentoClienteProps> {
  handleSubmit = (event, errors, { nome, email, telefone, cpf }) => {
    const { handleClose } = this.props;
    if (errors.length > 0) {
      // console.log("erro: " + errors)
    } else if (this.TestaCPF(cpf)) {
      const { dadoCliente } = this.props;
      dadoCliente(nome, email, telefone, cpf);
      handleClose();
    } else {
      alert('CPF inválido');
    }
  };

  TestaCPF(cpf) {
    let Soma = 0;
    let Resto;
    if (
      cpf === '00000000000' ||
      cpf === '11111111111' ||
      cpf === '22222222222' ||
      cpf === '33333333333' ||
      cpf === '44444444444' ||
      cpf === '55555555555' ||
      cpf === '66666666666' ||
      cpf === '77777777777' ||
      cpf === '88888888888' ||
      cpf === '99999999999'
    ) {
      return false;
    }
    for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);
    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(cpf.substring(9, 10), 10)) return false;
    Soma = 0;
    for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);
    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(cpf.substring(10, 11), 10)) return false;
    return true;
  }

  render() {
    const { handleClose } = this.props;

    return (
      <Modal isOpen={this.props.mostrarCliente} backdrop="static" id="login-page" autoFocus={false}>
        <ModalHeader id="login-title">Dados para Orçamento</ModalHeader>
        <br />
        <Row className="justify-content-center">
          <Col md="8">
            <AvForm id="register-form" onSubmit={this.handleSubmit}>
              <AvField
                name="nome"
                label="Nome Completo"
                placeholder="Seu nome completo"
                validate={{
                  required: { value: true, errorMessage: translate('register.messages.validate.login.required') },
                  minLength: { value: 1, errorMessage: translate('register.messages.validate.login.minlength') },
                  maxLength: { value: 250, errorMessage: translate('register.messages.validate.login.maxlength') }
                }}
              />
              <AvField
                name="email"
                label={translate('global.form.email')}
                placeholder={translate('global.form.email.placeholder')}
                type="email"
                validate={{
                  required: { value: true, errorMessage: translate('global.messages.validate.email.required') },
                  minLength: { value: 5, errorMessage: translate('global.messages.validate.email.minlength') },
                  maxLength: { value: 254, errorMessage: translate('global.messages.validate.email.maxlength') }
                }}
              />
              <AvField
                name="cpf"
                label="CPF"
                placeholder="Seu CPF"
                validate={{
                  required: { value: true, errorMessage: 'O CPF é obrigatório' },
                  minLength: { value: 11, errorMessage: 'O CPF é obrigatório' },
                  maxLength: { value: 11, errorMessage: 'O CPF é obrigatório' }
                }}
              />
              <AvField
                name="telefone"
                label="Telefone"
                placeholder="Seu telefone"
                validate={{
                  required: { value: true, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' },
                  minLength: { value: 5, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' },
                  maxLength: { value: 254, errorMessage: 'O Telefone é obrigatório, para entrarmos em contato' }
                }}
              />
              <br />
              <Button color="primary" type="submit">
                Guardar
              </Button>{' '}
              <Button color="secondary" onClick={handleClose} tabIndex="1">
                <Translate contentKey="entity.action.cancel">Cancel</Translate>
              </Button>
            </AvForm>
          </Col>
        </Row>
        <ModalFooter />
      </Modal>
    );
  }
}
export default OrcamentoModal;

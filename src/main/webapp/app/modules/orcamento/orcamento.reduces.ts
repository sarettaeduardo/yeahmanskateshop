import { IOrcamento, defaultValue } from 'app/shared/model/orcamento.model';
import { ADICIONAR_UM } from 'app/modules/orcamento/orcamento-type';
import { combineReducers } from 'redux';

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOrcamento>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

const valorReducer = (state = { valor: 0 }, action) => {
  switch (action.type) {
    case ADICIONAR_UM:
      return { ...state, valor: state.valor + 1 };
    default:
      return state;
  }
};

const rootReduceOrc = combineReducers({
  valores: valorReducer
});

// Export default rootReduceOrc;

export type OrcamentoState = Readonly<typeof initialState>;

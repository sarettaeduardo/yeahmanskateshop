import './orcamento.css';

import React from 'react';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { AvForm, AvInput, AvField } from 'availity-reactstrap-validation';
import { Button, Label } from 'reactstrap';
import { getSession } from 'app/shared/reducers/authentication';
import OrcamentoModal from './orcamento-cliente';

export interface IOrcamentoProp extends StateProps, DispatchProps {}

export interface IOrcamentoState {
  lista: {
    item0: boolean;
    item1: boolean;
    item2: boolean;
    item3: boolean;
    item4: boolean;
    item5: boolean;
    item6: boolean;
    item7: boolean;
  };
  altValor: boolean;
  TOTAL: number;
  area: number;
  valorMetro: number;
  desconto: number;
  mostrarCliente: boolean;
  cliente: {
    nome: string;
    email: string;
    telefone: string;
    cpf: string;
  };
  disabledButton: string;
  goToTopStart: boolean;
}

export class Orcamento extends React.Component<IOrcamentoProp, IOrcamentoState> {
  state: IOrcamentoState = {
    lista: {
      item0: false,
      item1: true,
      item2: true,
      item3: true,
      item4: true,
      item5: true,
      item6: false,
      item7: false
    },
    altValor: true,
    TOTAL: 0,
    area: 0,
    valorMetro: 45,
    desconto: 25,
    mostrarCliente: false,
    cliente: {
      nome: '',
      email: '',
      telefone: '',
      cpf: ''
    },
    disabledButton: '',
    goToTopStart: true
  };

  dadoCliente = (nomeF, emailF, telefoneF, cpfF) => {
    // console.log(': ' + nomeF + ': ' + emailF + ': ' + telefoneF + ': ' + cpfF);
    this.state.cliente.nome = nomeF;
    this.state.cliente.email = emailF;
    this.state.cliente.telefone = telefoneF;
    this.state.cliente.cpf = cpfF;
  };

  alteraMetros = ev => {
    this.setState({ area: ev.target.value });
    this.state.TOTAL = ev.target.value * this.state.valorMetro;
    // tem q limitar por conta do boleto ter limite
    // if(this.state.TOTAL >= 9999) {
    //  this.state.TOTAL = 10000;
    // }
    this.calculaDesconto(this.state.TOTAL);
  };

  setItem0 = evento => {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 4);
  };

  setItem1(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 15);
  }

  setItem2(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 15);
  }

  setItem3(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 5);
  }

  setItem4(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 5);
  }

  setItem5(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 5);
  }

  setItem6(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 10);
  }

  setItem7(evento) {
    this.setState({ altValor: evento.target.value });
    this.calculaTotal(evento.target.value, 15);
  }

  calculaTotal = (estado, metro) => {
    const metroVar = this.state.valorMetro;
    /*
        if (estado === 'false') {
            this.state.valorMetro = metroVar + metro;
            // +
        } else {
            this.state.valorMetro = metroVar - metro;
            // -
        }
        */

    estado === 'false' ? (this.state.valorMetro = this.state.valorMetro + metro) : (this.state.valorMetro = this.state.valorMetro - metro);

    this.state.TOTAL = this.state.area * this.state.valorMetro;
    // if (this.state.TOTAL >= 9999) {
    //  this.state.TOTAL = 10000;
    // }
    this.calculaDesconto(this.state.TOTAL);
  };

  /*
   calculaTotal = (estado, metro) => {
        const metroVar = this.state.valorMetro;
        let calcTot = 0;
        estado === 'false' ? calcTot = metroVar + metro : calcTot = metroVar - metro;
        this.state.TOTAL = (this.state.area * calcTot);
        this.calculaDesconto(this.state.TOTAL);
    };
    */

  calculaDesconto(val) {
    if (val < 2000) {
      this.state.desconto = 5;
    } else if (val > 2001 && val < 6000) {
      this.state.desconto = 9;
    } else if (val > 6001 && val < 9998) {
      this.state.desconto = 12;
    } else if (val > 9999) {
      this.state.desconto = 99;
    }
  }

  handleValidSubmit = (event, values) => {
    // veriica se tem todos os Dados
    if (
      this.state.cliente.nome === '' ||
      this.state.cliente.email === '' ||
      this.state.cliente.telefone === '' ||
      this.state.cliente.cpf === ''
    ) {
      this.handleClose();
    } else if (this.state.TOTAL === 0) {
      alert('Marque um Serviço da Lista!');
    } else {
      values.desconto = this.state.desconto;
      values.total = this.state.TOTAL;
      values.nome = this.state.cliente.nome;
      values.email = this.state.cliente.email;
      values.cpf = this.state.cliente.cpf;
      values.telefone = this.state.cliente.telefone;
      // values.listaItens = this.state.listaItens;
      values.area = this.state.area;
      // values.nome = "joao da silva";
      // values.telefone = "3334477";
      // values.cpf = "01234567890";
      const data = JSON.stringify(values);
      // this.props.handleRegister(values.area, values.email, values.firstPassword, this.props.currentLocale);
      // event.preventDefault();
      // console.log(JSON.stringify(values.cliente));

      fetch('/api/geraBoleto', {
        body: data,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      })
        .then(response => {
          /*
                console.log("-------------------------");
                console.log(response.headers);
                console.log("-------------------------");
                console.log(response.headers.get);
                console.log("-------------------------");
                console.log(response.body);
                console.log("-------------------------");
                console.log(response.body.getReader);
                console.log("-------------------------");
                */
          this.setState({ disabledButton: 'desabilitado' });
          response.body
            .getReader()
            .read()
            .then(dataIn => {
              // transformando em string, para
              // visualizarmos melhor
              const fetched = String.fromCharCode.apply(null, dataIn.value);
              // console.log(fetched);
              window.open('/api/downloadPDF?in=' + fetched, 'PDF', 'width=auto,height=auto');
              // var myWindow = window.open("", "PDF", "width=auto,height=auto");
              // myWindow.document.write(response.headers);
              // myWindow.document.write(data.value);
            });
        })
        .catch(err => {
          alert('ERRO:' + err);
        });
    }
  };

  // Funções para outra Janela
  handleClose = () => {
    this.setState({ mostrarCliente: !this.state.mostrarCliente });
  };

  render() {
    const { TOTAL } = this.state;
    this.setItem0 = this.setItem0.bind(this);
    this.setItem1 = this.setItem1.bind(this);
    this.setItem2 = this.setItem2.bind(this);
    this.setItem3 = this.setItem3.bind(this);
    this.setItem4 = this.setItem4.bind(this);
    this.setItem5 = this.setItem5.bind(this);
    this.setItem6 = this.setItem6.bind(this);
    this.setItem7 = this.setItem7.bind(this);

    if (this.state.goToTopStart === true) {
      window.scrollTo(0, 0);
      this.state.goToTopStart = false;
    }

    return (
      <section id="orcamento">
        <div className="orcamentoBody">
          <h2>
            <span className="fa fa-arrow-circle-left icon" />
            <Translate contentKey="orcamento.projeto.title">Projeto</Translate>
            <span className="fa fa-arrow-circle-right icon" />
          </h2>

          <AvForm className="orcamentoForm" action="" target="_blank" onValidSubmit={this.handleValidSubmit}>
            <table>
              <tr>
                <td />
                <td>
                  <Label id="areaLabel" for="area" className="areaLabel font26">
                    Area
                  </Label>
                  <Label id="areaLabel" for="area" className="areaLabel">
                    (m²):
                  </Label>
                </td>
                <td>
                  <AvField
                    value={this.state.area}
                    onChange={this.alteraMetros}
                    id="orcamento-area"
                    type="number"
                    name="area"
                    className="font24 area"
                    placeholder="Sua área de construção"
                    validate={{
                      required: { value: true, errorMessage: 'A Área é Obrigatória' },
                      maxLength: { value: 4, errorMessage: 'A Área é muito grande para orçar online.Entre em contato e vamos conversar!' }
                    }}
                  />
                </td>
              </tr>
              <br />

              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item0}
                    onChange={this.setItem0}
                    id="orcamento-item0"
                    type="checkbox"
                    name="listaItens.item0"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item0Label" for="item0" className="font24">
                    Desmembramento e Unificação de Lote
                  </Label>
                </td>
              </tr>

              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item1}
                    onChange={this.setItem1}
                    id="orcamento-item1"
                    type="checkbox"
                    name="listaItens.item1"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item1Label" for="item1" className="font24">
                    Projeto Arquitetônico
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item2}
                    onChange={this.setItem2}
                    id="orcamento-item2"
                    type="checkbox"
                    name="listaItens.item2"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item2Label" for="item2" className="font24">
                    Projeto Estrutural
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item3}
                    onChange={this.setItem3}
                    id="orcamento-item3"
                    type="checkbox"
                    name="listaItens.item3"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item3Label" for="item3" className="font24">
                    Projeto Eletrico
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item4}
                    onChange={this.setItem4}
                    id="orcamento-item4"
                    type="checkbox"
                    name="listaItens.item4"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item4Label" for="item4" className="font24">
                    Projeto Hidrosanitario
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item5}
                    onChange={this.setItem5}
                    id="orcamento-item5"
                    type="checkbox"
                    name="listaItens.item5"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item5Label" for="item5" className="font24">
                    Projeto Interiores 3D
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item6}
                    onChange={this.setItem6}
                    id="orcamento-item6"
                    type="checkbox"
                    name="listaItens.item6"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item6Label" for="item6" className="font24">
                    PPCI / PSCI
                  </Label>
                </td>
              </tr>
              <tr>
                <td>
                  <span className="fa fa-plus-circle iconPlus" />
                </td>
                <td>
                  <AvInput
                    value={this.state.lista.item7}
                    onChange={this.setItem7}
                    id="orcamento-item7"
                    type="checkbox"
                    name="listaItens.item7"
                    className="checkboxItem"
                  />
                </td>
                <td className="left">
                  <Label id="item7Label" for="item7" className="font24">
                    Acompanhamento em Obra
                  </Label>
                </td>
              </tr>
              <br />
            </table>
            <table className="orcamentoButton">
              <tr>
                <td>
                  <Label className="font24 left">Valor do Projeto:</Label>
                </td>
                <td>
                  <Label className="font26 left">R$ {TOTAL},00</Label>
                </td>
              </tr>
              <tr>
                <td>
                  <Button type="submit" disabled={this.state.disabledButton} className="buttonBoleto">
                    Gerar Orçamento<br />
                    <span className="fa fa-barcode iconBoleto" />
                  </Button>
                </td>
                <td className="font10 left">
                  Desconto {this.state.desconto !== 99 ? 'de ' + this.state.desconto + '% ' : 'Expecial para Você'}
                  <br />no Boleto Online
                </td>
              </tr>
            </table>
          </AvForm>
          <OrcamentoModal mostrarCliente={this.state.mostrarCliente} handleClose={this.handleClose} dadoCliente={this.dadoCliente} />
          <br />
          <br />
          <img src="content/images/CertificadoTransac.png" width="264" height="70" alt="certificado" />
          <br />
          <br />
        </div>
      </section>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orcamento);

import './carousel.css';

import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
const valFal = false;
const valtrue = true;
const items = [
  {
    src: 'content/images/slide0.jpg'
  },
  {
    src: 'content/images/slide1.jpg'
  },
  {
    src: 'content/images/slide2.jpg'
  }
];

const Carousel = props => (
  <div className="carousel" id="slideCarousel">
    <UncontrolledCarousel items={items} indicators={valFal} controls={valtrue} />
  </div>
);

export default Carousel;

import './contact.css';

import React from 'react';
import { Navbar } from 'reactstrap';

const Contact = props => (
  <div className="contact" id="logoBar">
    <Navbar light expand="sm" className="logosLedft">
      <a href="/">
        <img src="content/images/logoyeahman180x127.png" className="logoBalanceHead" alt="Logo" />
      </a>
    </Navbar>
    <Navbar light expand="sm" className="logos">
      <a
        target="_blank"
        href="https://wa.me/5554999305230?text=Olá!%20Entro%20em%20contato%20através%20do%20site!%20yeahmanskateshop.com.br"
      >
        <img src="content/images/whatsapp.png" alt="Whatz" />
      </a>
      <a target="_blank" href="https://www.facebook.com/yeahmanskateshop/">
        <img src="content/images/facebook.png" alt="Face" />
      </a>
      <a target="_blank" href="https://www.instagram.com/yeahmanskateshop/">
        <img src="content/images/instagram.png" alt="Insta" />
      </a>
      <a target="_blank" href="mailto:loja@yeahmanskateshop.com.br">
        <img src="content/images/gmail.png" alt="Gmail" />
      </a>
    </Navbar>
  </div>
);

export default Contact;

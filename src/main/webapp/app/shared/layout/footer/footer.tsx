import './footer.css';

import React from 'react';
import { translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { Col, Row, Form, FormGroup, Input, Label, Button } from 'reactstrap';
import { IFooterProp, IFooterState } from 'app/shared/layout/footer/footer';
import { AvForm, AvInput, AvField } from 'availity-reactstrap-validation';
import { getSession } from 'app/shared/reducers/authentication';
import { connect } from 'react-redux';

export interface IFooterProp extends StateProps, DispatchProps {}

export interface IFooterState {
  nome: string;
  email: string;
  mensagem: string;
  disableButton: string;
}

export class Footer extends React.Component<IFooterProp, IFooterState> {
  state: IFooterState = {
    nome: '',
    email: '',
    mensagem: '',
    disableButton: ''
  };

  goTop = () => {
    window.scrollTo(0, 0);
  };

  handleValidSubmit = (event, values) => {
    // veriica se tem todos os Dados
    if (values.nome === '' || values.email === '' || values.mensagem === '') {
      // Se deu nao preencheu os dados!!!
      alert('Favor, preencha seu Nome e E-mail para entrar em contato!');
    } else {
      const data = JSON.stringify(values);

      fetch('/api/contatoSendFooter', {
        body: data,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      })
        .then(response => {
          if (response.status !== 200) {
            alert('Desculpe! \n Tivemos um problema para enviar mensagem de contato! \n Tente novamente mais tarde.');
          } else {
            alert('Mensagem enviada com sucesso! \n' + this.state.nome + ' Agradecemos seu contato! Logo Entraremos em Contato com Você.');
            this.setState({
              disableButton: 'desabilitado',
              email: '',
              nome: '',
              mensagem: ''
            });
          }
        })
        .catch(err => {
          alert('ERRO:' + err);
        });
    }
  };

  render() {
    return (
      <div>
        <section id="contato" className="footer page-content">
          <br />
          <Row>
            <Col xs="12" className="colCenter">
              <h1>Contato</h1>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <AvForm onValidSubmit={this.handleValidSubmit}>
                <Label className="labelForm" sm={3} row>
                  Nome
                </Label>
                <Col sm={10}>
                  <AvField
                    type="name"
                    value={this.state.nome}
                    name="nome"
                    className="inputForm"
                    placeholder="Seu nome para contato"
                    validate={{
                      required: { value: true, errorMessage: 'O nome é obrigatorio..' },
                      minLength: { value: 3, errorMessage: 'Complete seu nome!' },
                      maxLength: { value: 250, errorMessage: 'O nome esta grande de mais.' }
                    }}
                  />
                </Col>
                <Label className="labelForm" sm={2} row>
                  E-mail
                </Label>
                <Col sm={10}>
                  <AvField
                    type="email"
                    nodeValue={this.state.email}
                    name="email"
                    id="email"
                    className="inputForm"
                    placeholder="Seu email aqui!"
                    validate={{
                      required: { value: true, errorMessage: 'O campo de e-mail é obrigatório!' },
                      minLength: { value: 9, errorMessage: 'Complete seu email' },
                      maxLength: { value: 254, errorMessage: 'E-mail esta grande de mais' }
                    }}
                  />
                </Col>
                <Label className="labelForm" sm={3} row>
                  Mensagem
                </Label>
                <Col sm={10}>
                  <AvField
                    type="textarea"
                    value={this.state.mensagem}
                    id="mensagem"
                    name="mensagem"
                    className="inputForm rowTextArea"
                    placeholder="Digite uma mensagem aqui..."
                    validate={{
                      required: { value: true, errorMessage: 'É obrigatório deixar uma mensagem!' }
                    }}
                  />
                </Col>
                <Col sm={10}>
                  <Button type="submit" disabled={this.state.disableButton} color="success">
                    Enviar
                  </Button>
                </Col>
              </AvForm>
            </Col>
            <Col md="6">
              <br />
              <Row>
                <Col className="colCenter">
                  <h6>
                    Já conferiu nossos produtos?<br />Clique aqui e acesse a loja online!
                  </h6>
                </Col>
              </Row>
              <Row>
                <Col md={{ size: 8, offset: 1 }}>
                  <Button tag={Link} to="/eshop" onClick={this.goTop} className="buttonOrcamento">
                    <h5>Loja Virtual</h5>
                  </Button>
                </Col>
              </Row>
              <br />
              <Row>
                <Col md={{ size: 8, offset: 1 }} className="imgContato">
                  <Row>
                    <a
                      target="_blank"
                      href="https://wa.me/5554999305230?text=Olá!%20Entro%20em%20contato%20através%20do%20site!%20yeahmanskateshop.com.br"
                    >
                      <img src="content/images/whatsappColor.png" width="40" height="40" alt="Whatz" />
                      <span className="whatz">(54) 99930-5230</span>
                    </a>
                  </Row>
                  <Row>
                    <a target="_blank" href="https://www.facebook.com/yeahmanskateshop/">
                      <img src="content/images/facebookColor.png" width="40" height="40" alt="Face" />
                      <span className="face">facebook.com/yeahmanskateshop</span>
                    </a>
                  </Row>
                  <Row>
                    <a target="_blank" href="mailto:loja@yeahmanskateshop.com.br">
                      <img src="content/images/gmailColor.png" width="40" height="40" alt="Gmail" />
                      <span className="gmail">loja@yeahmanskateshop.com.br</span>
                    </a>
                  </Row>
                  <Row>
                    <a target="_blank" href="https://www.instagram.com/yeahmanskateshop/">
                      <img src="content/images/instagramColor.png" width="40" height="40" alt="Insta" />
                      <span className="insta">instagram.com/yeahmanskateshop</span>
                    </a>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
        </section>
        <Row className="certificadoSSL">
          <Col md="4">
            <br />
            Tudo em até 12x no cartão <br />
            Boleto Bancário com 10% de Desconto.<br />
            <img src="content/images/bandeiras.png" height="70" alt="certificado" />
          </Col>
          <Col md="4">
            <br />
            <br />
            <br />
            YeahMan Skate Shop LTDA - CNPJ 118.111.222/0001-55<br />
            Alberto Pasqualini, 2001 - Centro - Guaporé/RS<br />
            © Todos os direitos reservador.
            <br />
          </Col>
          <Col md="4">
            <br />
            <a href="https://letsencrypt.org/" target="_blank">
              <img src="content/images/CertificadoSite.png" height="70" alt="certificado" />
            </a>
            <br />
            <br />
            <a target="_blank" href="https://www.instagram.com/dudusaretta/">
              Eduardo Saretta - Copyright ® 2019
            </a>
            <br />
            <br />
            <br />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Footer);

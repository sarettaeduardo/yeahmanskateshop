import React from 'react';
import { Translate } from 'react-jhipster';

import { UncontrolledDropdown, DropdownToggle, DropdownMenu, NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import appConfig from 'app/config/constants';

export const NavDropdown = props => (
  <UncontrolledDropdown nav inNavbar id={props.id}>
    <DropdownToggle nav caret className="d-flex align-items-center">
      <FontAwesomeIcon icon={props.icon} />
      <span>{props.name}</span>
    </DropdownToggle>
    <DropdownMenu right style={props.style}>
      {props.children}
    </DropdownMenu>
  </UncontrolledDropdown>
);

export const BrandIcon = props => (
  <div {...props} className="brand-icon">
    <img src="content/images/logo-jhipster-react.svg" alt="Logo" />
  </div>
);

export const LogoLTIcon = props => (
  <div {...props} className="brand-icon">
    <img src="content/images/logoyeahmanloja.png" alt="Logo" id="logolt" />
  </div>
);

export const Brand = props => (
  <NavbarBrand tag={Link} to="/eshop" className="brand-logo">
    <BrandIcon />
    <span className="brand-title">
      <Translate contentKey="global.title">Balance Board</Translate>
    </span>
    <span className="navbar-version">{appConfig.VERSION}</span>
  </NavbarBrand>
);

export const LogoLT = props => (
  <NavbarBrand data-scroll tag={Link} to="/eshop" className="brand-logo">
    <LogoLTIcon />
  </NavbarBrand>
);

export const Home = props => (
  <NavItem>
    <NavLink data-scroll tag={Link} to="/inicio" className="d-flex align-items-center">
      <FontAwesomeIcon icon="home" />
      <span>
        <Translate contentKey="global.menu.home">Home</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Servicos = props => (
  <NavItem>
    <NavLink data-scroll tag={Link} to="/servicos" className="d-flex align-items-center">
      <span>
        <Translate contentKey="global.menu.servicos">Servicos</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Orcamento = props => (
  <NavItem>
    <NavLink data-scroll tag={Link} to="/eshop" className="d-flex align-items-center">
      <span>
        <Translate contentKey="global.menu.orcamento">Orçamento</Translate>
      </span>
    </NavLink>
  </NavItem>
);

export const Contato = props => (
  <NavItem>
    <NavLink data-scroll tag={Link} to="/contato" className="d-flex align-items-center">
      <span>
        <Translate contentKey="global.menu.contato">Contato</Translate>
      </span>
    </NavLink>
  </NavItem>
);

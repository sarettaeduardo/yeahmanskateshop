import './header.css';

import React from 'react';

import { Translate } from 'react-jhipster';
import { Navbar, Nav, NavbarToggler, NavbarBrand, Collapse, Fade } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink as Link } from 'react-router-dom';
import LoadingBar from 'react-redux-loading-bar';

import { Home, Servicos, Orcamento, Contato, LogoLT } from './header-components';
import { AdminMenu, EntitiesMenu, AccountMenu, LocaleMenu } from './menus';

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isSwaggerEnabled: boolean;
  currentLocale: string;
  onLocaleChange: Function;
}

export interface IHeaderState {
  menuOpen: boolean;
  fadeIn: boolean;
}

export default class Header extends React.Component<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    menuOpen: false,
    fadeIn: true
  };

  handleScroll() {
    // if (document.documentElement.scrollTop > 430) {
    const navbar = document.getElementById('myNavbar');
    const bandlogo = document.getElementById('logolt');
    // const logoBar = document.getElementById('logoBar');
    const calculoBar = window.scrollY > 200 ? 0 : 30 - window.scrollY / 8;
    // navbar.style.marginTop = calculoBar + 'vh';
    bandlogo.style.visibility = calculoBar === 0 ? 'visible' : 'hidden';
    navbar.style.position = calculoBar === 0 ? 'fixed' : 'absolute';
    navbar.style.marginTop = calculoBar === 0 ? '-60px' : '';
    navbar.style.fontSize = calculoBar === 0 ? '17px' : '';
    // }
  }

  componentDidMount() {
    window.onscroll = () => this.handleScroll();
  }

  handleLocaleChange = event => {
    this.props.onLocaleChange(event.target.value);
  };

  renderDevRibbon = () =>
    this.props.isInProduction === false ? (
      <div className="ribbon dev">
        <a href="">
          <Translate contentKey={`global.ribbon.${this.props.ribbonEnv}`} />
        </a>
      </div>
    ) : null;

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  };

  toggleFade = () => {
    this.setState({ fadeIn: !this.state.fadeIn });
  };

  render() {
    const { currentLocale, isAuthenticated, isAdmin, isSwaggerEnabled, isInProduction } = this.props;

    /* jhipster-needle-add-element-to-menu - JHipster will add new menu items here */

    return (
      <section id="inicio">
        <div id="app-header">
          {this.renderDevRibbon()}
          <LoadingBar className="loading-bar" />
          <Navbar light expand="sm" className="jh-navbar" id="myNavbar">
            <NavbarToggler aria-label="Menu" onClick={this.toggleMenu} />
            {/* <Brand /> */}
            <Fade in={this.state.fadeIn}>
              <LogoLT />
            </Fade>
            <Collapse isOpen={this.state.menuOpen} navbar>
              <Nav id="header-tabs" navbar>
                <Home />
                <Servicos />
                <Orcamento />
                <Contato />
                {isAuthenticated && <EntitiesMenu />}
                {isAuthenticated && isAdmin && <AdminMenu showSwagger={isSwaggerEnabled} />}
                <LocaleMenu currentLocale={currentLocale} onClick={this.handleLocaleChange} />
                <AccountMenu isAuthenticated={isAuthenticated} />
              </Nav>
            </Collapse>
          </Navbar>
        </div>
      </section>
    );
  }
}

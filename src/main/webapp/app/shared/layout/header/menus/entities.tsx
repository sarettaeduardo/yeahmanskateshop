import React from 'react';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from '../header-components';

export const EntitiesMenu = props => (
  // tslint:disable-next-line:jsx-self-close
  <NavDropdown icon="th-list" name={translate('global.menu.entities.main')} id="entity-menu">
    {/* <DropdownItem tag={Link} to="/entity/endereco">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.endereco" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/pessoa">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.pessoa" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/produto">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.produto" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/transacao">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.transacao" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/orcamento">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.orcamento" />
    </DropdownItem>*/}
    <DropdownItem tag={Link} to="/entity/portfolio">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.portfolio" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/produtos">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.produtos" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/categoria">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.categoria" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/colecao">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.colecao" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/venda">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.venda" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/saida-caixa">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.saidaCaixa" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/entrada-caixa">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.entradaCaixa" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/cliente">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.cliente" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/endereco-entrega">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.enderecoEntrega" />
    </DropdownItem>
    <DropdownItem tag={Link} to="/entity/hser">
      <FontAwesomeIcon icon="asterisk" />&nbsp;<Translate contentKey="global.menu.entities.hser" />
    </DropdownItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);

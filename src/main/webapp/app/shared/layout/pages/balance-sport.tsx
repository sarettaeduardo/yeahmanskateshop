import './pages-shared.css';
import React from 'react';

export class BalanceSport extends React.Component {
  render() {
    document.title = 'Balance Sport - Balance Board Ecológico';
    const _desc =
      'Confira nosso videos de manobras perfeito para esportes coletivos, como futebol, basquete, hóquei, entre outros.' +
      'Ter sucesso em equipes requer que você tenha excelente tempo de reação, agilidade, equilíbrio e força central, e o Balance Board' +
      'treina todos eles.';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">Balance Board Sport</h2>
          <div className="textoContainer">
            YeahMan Skate Shop é perfeito para esportes coletivos, como futebol, basquete, hóquei, entre outros. Ter sucesso em equipes
            requer que você tenha excelente tempo de reação, agilidade, equilíbrio e força central, e o Balance Board treina todos eles. As
            pranchas têm sido usadas por atletas em praticamente todos os esportes coletivos para fornecer exercícios funcionais que
            beneficiam diretamente o treinamento de futebol, treinamento de basquete e muito mais.
            <br />
            Talvez nenhum outro esporte defina melhor a cultura do Balance Board do que o surf. O Balance Board foi originalmente
            desenvolvido por um treinador de surf para ajudar os surfistas, fornecendo treinamento de equilíbrio e exercícios básicos que
            não só ajudam os iniciantes a surfar, mas também fornecem surfistas avançados com uma prancha de equilíbrio permitindo que eles
            realizem exercícios de surf e exercícios avançados de surf. Qualquer tempo e qualquer lugar. Os melhores treinadores de fitness
            de todo o mundo têm usado o Balance Board para treinar todos os níveis de surfistas desde 1998.
            <br />
            <br />
            <h5>Esporte de Resistência</h5>
            YeahMan Skate Shop é a ferramenta perfeita para suplementar treinamento típico de triatlo, treinamento de maratona, treinamento
            de velocidade e outras formas comuns de treinamento de resistência com um método de treinamento funcional sem impacto que não
            levará aos problemas típicos associados ao overtraining para esportes como como corrida, ciclismo ou triatlo.
            <br />
            <br />
            Os treinadores de fitness de Balance Board, desenvolvem exercícios de surf específicos e os melhores exercícios do mesmo para
            ajudar os surfistas a melhorar seu equilíbrio, estabilidade e desempenho geral do surf.
            <br />
            Nenhum outro dispositivo de treinamento de equilíbrio tem a versatilidade e funcionalidade do Balance Board.
            <br />
            <br />
          </div>
          <br />
          <br />
          <h3 className="boldh3">Treinamento Avançado - Ollie</h3>
          <iframe
            className="centerImg"
            width="auto"
            height="400"
            src="https://www.youtube.com/embed/JCdDJ9H9GdQ"
            frameBorder="0"
            allowFullScreen
          />
          <br />
          <br />
          <br />
          <h3 className="boldh3">Treinamento Avançado - Kick Back e Rocking Chair</h3>
          <iframe
            className="centerImg"
            width="auto"
            height="400"
            src="https://www.youtube.com/embed/iH_KtGJiS7M"
            frameBorder="0"
            allowFullScreen
          />
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default BalanceSport;

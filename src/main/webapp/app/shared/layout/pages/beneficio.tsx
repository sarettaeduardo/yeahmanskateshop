import './pages-shared.css';
import React from 'react';

export class Beneficio extends React.Component {
  render() {
    document.title = 'Benefício - Balance Board Ecológico';
    const _desc =
      'Além de estar fazendo o bem para o meio ambiente faz um ótimo bem para nosso condicionamento físico e mental. A prancha de' +
      'equilíbrio ajuda a aprimorar o equilíblio e controle dos movimentos das pernas, quadril e fortalece os ligamentos e músculos';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">Benefício</h2>
          <div className="textoContainer">
            Além de estar fazendo o bem para o meio ambiente faz um ótimo bem para nosso condicionamento físico e mental. A prancha de
            equilíbrio ajuda a aprimorar o equilíblio e controle dos movimentos das pernas, quadril e fortalece os ligamentos e músculos.
            <br />
            <br />
            <h5>Balance Board em Programas de Fitness</h5>
            O treinamento de equilíbrio funcional e de resistência à instabilidade têm lugar no treinamento de aptidão funcional que
            produzirá benefícios específicos quando pré-formados sob as condições corretas. O treino de equilíbrio funcional melhora o
            equilíbrio, coordenação, resposta neuromuscular, consciência postural e propriocepção, mas quando combinado com treinamento de
            resistência pode fornecer um programa de treinamento verdadeiramente integrado que oferece uma gama mais ampla de benefícios e
            ativação muscular do que um programa de treinamento de resistência estável.
            <br />
            <br />
            <h5>A saúde e a aptidão começam com exercícios e atividade física</h5>
            O uso do Balance Board por diversão ou como um treino sério ajuda a desenvolver o equilíbrio, a coordenação e o aumento da força
            nas pernas, ao mesmo tempo em que aprimora o condicionamento físico. Ele também melhora as habilidades motoras, tornando o
            Balance Board uma ótima ferramentade treinamento cruzado para uma variedade de esportes, desde esportes radicais extremos até
            todos os principais esportes que exigem agilidade, equilíbrio e coordenação motora.
          </div>
          <img className="centerImg" src="content/images/beneficio.jpg" width="250" height="380" alt="conceito" />
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default Beneficio;

import './pages-shared.css';
import React from 'react';

export class Conceito extends React.Component {
  render() {
    document.title = 'Conceito - Balance Board Ecológico';
    const _desc =
      'A YeahMan Skate Shop é uma empresa emergente buscando novos métodos e formas para reestruturar a madeira de demolição.' +
      'É uma marca de shapes fabricada a partir do reaproveitamento da madeira de demolição';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">Conceito</h2>
          <div className="textoContainer">
            <img src="content/images/logoyeahman.jpg" width="100" height="100" alt="logo" /> A SEVERO é uma empresa emergente buscando novos
            métodos e formas para reestruturar a madeira de demolição. Nossa proposta é desenvolver produtos através da matéria prima já
            existente e dessa forma expandir para novos métodos de reciclagem.
            <br />
            <br />
            YeahMan Skate Shop é uma marca de shapes fabricada a partir do reaproveitamento da madeira de demolição. Com o objetivo de dar
            uma vida útil para a madeira, cada shape de Balance Board é diferente, mostrando assim a beleza natural do produto.
            <br />
            <br />
            Além de trazer benefícios para o condicionamento físico, equilíbrio e mental, é uma forma de diversão para adultos e crianças.
            Fluir é movimento, leveza, presença e equilíbrio. É confiança, é se entregar na energia do fluxo da vida.
          </div>
          <img className="centerImg" src="content/images/conceito.jpg" width="250" height="398" alt="conceito" />
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default Conceito;

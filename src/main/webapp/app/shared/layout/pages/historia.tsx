import './pages-shared.css';
import React from 'react';

export class Historia extends React.Component {
  render() {
    document.title = 'História - Severo Recycled Wood';
    const _desc =
      'Nossa história. Cristiano Severo deu início à Severo Recycled Wood em abril de 2018.' +
      'O primeiro produto lançado pela marca foi o Balance Board. Diversos produtos que carregam o lema de sustentabilidade';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">História</h2>
          <div className="textoContainer">
            Amante de práticas como yoga, calistenia e slackline, Cristiano Severo deu início à Severo Recycled Wood em abril de 2018, na
            cidade de Florianópolis, quando ele teve a ideia de reaproveitar tábuas velhas que seriam descartadas para desenvolver produtos
            que pudessem proporcionar diversão e qualidade de vida para as pessoas.
            <br />
            <br />
            O primeiro produto lançado pela marca foi o Balance Board. Embora seja um objeto relativamente simples, possui grande
            funcionalidade, podendo ser utilizado para treinos funcionais para diversas modalidades de esporte ou até mesmo em sessões de
            fisioterapia.
            <br />
            <br />
            Devido a simplicidade e grande aplicabilidade do produto, as primeiras levas produzidas foram rapidamente vendidas e a marca
            começou a ganhar espaço no mercado. A partir desse ponto, novos produtos passaram a surgir, visando aproveitar as madeiras que
            não eram passíveis de ser tornarem um balance board. Assim foram criadas as raquetes de frescobol e hand plants.
            <br />
            <br />
            <img className="centerImg" src="content/images/historia2.jpeg" width="300" height="410" alt="conceito" />
            <br />
            <br />
            Tal demanda fez com que uma quantidade significativa de matéria prima de qualidade e reciclada tivesse de ser encontrada. Para
            resolver esse problema surgiu uma idéia genial. Buscar casas de madeira que estivessem abandonadas, para transformar em produtos
            que gerem bem estar para as pessoas. Assim, além de proporcionar um bom destino para essa matéria prima, os produtos da Severo
            ainda trazem consigo a história de cada casa onde uma vez fizeram parte.
            <br />
            <br />
            Para dar início a esse projeto ambicioso, nada melhor do que começar com uma casa que tivesse uma grande história e uma conexão
            direta com a marca. Nada poderia ser melhor que a própria casa da família Severo, onde tudo começou. Atualmente a casa já se
            transformou em diversos produtos que carregam o lema de sustentabilidade, história e qualidade de vida. Essa coleção é
            carinhosamente chamada de “Green House 436” e hoje se encontra disponível em nosso site.
            <img className="centerImg" src="content/images/historia.jpg" width="300" height="420" alt="conceito" />
          </div>
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default Historia;

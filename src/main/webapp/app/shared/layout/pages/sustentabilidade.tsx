import './pages-shared.css';
import React from 'react';
import { Row, Col } from 'reactstrap';

export class Sustentabilidade extends React.Component {
  render() {
    document.title = 'Sustentabilidade - Balance Board Ecológico';
    const _desc =
      'O projeto desenvolvido é o conceito de reciclagem de madeiras de casas, galpões antigos, que são madeiras de lei com ' +
      'muitos anos de exigência, talvez 50,60 anos, e com a durabilidade de muitos anos ainda';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">Sustentabilidade</h2>
          <div className="textoContainer">
            <img src="content/images/sustentabilidadereciclavel.png" width="100" height="100" alt="logo" /> O médoto "Recicle Wood" busca
            encontrar madeiras perdidas na natureza ou desmanches e as tornar produtos.
            <br />
            <br />
            O projeto desenvolvido é o conceito de reciclagem de madeiras de casas, galpões antigos, que são madeiras de lei com muitos anos
            de exigência, talvez 50,60 anos, e com a durabilidade de muitos anos ainda.
            <br />
            <br />
            Por ela já ter passado muito tempo pegando chuva e sol e hoje ela estando seca e com sua beleza natural. A partir daí é feito
            uma limpeza nela trazendo vida útil a essa madeira,sendo utilizadas para desenvolver as pranchas de equilíbrio Severo Balance
            Board, feitas à mão uma a uma.
          </div>
          <img className="centerImg" src="content/images/sustentabilidade2.jpg" width="250" height="398" alt="conceito" />
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default Sustentabilidade;

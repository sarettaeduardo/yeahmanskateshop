import './pages-shared.css';
import React from 'react';

export class Treinamento extends React.Component {
  render() {
    document.title = 'Treinamento - Balance Board Ecológico';
    const _desc =
      'Estamos preparando uma série de vídeo aulas, focadas em um treinamento Funcional com técnicas de Yoga e' +
      'Manobras para prática do Balance Board.';
    document.querySelector('meta[name="description"]').setAttribute('content', _desc);
    return (
      <div>
        <br />
        <br />
        <br />
        <div className="container">
          <h2 className="center">Treinamento</h2>
          <div className="textoContainer">
            Estamos preparando uma série de vídeo aulas, focadas em um treinamento Funcional com técnicas de Yoga e Manobras para prática do
            Balance Board.
            <br />
            <br />
            <br />
          </div>
          <h3 className="boldh3">Treinamento Severo Balance Board</h3>
          <iframe
            className="centerImg"
            width="auto"
            height="400"
            src="https://www.youtube.com/embed/wKwlKpEAWoU"
            frameBorder="0"
            allowFullScreen
          />
        </div>
        <br />
        <br />
      </div>
    );
  }
}
export default Treinamento;

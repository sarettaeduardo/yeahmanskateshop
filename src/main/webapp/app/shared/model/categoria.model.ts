export interface ICategoria {
  id?: number;
  nome?: string;
  descricao?: string;
}

export const defaultValue: Readonly<ICategoria> = {};

import { IEnderecoEntrega } from 'app/shared/model//endereco-entrega.model';
import { IHser } from 'app/shared/model//hser.model';

export interface ICliente {
  id?: number;
  nome?: string;
  sobrenome?: string;
  cpf?: string;
  rg?: string;
  telefone?: string;
  email?: string;
  endereco?: IEnderecoEntrega;
  hser?: IHser;
}

export const defaultValue: Readonly<ICliente> = {};

export interface IColecao {
  id?: number;
  nome?: string;
  descricao?: string;
}

export const defaultValue: Readonly<IColecao> = {};

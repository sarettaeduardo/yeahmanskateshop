export interface IEnderecoEntrega {
  id?: number;
  rua?: string;
  numero?: number;
  pais?: string;
  uf?: string;
  cidade?: string;
  cep?: string;
  bairro?: string;
  complemento?: string;
}

export const defaultValue: Readonly<IEnderecoEntrega> = {};

import { IPessoa } from 'app/shared/model//pessoa.model';

export interface IEndereco {
  id?: number;
  rua?: string;
  cep?: string;
  cidade?: string;
  estado?: string;
  pais?: string;
  enderecos?: IPessoa[];
}

export const defaultValue: Readonly<IEndereco> = {};

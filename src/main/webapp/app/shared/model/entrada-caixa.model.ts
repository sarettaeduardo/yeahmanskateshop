import { Moment } from 'moment';
import { ICliente } from 'app/shared/model//cliente.model';
import { IProdutos } from 'app/shared/model//produtos.model';

export interface IEntradaCaixa {
  id?: number;
  nome?: string;
  descricao?: string;
  valor?: number;
  data?: Moment;
  cliente?: ICliente;
  produtos?: IProdutos[];
}

export const defaultValue: Readonly<IEntradaCaixa> = {};

import { Moment } from 'moment';

export interface IOrcamento {
  id?: number;
  codigoTransacao?: number;
  nome?: string;
  email?: string;
  telefone?: string;
  cpf?: string;
  link?: string;
  listaItens?: string;
  dataGeracao?: Moment;
  dataVencimento?: Moment;
  total?: number;
  desconto?: number;
  area?: number;
}

export const defaultValue: Readonly<IOrcamento> = {};

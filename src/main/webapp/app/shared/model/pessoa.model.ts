import { IEndereco } from 'app/shared/model//endereco.model';
import { ITransacao } from 'app/shared/model//transacao.model';

export interface IPessoa {
  id?: number;
  nome?: string;
  email?: string;
  senha?: string;
  endereco?: IEndereco;
  clientes?: ITransacao[];
}

export const defaultValue: Readonly<IPessoa> = {};

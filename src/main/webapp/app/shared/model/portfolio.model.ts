export interface IPortfolio {
  id?: number;
  foto?: string;
  titulo?: string;
  mensagem?: string;
  linkYoutube?: string;
  linkInstagram?: string;
}

export const defaultValue: Readonly<IPortfolio> = {};

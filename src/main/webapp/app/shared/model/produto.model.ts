import { ITransacao } from 'app/shared/model//transacao.model';

export interface IProduto {
  id?: number;
  nome?: string;
  descricao?: string;
  valor?: number;
  disponivel?: boolean;
  itens?: ITransacao[];
}

export const defaultValue: Readonly<IProduto> = {
  disponivel: false
};

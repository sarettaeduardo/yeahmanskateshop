import { ICategoria } from 'app/shared/model//categoria.model';
import { IColecao } from 'app/shared/model//colecao.model';
import { IVenda } from 'app/shared/model//venda.model';
import { IEntradaCaixa } from 'app/shared/model//entrada-caixa.model';

export interface IProdutos {
  id?: number;
  foto0ContentType?: string;
  foto0?: any;
  foto1ContentType?: string;
  foto1?: any;
  foto2ContentType?: string;
  foto2?: any;
  foto3ContentType?: string;
  foto3?: any;
  foto4ContentType?: string;
  foto4?: any;
  valorVenda?: number;
  valorCusto?: number;
  quantidade?: number;
  nome?: string;
  descricao?: string;
  promocao?: number;
  destaque?: boolean;
  categoria?: ICategoria;
  colecao?: IColecao;
  vendas?: IVenda[];
  entradaCaixas?: IEntradaCaixa[];
}

export const defaultValue: Readonly<IProdutos> = {
  destaque: false
};

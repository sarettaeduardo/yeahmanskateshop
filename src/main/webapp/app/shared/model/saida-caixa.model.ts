import { Moment } from 'moment';

export interface ISaidaCaixa {
  id?: number;
  nome?: string;
  tipo?: string;
  descricao?: string;
  valor?: number;
  data?: Moment;
}

export const defaultValue: Readonly<ISaidaCaixa> = {};

import { Moment } from 'moment';
import { IPessoa } from 'app/shared/model//pessoa.model';
import { IProduto } from 'app/shared/model//produto.model';

export interface ITransacao {
  id?: number;
  tipo?: string;
  dataGeracao?: Moment;
  dataValidade?: Moment;
  dataPagamento?: Moment;
  atualizado?: string;
  valorTotal?: number;
  pessoa?: IPessoa;
  produto?: IProduto;
}

export const defaultValue: Readonly<ITransacao> = {};

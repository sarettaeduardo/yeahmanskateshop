import { Moment } from 'moment';
import { ICliente } from 'app/shared/model//cliente.model';
import { IEnderecoEntrega } from 'app/shared/model//endereco-entrega.model';
import { IProdutos } from 'app/shared/model//produtos.model';

export interface IVenda {
  id?: number;
  descricao?: string;
  codigoTransacao?: string;
  link?: string;
  listaItens?: string;
  dataGeracao?: Moment;
  dataVencimento?: Moment;
  total?: number;
  desconto?: number;
  cliente?: ICliente;
  endereco?: IEnderecoEntrega;
  produtos?: IProdutos[];
}

export const defaultValue: Readonly<IVenda> = {};

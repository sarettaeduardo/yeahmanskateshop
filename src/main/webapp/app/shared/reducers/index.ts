// CombineReducers Combina reduces, para Cliente de Transacao
import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import endereco, {
  EnderecoState
} from 'app/entities/endereco/endereco.reducer';
// prettier-ignore
import pessoa, {
  PessoaState
} from 'app/entities/pessoa/pessoa.reducer';
// prettier-ignore
import produto, {
  ProdutoState
} from 'app/entities/produto/produto.reducer';
// prettier-ignore
import transacao, {
  TransacaoState
} from 'app/entities/transacao/transacao.reducer';

// prettier-ignore
import orcamento, {
  OrcamentoState
} from 'app/entities/orcamento/orcamento.reducer';
// prettier-ignore
import portfolio, {
  PortfolioState
} from 'app/entities/portfolio/portfolio.reducer';
// prettier-ignore
import produtos, {
  ProdutosState
} from 'app/entities/produtos/produtos.reducer';
// prettier-ignore
import categoria, {
  CategoriaState
} from 'app/entities/categoria/categoria.reducer';
// prettier-ignore
import colecao, {
  ColecaoState
} from 'app/entities/colecao/colecao.reducer';
// prettier-ignore
import venda, {
  VendaState
} from 'app/entities/venda/venda.reducer';
// prettier-ignore
import saidaCaixa, {
  SaidaCaixaState
} from 'app/entities/saida-caixa/saida-caixa.reducer';
// prettier-ignore
import entradaCaixa, {
  EntradaCaixaState
} from 'app/entities/entrada-caixa/entrada-caixa.reducer';
// prettier-ignore
import cliente, {
  ClienteState
} from 'app/entities/cliente/cliente.reducer';
// prettier-ignore
import enderecoEntrega, {
  EnderecoEntregaState
} from 'app/entities/endereco-entrega/endereco-entrega.reducer';
// prettier-ignore
import hser, {
  HserState
} from 'app/entities/hser/hser.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly endereco: EnderecoState;
  readonly pessoa: PessoaState;
  readonly produto: ProdutoState;
  readonly transacao: TransacaoState;
  // readonly orcamento: OrcamentoState;
  readonly orcamento: OrcamentoState;
  readonly portfolio: PortfolioState;
  readonly produtos: ProdutosState;
  readonly categoria: CategoriaState;
  readonly colecao: ColecaoState;
  readonly venda: VendaState;
  readonly saidaCaixa: SaidaCaixaState;
  readonly entradaCaixa: EntradaCaixaState;
  readonly cliente: ClienteState;
  readonly enderecoEntrega: EnderecoEntregaState;
  readonly hser: HserState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  endereco,
  pessoa,
  produto,
  transacao,
  orcamento,
  portfolio,
  produtos,
  categoria,
  colecao,
  venda,
  saidaCaixa,
  entradaCaixa,
  cliente,
  enderecoEntrega,
  hser,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;

# robotstxt.org/

User-agent: Googlebot
User-agent: AdsBot-Google
User-agent: Googlebot-news
User-agent: Googlebot-Image
User-agent: *
Allow: /
Allow: /historia
Allow: /inicio
Allow: /conceito
Allow: /beneficio
Allow: /sustentabilidade
Allow: /sport
Allow: /treinamento
Allow: /servicos
Allow: /contato
Allow: /eshop
Allow: /compra
Allow: /produto/:id


User-agent: *
Disallow: /api/account
Disallow: /api/account/change-password
Disallow: /api/account/sessions
Disallow: /api/audits/
Disallow: /api/logs/
Disallow: /api/users/
Disallow: /management/
Disallow: /v2/api-docs/

package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.YeahmanskateshopApp;

import br.com.siteyeahmanskateshop.domain.EnderecoEntrega;
import br.com.siteyeahmanskateshop.repository.EnderecoEntregaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.siteyeahmanskateshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EnderecoEntregaResource REST controller.
 *
 * @see EnderecoEntregaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YeahmanskateshopApp.class)
public class EnderecoEntregaResourceIntTest {

    private static final String DEFAULT_RUA = "AAAAAAAAAA";
    private static final String UPDATED_RUA = "BBBBBBBBBB";

    private static final Long DEFAULT_NUMERO = 1L;
    private static final Long UPDATED_NUMERO = 2L;

    private static final String DEFAULT_PAIS = "AAAAAAAAAA";
    private static final String UPDATED_PAIS = "BBBBBBBBBB";

    private static final String DEFAULT_UF = "AAAAAAAAAA";
    private static final String UPDATED_UF = "BBBBBBBBBB";

    private static final String DEFAULT_CIDADE = "AAAAAAAAAA";
    private static final String UPDATED_CIDADE = "BBBBBBBBBB";

    private static final String DEFAULT_CEP = "AAAAAAAAAA";
    private static final String UPDATED_CEP = "BBBBBBBBBB";

    private static final String DEFAULT_BAIRRO = "AAAAAAAAAA";
    private static final String UPDATED_BAIRRO = "BBBBBBBBBB";

    private static final String DEFAULT_COMPLEMENTO = "AAAAAAAAAA";
    private static final String UPDATED_COMPLEMENTO = "BBBBBBBBBB";

    @Autowired
    private EnderecoEntregaRepository enderecoEntregaRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEnderecoEntregaMockMvc;

    private EnderecoEntrega enderecoEntrega;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EnderecoEntregaResource enderecoEntregaResource = new EnderecoEntregaResource(enderecoEntregaRepository);
        this.restEnderecoEntregaMockMvc = MockMvcBuilders.standaloneSetup(enderecoEntregaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EnderecoEntrega createEntity(EntityManager em) {
        EnderecoEntrega enderecoEntrega = new EnderecoEntrega()
            .rua(DEFAULT_RUA)
            .numero(DEFAULT_NUMERO)
            .pais(DEFAULT_PAIS)
            .uf(DEFAULT_UF)
            .cidade(DEFAULT_CIDADE)
            .cep(DEFAULT_CEP)
            .bairro(DEFAULT_BAIRRO)
            .complemento(DEFAULT_COMPLEMENTO);
        return enderecoEntrega;
    }

    @Before
    public void initTest() {
        enderecoEntrega = createEntity(em);
    }

    @Test
    @Transactional
    public void createEnderecoEntrega() throws Exception {
        int databaseSizeBeforeCreate = enderecoEntregaRepository.findAll().size();

        // Create the EnderecoEntrega
        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isCreated());

        // Validate the EnderecoEntrega in the database
        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeCreate + 1);
        EnderecoEntrega testEnderecoEntrega = enderecoEntregaList.get(enderecoEntregaList.size() - 1);
        assertThat(testEnderecoEntrega.getRua()).isEqualTo(DEFAULT_RUA);
        assertThat(testEnderecoEntrega.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testEnderecoEntrega.getPais()).isEqualTo(DEFAULT_PAIS);
        assertThat(testEnderecoEntrega.getUf()).isEqualTo(DEFAULT_UF);
        assertThat(testEnderecoEntrega.getCidade()).isEqualTo(DEFAULT_CIDADE);
        assertThat(testEnderecoEntrega.getCep()).isEqualTo(DEFAULT_CEP);
        assertThat(testEnderecoEntrega.getBairro()).isEqualTo(DEFAULT_BAIRRO);
        assertThat(testEnderecoEntrega.getComplemento()).isEqualTo(DEFAULT_COMPLEMENTO);
    }

    @Test
    @Transactional
    public void createEnderecoEntregaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = enderecoEntregaRepository.findAll().size();

        // Create the EnderecoEntrega with an existing ID
        enderecoEntrega.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        // Validate the EnderecoEntrega in the database
        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkRuaIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setRua(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumeroIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setNumero(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaisIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setPais(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUfIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setUf(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCidadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setCidade(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCepIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setCep(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBairroIsRequired() throws Exception {
        int databaseSizeBeforeTest = enderecoEntregaRepository.findAll().size();
        // set the field null
        enderecoEntrega.setBairro(null);

        // Create the EnderecoEntrega, which fails.

        restEnderecoEntregaMockMvc.perform(post("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEnderecoEntregas() throws Exception {
        // Initialize the database
        enderecoEntregaRepository.saveAndFlush(enderecoEntrega);

        // Get all the enderecoEntregaList
        restEnderecoEntregaMockMvc.perform(get("/api/endereco-entregas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(enderecoEntrega.getId().intValue())))
            .andExpect(jsonPath("$.[*].rua").value(hasItem(DEFAULT_RUA.toString())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO.intValue())))
            .andExpect(jsonPath("$.[*].pais").value(hasItem(DEFAULT_PAIS.toString())))
            .andExpect(jsonPath("$.[*].uf").value(hasItem(DEFAULT_UF.toString())))
            .andExpect(jsonPath("$.[*].cidade").value(hasItem(DEFAULT_CIDADE.toString())))
            .andExpect(jsonPath("$.[*].cep").value(hasItem(DEFAULT_CEP.toString())))
            .andExpect(jsonPath("$.[*].bairro").value(hasItem(DEFAULT_BAIRRO.toString())))
            .andExpect(jsonPath("$.[*].complemento").value(hasItem(DEFAULT_COMPLEMENTO.toString())));
    }
    

    @Test
    @Transactional
    public void getEnderecoEntrega() throws Exception {
        // Initialize the database
        enderecoEntregaRepository.saveAndFlush(enderecoEntrega);

        // Get the enderecoEntrega
        restEnderecoEntregaMockMvc.perform(get("/api/endereco-entregas/{id}", enderecoEntrega.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(enderecoEntrega.getId().intValue()))
            .andExpect(jsonPath("$.rua").value(DEFAULT_RUA.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO.intValue()))
            .andExpect(jsonPath("$.pais").value(DEFAULT_PAIS.toString()))
            .andExpect(jsonPath("$.uf").value(DEFAULT_UF.toString()))
            .andExpect(jsonPath("$.cidade").value(DEFAULT_CIDADE.toString()))
            .andExpect(jsonPath("$.cep").value(DEFAULT_CEP.toString()))
            .andExpect(jsonPath("$.bairro").value(DEFAULT_BAIRRO.toString()))
            .andExpect(jsonPath("$.complemento").value(DEFAULT_COMPLEMENTO.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingEnderecoEntrega() throws Exception {
        // Get the enderecoEntrega
        restEnderecoEntregaMockMvc.perform(get("/api/endereco-entregas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEnderecoEntrega() throws Exception {
        // Initialize the database
        enderecoEntregaRepository.saveAndFlush(enderecoEntrega);

        int databaseSizeBeforeUpdate = enderecoEntregaRepository.findAll().size();

        // Update the enderecoEntrega
        EnderecoEntrega updatedEnderecoEntrega = enderecoEntregaRepository.findById(enderecoEntrega.getId()).get();
        // Disconnect from session so that the updates on updatedEnderecoEntrega are not directly saved in db
        em.detach(updatedEnderecoEntrega);
        updatedEnderecoEntrega
            .rua(UPDATED_RUA)
            .numero(UPDATED_NUMERO)
            .pais(UPDATED_PAIS)
            .uf(UPDATED_UF)
            .cidade(UPDATED_CIDADE)
            .cep(UPDATED_CEP)
            .bairro(UPDATED_BAIRRO)
            .complemento(UPDATED_COMPLEMENTO);

        restEnderecoEntregaMockMvc.perform(put("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEnderecoEntrega)))
            .andExpect(status().isOk());

        // Validate the EnderecoEntrega in the database
        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeUpdate);
        EnderecoEntrega testEnderecoEntrega = enderecoEntregaList.get(enderecoEntregaList.size() - 1);
        assertThat(testEnderecoEntrega.getRua()).isEqualTo(UPDATED_RUA);
        assertThat(testEnderecoEntrega.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testEnderecoEntrega.getPais()).isEqualTo(UPDATED_PAIS);
        assertThat(testEnderecoEntrega.getUf()).isEqualTo(UPDATED_UF);
        assertThat(testEnderecoEntrega.getCidade()).isEqualTo(UPDATED_CIDADE);
        assertThat(testEnderecoEntrega.getCep()).isEqualTo(UPDATED_CEP);
        assertThat(testEnderecoEntrega.getBairro()).isEqualTo(UPDATED_BAIRRO);
        assertThat(testEnderecoEntrega.getComplemento()).isEqualTo(UPDATED_COMPLEMENTO);
    }

    @Test
    @Transactional
    public void updateNonExistingEnderecoEntrega() throws Exception {
        int databaseSizeBeforeUpdate = enderecoEntregaRepository.findAll().size();

        // Create the EnderecoEntrega

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEnderecoEntregaMockMvc.perform(put("/api/endereco-entregas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(enderecoEntrega)))
            .andExpect(status().isBadRequest());

        // Validate the EnderecoEntrega in the database
        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEnderecoEntrega() throws Exception {
        // Initialize the database
        enderecoEntregaRepository.saveAndFlush(enderecoEntrega);

        int databaseSizeBeforeDelete = enderecoEntregaRepository.findAll().size();

        // Get the enderecoEntrega
        restEnderecoEntregaMockMvc.perform(delete("/api/endereco-entregas/{id}", enderecoEntrega.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EnderecoEntrega> enderecoEntregaList = enderecoEntregaRepository.findAll();
        assertThat(enderecoEntregaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EnderecoEntrega.class);
        EnderecoEntrega enderecoEntrega1 = new EnderecoEntrega();
        enderecoEntrega1.setId(1L);
        EnderecoEntrega enderecoEntrega2 = new EnderecoEntrega();
        enderecoEntrega2.setId(enderecoEntrega1.getId());
        assertThat(enderecoEntrega1).isEqualTo(enderecoEntrega2);
        enderecoEntrega2.setId(2L);
        assertThat(enderecoEntrega1).isNotEqualTo(enderecoEntrega2);
        enderecoEntrega1.setId(null);
        assertThat(enderecoEntrega1).isNotEqualTo(enderecoEntrega2);
    }
}

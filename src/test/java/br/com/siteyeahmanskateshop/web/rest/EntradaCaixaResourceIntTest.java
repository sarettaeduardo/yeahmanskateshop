package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.YeahmanskateshopApp;

import br.com.siteyeahmanskateshop.domain.EntradaCaixa;
import br.com.siteyeahmanskateshop.repository.EntradaCaixaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


import static br.com.siteyeahmanskateshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EntradaCaixaResource REST controller.
 *
 * @see EntradaCaixaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YeahmanskateshopApp.class)
public class EntradaCaixaResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final Long DEFAULT_VALOR = 1L;
    private static final Long UPDATED_VALOR = 2L;

    private static final Instant DEFAULT_DATA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private EntradaCaixaRepository entradaCaixaRepository;
    @Mock
    private EntradaCaixaRepository entradaCaixaRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEntradaCaixaMockMvc;

    private EntradaCaixa entradaCaixa;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EntradaCaixaResource entradaCaixaResource = new EntradaCaixaResource(entradaCaixaRepository);
        this.restEntradaCaixaMockMvc = MockMvcBuilders.standaloneSetup(entradaCaixaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EntradaCaixa createEntity(EntityManager em) {
        EntradaCaixa entradaCaixa = new EntradaCaixa()
            .nome(DEFAULT_NOME)
            .descricao(DEFAULT_DESCRICAO)
            .valor(DEFAULT_VALOR)
            .data(DEFAULT_DATA);
        return entradaCaixa;
    }

    @Before
    public void initTest() {
        entradaCaixa = createEntity(em);
    }

    @Test
    @Transactional
    public void createEntradaCaixa() throws Exception {
        int databaseSizeBeforeCreate = entradaCaixaRepository.findAll().size();

        // Create the EntradaCaixa
        restEntradaCaixaMockMvc.perform(post("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isCreated());

        // Validate the EntradaCaixa in the database
        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeCreate + 1);
        EntradaCaixa testEntradaCaixa = entradaCaixaList.get(entradaCaixaList.size() - 1);
        assertThat(testEntradaCaixa.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testEntradaCaixa.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testEntradaCaixa.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testEntradaCaixa.getData()).isEqualTo(DEFAULT_DATA);
    }

    @Test
    @Transactional
    public void createEntradaCaixaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = entradaCaixaRepository.findAll().size();

        // Create the EntradaCaixa with an existing ID
        entradaCaixa.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEntradaCaixaMockMvc.perform(post("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isBadRequest());

        // Validate the EntradaCaixa in the database
        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = entradaCaixaRepository.findAll().size();
        // set the field null
        entradaCaixa.setNome(null);

        // Create the EntradaCaixa, which fails.

        restEntradaCaixaMockMvc.perform(post("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isBadRequest());

        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = entradaCaixaRepository.findAll().size();
        // set the field null
        entradaCaixa.setValor(null);

        // Create the EntradaCaixa, which fails.

        restEntradaCaixaMockMvc.perform(post("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isBadRequest());

        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = entradaCaixaRepository.findAll().size();
        // set the field null
        entradaCaixa.setData(null);

        // Create the EntradaCaixa, which fails.

        restEntradaCaixaMockMvc.perform(post("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isBadRequest());

        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEntradaCaixas() throws Exception {
        // Initialize the database
        entradaCaixaRepository.saveAndFlush(entradaCaixa);

        // Get all the entradaCaixaList
        restEntradaCaixaMockMvc.perform(get("/api/entrada-caixas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entradaCaixa.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(DEFAULT_VALOR.intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())));
    }
    
    public void getAllEntradaCaixasWithEagerRelationshipsIsEnabled() throws Exception {
        EntradaCaixaResource entradaCaixaResource = new EntradaCaixaResource(entradaCaixaRepositoryMock);
        when(entradaCaixaRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restEntradaCaixaMockMvc = MockMvcBuilders.standaloneSetup(entradaCaixaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restEntradaCaixaMockMvc.perform(get("/api/entrada-caixas?eagerload=true"))
        .andExpect(status().isOk());

        verify(entradaCaixaRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    public void getAllEntradaCaixasWithEagerRelationshipsIsNotEnabled() throws Exception {
        EntradaCaixaResource entradaCaixaResource = new EntradaCaixaResource(entradaCaixaRepositoryMock);
            when(entradaCaixaRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restEntradaCaixaMockMvc = MockMvcBuilders.standaloneSetup(entradaCaixaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restEntradaCaixaMockMvc.perform(get("/api/entrada-caixas?eagerload=true"))
        .andExpect(status().isOk());

            verify(entradaCaixaRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getEntradaCaixa() throws Exception {
        // Initialize the database
        entradaCaixaRepository.saveAndFlush(entradaCaixa);

        // Get the entradaCaixa
        restEntradaCaixaMockMvc.perform(get("/api/entrada-caixas/{id}", entradaCaixa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(entradaCaixa.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.valor").value(DEFAULT_VALOR.intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingEntradaCaixa() throws Exception {
        // Get the entradaCaixa
        restEntradaCaixaMockMvc.perform(get("/api/entrada-caixas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEntradaCaixa() throws Exception {
        // Initialize the database
        entradaCaixaRepository.saveAndFlush(entradaCaixa);

        int databaseSizeBeforeUpdate = entradaCaixaRepository.findAll().size();

        // Update the entradaCaixa
        EntradaCaixa updatedEntradaCaixa = entradaCaixaRepository.findById(entradaCaixa.getId()).get();
        // Disconnect from session so that the updates on updatedEntradaCaixa are not directly saved in db
        em.detach(updatedEntradaCaixa);
        updatedEntradaCaixa
            .nome(UPDATED_NOME)
            .descricao(UPDATED_DESCRICAO)
            .valor(UPDATED_VALOR)
            .data(UPDATED_DATA);

        restEntradaCaixaMockMvc.perform(put("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEntradaCaixa)))
            .andExpect(status().isOk());

        // Validate the EntradaCaixa in the database
        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeUpdate);
        EntradaCaixa testEntradaCaixa = entradaCaixaList.get(entradaCaixaList.size() - 1);
        assertThat(testEntradaCaixa.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testEntradaCaixa.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testEntradaCaixa.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testEntradaCaixa.getData()).isEqualTo(UPDATED_DATA);
    }

    @Test
    @Transactional
    public void updateNonExistingEntradaCaixa() throws Exception {
        int databaseSizeBeforeUpdate = entradaCaixaRepository.findAll().size();

        // Create the EntradaCaixa

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEntradaCaixaMockMvc.perform(put("/api/entrada-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(entradaCaixa)))
            .andExpect(status().isBadRequest());

        // Validate the EntradaCaixa in the database
        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEntradaCaixa() throws Exception {
        // Initialize the database
        entradaCaixaRepository.saveAndFlush(entradaCaixa);

        int databaseSizeBeforeDelete = entradaCaixaRepository.findAll().size();

        // Get the entradaCaixa
        restEntradaCaixaMockMvc.perform(delete("/api/entrada-caixas/{id}", entradaCaixa.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EntradaCaixa> entradaCaixaList = entradaCaixaRepository.findAll();
        assertThat(entradaCaixaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EntradaCaixa.class);
        EntradaCaixa entradaCaixa1 = new EntradaCaixa();
        entradaCaixa1.setId(1L);
        EntradaCaixa entradaCaixa2 = new EntradaCaixa();
        entradaCaixa2.setId(entradaCaixa1.getId());
        assertThat(entradaCaixa1).isEqualTo(entradaCaixa2);
        entradaCaixa2.setId(2L);
        assertThat(entradaCaixa1).isNotEqualTo(entradaCaixa2);
        entradaCaixa1.setId(null);
        assertThat(entradaCaixa1).isNotEqualTo(entradaCaixa2);
    }
}

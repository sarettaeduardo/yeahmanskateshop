package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.YeahmanskateshopApp;

import br.com.siteyeahmanskateshop.domain.Hser;
import br.com.siteyeahmanskateshop.repository.HserRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.siteyeahmanskateshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HserResource REST controller.
 *
 * @see HserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YeahmanskateshopApp.class)
public class HserResourceIntTest {

    @Autowired
    private HserRepository hserRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHserMockMvc;

    private Hser hser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HserResource hserResource = new HserResource(hserRepository);
        this.restHserMockMvc = MockMvcBuilders.standaloneSetup(hserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hser createEntity(EntityManager em) {
        Hser hser = new Hser();
        return hser;
    }

    @Before
    public void initTest() {
        hser = createEntity(em);
    }

    @Test
    @Transactional
    public void createHser() throws Exception {
        int databaseSizeBeforeCreate = hserRepository.findAll().size();

        // Create the Hser
        restHserMockMvc.perform(post("/api/hsers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hser)))
            .andExpect(status().isCreated());

        // Validate the Hser in the database
        List<Hser> hserList = hserRepository.findAll();
        assertThat(hserList).hasSize(databaseSizeBeforeCreate + 1);
        Hser testHser = hserList.get(hserList.size() - 1);
    }

    @Test
    @Transactional
    public void createHserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hserRepository.findAll().size();

        // Create the Hser with an existing ID
        hser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHserMockMvc.perform(post("/api/hsers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hser)))
            .andExpect(status().isBadRequest());

        // Validate the Hser in the database
        List<Hser> hserList = hserRepository.findAll();
        assertThat(hserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllHsers() throws Exception {
        // Initialize the database
        hserRepository.saveAndFlush(hser);

        // Get all the hserList
        restHserMockMvc.perform(get("/api/hsers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hser.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getHser() throws Exception {
        // Initialize the database
        hserRepository.saveAndFlush(hser);

        // Get the hser
        restHserMockMvc.perform(get("/api/hsers/{id}", hser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(hser.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingHser() throws Exception {
        // Get the hser
        restHserMockMvc.perform(get("/api/hsers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHser() throws Exception {
        // Initialize the database
        hserRepository.saveAndFlush(hser);

        int databaseSizeBeforeUpdate = hserRepository.findAll().size();

        // Update the hser
        Hser updatedHser = hserRepository.findById(hser.getId()).get();
        // Disconnect from session so that the updates on updatedHser are not directly saved in db
        em.detach(updatedHser);

        restHserMockMvc.perform(put("/api/hsers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHser)))
            .andExpect(status().isOk());

        // Validate the Hser in the database
        List<Hser> hserList = hserRepository.findAll();
        assertThat(hserList).hasSize(databaseSizeBeforeUpdate);
        Hser testHser = hserList.get(hserList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingHser() throws Exception {
        int databaseSizeBeforeUpdate = hserRepository.findAll().size();

        // Create the Hser

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHserMockMvc.perform(put("/api/hsers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hser)))
            .andExpect(status().isBadRequest());

        // Validate the Hser in the database
        List<Hser> hserList = hserRepository.findAll();
        assertThat(hserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHser() throws Exception {
        // Initialize the database
        hserRepository.saveAndFlush(hser);

        int databaseSizeBeforeDelete = hserRepository.findAll().size();

        // Get the hser
        restHserMockMvc.perform(delete("/api/hsers/{id}", hser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Hser> hserList = hserRepository.findAll();
        assertThat(hserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hser.class);
        Hser hser1 = new Hser();
        hser1.setId(1L);
        Hser hser2 = new Hser();
        hser2.setId(hser1.getId());
        assertThat(hser1).isEqualTo(hser2);
        hser2.setId(2L);
        assertThat(hser1).isNotEqualTo(hser2);
        hser1.setId(null);
        assertThat(hser1).isNotEqualTo(hser2);
    }
}

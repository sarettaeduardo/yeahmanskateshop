package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.YeahmanskateshopApp;

import br.com.siteyeahmanskateshop.domain.Produtos;
import br.com.siteyeahmanskateshop.repository.ProdutosRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;


import static br.com.siteyeahmanskateshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProdutosResource REST controller.
 *
 * @see ProdutosResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YeahmanskateshopApp.class)
public class ProdutosResourceIntTest {

    private static final byte[] DEFAULT_FOTO_0 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO_0 = TestUtil.createByteArray(3145728, "1");
    private static final String DEFAULT_FOTO_0_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_0_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_FOTO_1 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO_1 = TestUtil.createByteArray(3145728, "1");
    private static final String DEFAULT_FOTO_1_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_1_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_FOTO_2 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO_2 = TestUtil.createByteArray(3145728, "1");
    private static final String DEFAULT_FOTO_2_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_2_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_FOTO_3 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO_3 = TestUtil.createByteArray(3145728, "1");
    private static final String DEFAULT_FOTO_3_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_3_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_FOTO_4 = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FOTO_4 = TestUtil.createByteArray(3145728, "1");
    private static final String DEFAULT_FOTO_4_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FOTO_4_CONTENT_TYPE = "image/png";

    private static final Long DEFAULT_VALOR_VENDA = 1L;
    private static final Long UPDATED_VALOR_VENDA = 2L;

    private static final Long DEFAULT_VALOR_CUSTO = 1L;
    private static final Long UPDATED_VALOR_CUSTO = 2L;

    private static final Long DEFAULT_QUANTIDADE = 1L;
    private static final Long UPDATED_QUANTIDADE = 2L;

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final Long DEFAULT_PROMOCAO = 1L;
    private static final Long UPDATED_PROMOCAO = 2L;

    private static final Boolean DEFAULT_DESTAQUE = false;
    private static final Boolean UPDATED_DESTAQUE = true;

    @Autowired
    private ProdutosRepository produtosRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProdutosMockMvc;

    private Produtos produtos;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProdutosResource produtosResource = new ProdutosResource(produtosRepository);
        this.restProdutosMockMvc = MockMvcBuilders.standaloneSetup(produtosResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Produtos createEntity(EntityManager em) {
        Produtos produtos = new Produtos()
            .foto0(DEFAULT_FOTO_0)
            .foto0ContentType(DEFAULT_FOTO_0_CONTENT_TYPE)
            .foto1(DEFAULT_FOTO_1)
            .foto1ContentType(DEFAULT_FOTO_1_CONTENT_TYPE)
            .foto2(DEFAULT_FOTO_2)
            .foto2ContentType(DEFAULT_FOTO_2_CONTENT_TYPE)
            .foto3(DEFAULT_FOTO_3)
            .foto3ContentType(DEFAULT_FOTO_3_CONTENT_TYPE)
            .foto4(DEFAULT_FOTO_4)
            .foto4ContentType(DEFAULT_FOTO_4_CONTENT_TYPE)
            .valorVenda(DEFAULT_VALOR_VENDA)
            .valorCusto(DEFAULT_VALOR_CUSTO)
            .quantidade(DEFAULT_QUANTIDADE)
            .nome(DEFAULT_NOME)
            .descricao(DEFAULT_DESCRICAO)
            .promocao(DEFAULT_PROMOCAO)
            .destaque(DEFAULT_DESTAQUE);
        return produtos;
    }

    @Before
    public void initTest() {
        produtos = createEntity(em);
    }

    @Test
    @Transactional
    public void createProdutos() throws Exception {
        int databaseSizeBeforeCreate = produtosRepository.findAll().size();

        // Create the Produtos
        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isCreated());

        // Validate the Produtos in the database
        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeCreate + 1);
        Produtos testProdutos = produtosList.get(produtosList.size() - 1);
        assertThat(testProdutos.getFoto0()).isEqualTo(DEFAULT_FOTO_0);
        assertThat(testProdutos.getFoto0ContentType()).isEqualTo(DEFAULT_FOTO_0_CONTENT_TYPE);
        assertThat(testProdutos.getFoto1()).isEqualTo(DEFAULT_FOTO_1);
        assertThat(testProdutos.getFoto1ContentType()).isEqualTo(DEFAULT_FOTO_1_CONTENT_TYPE);
        assertThat(testProdutos.getFoto2()).isEqualTo(DEFAULT_FOTO_2);
        assertThat(testProdutos.getFoto2ContentType()).isEqualTo(DEFAULT_FOTO_2_CONTENT_TYPE);
        assertThat(testProdutos.getFoto3()).isEqualTo(DEFAULT_FOTO_3);
        assertThat(testProdutos.getFoto3ContentType()).isEqualTo(DEFAULT_FOTO_3_CONTENT_TYPE);
        assertThat(testProdutos.getFoto4()).isEqualTo(DEFAULT_FOTO_4);
        assertThat(testProdutos.getFoto4ContentType()).isEqualTo(DEFAULT_FOTO_4_CONTENT_TYPE);
        assertThat(testProdutos.getValorVenda()).isEqualTo(DEFAULT_VALOR_VENDA);
        assertThat(testProdutos.getValorCusto()).isEqualTo(DEFAULT_VALOR_CUSTO);
        assertThat(testProdutos.getQuantidade()).isEqualTo(DEFAULT_QUANTIDADE);
        assertThat(testProdutos.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testProdutos.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testProdutos.getPromocao()).isEqualTo(DEFAULT_PROMOCAO);
        assertThat(testProdutos.isDestaque()).isEqualTo(DEFAULT_DESTAQUE);
    }

    @Test
    @Transactional
    public void createProdutosWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = produtosRepository.findAll().size();

        // Create the Produtos with an existing ID
        produtos.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        // Validate the Produtos in the database
        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkValorVendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = produtosRepository.findAll().size();
        // set the field null
        produtos.setValorVenda(null);

        // Create the Produtos, which fails.

        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorCustoIsRequired() throws Exception {
        int databaseSizeBeforeTest = produtosRepository.findAll().size();
        // set the field null
        produtos.setValorCusto(null);

        // Create the Produtos, which fails.

        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQuantidadeIsRequired() throws Exception {
        int databaseSizeBeforeTest = produtosRepository.findAll().size();
        // set the field null
        produtos.setQuantidade(null);

        // Create the Produtos, which fails.

        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = produtosRepository.findAll().size();
        // set the field null
        produtos.setNome(null);

        // Create the Produtos, which fails.

        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescricaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = produtosRepository.findAll().size();
        // set the field null
        produtos.setDescricao(null);

        // Create the Produtos, which fails.

        restProdutosMockMvc.perform(post("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProdutos() throws Exception {
        // Initialize the database
        produtosRepository.saveAndFlush(produtos);

        // Get all the produtosList
        restProdutosMockMvc.perform(get("/api/produtos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(produtos.getId().intValue())))
            .andExpect(jsonPath("$.[*].foto0ContentType").value(hasItem(DEFAULT_FOTO_0_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto0").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO_0))))
            .andExpect(jsonPath("$.[*].foto1ContentType").value(hasItem(DEFAULT_FOTO_1_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto1").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO_1))))
            .andExpect(jsonPath("$.[*].foto2ContentType").value(hasItem(DEFAULT_FOTO_2_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto2").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO_2))))
            .andExpect(jsonPath("$.[*].foto3ContentType").value(hasItem(DEFAULT_FOTO_3_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto3").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO_3))))
            .andExpect(jsonPath("$.[*].foto4ContentType").value(hasItem(DEFAULT_FOTO_4_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].foto4").value(hasItem(Base64Utils.encodeToString(DEFAULT_FOTO_4))))
            .andExpect(jsonPath("$.[*].valorVenda").value(hasItem(DEFAULT_VALOR_VENDA.intValue())))
            .andExpect(jsonPath("$.[*].valorCusto").value(hasItem(DEFAULT_VALOR_CUSTO.intValue())))
            .andExpect(jsonPath("$.[*].quantidade").value(hasItem(DEFAULT_QUANTIDADE.intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
            .andExpect(jsonPath("$.[*].promocao").value(hasItem(DEFAULT_PROMOCAO.intValue())))
            .andExpect(jsonPath("$.[*].destaque").value(hasItem(DEFAULT_DESTAQUE.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getProdutos() throws Exception {
        // Initialize the database
        produtosRepository.saveAndFlush(produtos);

        // Get the produtos
        restProdutosMockMvc.perform(get("/api/produtos/{id}", produtos.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(produtos.getId().intValue()))
            .andExpect(jsonPath("$.foto0ContentType").value(DEFAULT_FOTO_0_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto0").value(Base64Utils.encodeToString(DEFAULT_FOTO_0)))
            .andExpect(jsonPath("$.foto1ContentType").value(DEFAULT_FOTO_1_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto1").value(Base64Utils.encodeToString(DEFAULT_FOTO_1)))
            .andExpect(jsonPath("$.foto2ContentType").value(DEFAULT_FOTO_2_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto2").value(Base64Utils.encodeToString(DEFAULT_FOTO_2)))
            .andExpect(jsonPath("$.foto3ContentType").value(DEFAULT_FOTO_3_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto3").value(Base64Utils.encodeToString(DEFAULT_FOTO_3)))
            .andExpect(jsonPath("$.foto4ContentType").value(DEFAULT_FOTO_4_CONTENT_TYPE))
            .andExpect(jsonPath("$.foto4").value(Base64Utils.encodeToString(DEFAULT_FOTO_4)))
            .andExpect(jsonPath("$.valorVenda").value(DEFAULT_VALOR_VENDA.intValue()))
            .andExpect(jsonPath("$.valorCusto").value(DEFAULT_VALOR_CUSTO.intValue()))
            .andExpect(jsonPath("$.quantidade").value(DEFAULT_QUANTIDADE.intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.promocao").value(DEFAULT_PROMOCAO.intValue()))
            .andExpect(jsonPath("$.destaque").value(DEFAULT_DESTAQUE.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProdutos() throws Exception {
        // Get the produtos
        restProdutosMockMvc.perform(get("/api/produtos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProdutos() throws Exception {
        // Initialize the database
        produtosRepository.saveAndFlush(produtos);

        int databaseSizeBeforeUpdate = produtosRepository.findAll().size();

        // Update the produtos
        Produtos updatedProdutos = produtosRepository.findById(produtos.getId()).get();
        // Disconnect from session so that the updates on updatedProdutos are not directly saved in db
        em.detach(updatedProdutos);
        updatedProdutos
            .foto0(UPDATED_FOTO_0)
            .foto0ContentType(UPDATED_FOTO_0_CONTENT_TYPE)
            .foto1(UPDATED_FOTO_1)
            .foto1ContentType(UPDATED_FOTO_1_CONTENT_TYPE)
            .foto2(UPDATED_FOTO_2)
            .foto2ContentType(UPDATED_FOTO_2_CONTENT_TYPE)
            .foto3(UPDATED_FOTO_3)
            .foto3ContentType(UPDATED_FOTO_3_CONTENT_TYPE)
            .foto4(UPDATED_FOTO_4)
            .foto4ContentType(UPDATED_FOTO_4_CONTENT_TYPE)
            .valorVenda(UPDATED_VALOR_VENDA)
            .valorCusto(UPDATED_VALOR_CUSTO)
            .quantidade(UPDATED_QUANTIDADE)
            .nome(UPDATED_NOME)
            .descricao(UPDATED_DESCRICAO)
            .promocao(UPDATED_PROMOCAO)
            .destaque(UPDATED_DESTAQUE);

        restProdutosMockMvc.perform(put("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProdutos)))
            .andExpect(status().isOk());

        // Validate the Produtos in the database
        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeUpdate);
        Produtos testProdutos = produtosList.get(produtosList.size() - 1);
        assertThat(testProdutos.getFoto0()).isEqualTo(UPDATED_FOTO_0);
        assertThat(testProdutos.getFoto0ContentType()).isEqualTo(UPDATED_FOTO_0_CONTENT_TYPE);
        assertThat(testProdutos.getFoto1()).isEqualTo(UPDATED_FOTO_1);
        assertThat(testProdutos.getFoto1ContentType()).isEqualTo(UPDATED_FOTO_1_CONTENT_TYPE);
        assertThat(testProdutos.getFoto2()).isEqualTo(UPDATED_FOTO_2);
        assertThat(testProdutos.getFoto2ContentType()).isEqualTo(UPDATED_FOTO_2_CONTENT_TYPE);
        assertThat(testProdutos.getFoto3()).isEqualTo(UPDATED_FOTO_3);
        assertThat(testProdutos.getFoto3ContentType()).isEqualTo(UPDATED_FOTO_3_CONTENT_TYPE);
        assertThat(testProdutos.getFoto4()).isEqualTo(UPDATED_FOTO_4);
        assertThat(testProdutos.getFoto4ContentType()).isEqualTo(UPDATED_FOTO_4_CONTENT_TYPE);
        assertThat(testProdutos.getValorVenda()).isEqualTo(UPDATED_VALOR_VENDA);
        assertThat(testProdutos.getValorCusto()).isEqualTo(UPDATED_VALOR_CUSTO);
        assertThat(testProdutos.getQuantidade()).isEqualTo(UPDATED_QUANTIDADE);
        assertThat(testProdutos.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testProdutos.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testProdutos.getPromocao()).isEqualTo(UPDATED_PROMOCAO);
        assertThat(testProdutos.isDestaque()).isEqualTo(UPDATED_DESTAQUE);
    }

    @Test
    @Transactional
    public void updateNonExistingProdutos() throws Exception {
        int databaseSizeBeforeUpdate = produtosRepository.findAll().size();

        // Create the Produtos

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProdutosMockMvc.perform(put("/api/produtos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produtos)))
            .andExpect(status().isBadRequest());

        // Validate the Produtos in the database
        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProdutos() throws Exception {
        // Initialize the database
        produtosRepository.saveAndFlush(produtos);

        int databaseSizeBeforeDelete = produtosRepository.findAll().size();

        // Get the produtos
        restProdutosMockMvc.perform(delete("/api/produtos/{id}", produtos.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Produtos> produtosList = produtosRepository.findAll();
        assertThat(produtosList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Produtos.class);
        Produtos produtos1 = new Produtos();
        produtos1.setId(1L);
        Produtos produtos2 = new Produtos();
        produtos2.setId(produtos1.getId());
        assertThat(produtos1).isEqualTo(produtos2);
        produtos2.setId(2L);
        assertThat(produtos1).isNotEqualTo(produtos2);
        produtos1.setId(null);
        assertThat(produtos1).isNotEqualTo(produtos2);
    }
}

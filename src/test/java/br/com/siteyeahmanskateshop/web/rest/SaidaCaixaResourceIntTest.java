package br.com.siteyeahmanskateshop.web.rest;

import br.com.siteyeahmanskateshop.YeahmanskateshopApp;

import br.com.siteyeahmanskateshop.domain.SaidaCaixa;
import br.com.siteyeahmanskateshop.repository.SaidaCaixaRepository;
import br.com.siteyeahmanskateshop.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static br.com.siteyeahmanskateshop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SaidaCaixaResource REST controller.
 *
 * @see SaidaCaixaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YeahmanskateshopApp.class)
public class SaidaCaixaResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final Long DEFAULT_VALOR = 1L;
    private static final Long UPDATED_VALOR = 2L;

    private static final Instant DEFAULT_DATA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private SaidaCaixaRepository saidaCaixaRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSaidaCaixaMockMvc;

    private SaidaCaixa saidaCaixa;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SaidaCaixaResource saidaCaixaResource = new SaidaCaixaResource(saidaCaixaRepository);
        this.restSaidaCaixaMockMvc = MockMvcBuilders.standaloneSetup(saidaCaixaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SaidaCaixa createEntity(EntityManager em) {
        SaidaCaixa saidaCaixa = new SaidaCaixa()
            .nome(DEFAULT_NOME)
            .tipo(DEFAULT_TIPO)
            .descricao(DEFAULT_DESCRICAO)
            .valor(DEFAULT_VALOR)
            .data(DEFAULT_DATA);
        return saidaCaixa;
    }

    @Before
    public void initTest() {
        saidaCaixa = createEntity(em);
    }

    @Test
    @Transactional
    public void createSaidaCaixa() throws Exception {
        int databaseSizeBeforeCreate = saidaCaixaRepository.findAll().size();

        // Create the SaidaCaixa
        restSaidaCaixaMockMvc.perform(post("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isCreated());

        // Validate the SaidaCaixa in the database
        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeCreate + 1);
        SaidaCaixa testSaidaCaixa = saidaCaixaList.get(saidaCaixaList.size() - 1);
        assertThat(testSaidaCaixa.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testSaidaCaixa.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testSaidaCaixa.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testSaidaCaixa.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testSaidaCaixa.getData()).isEqualTo(DEFAULT_DATA);
    }

    @Test
    @Transactional
    public void createSaidaCaixaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = saidaCaixaRepository.findAll().size();

        // Create the SaidaCaixa with an existing ID
        saidaCaixa.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSaidaCaixaMockMvc.perform(post("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isBadRequest());

        // Validate the SaidaCaixa in the database
        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = saidaCaixaRepository.findAll().size();
        // set the field null
        saidaCaixa.setNome(null);

        // Create the SaidaCaixa, which fails.

        restSaidaCaixaMockMvc.perform(post("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isBadRequest());

        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorIsRequired() throws Exception {
        int databaseSizeBeforeTest = saidaCaixaRepository.findAll().size();
        // set the field null
        saidaCaixa.setValor(null);

        // Create the SaidaCaixa, which fails.

        restSaidaCaixaMockMvc.perform(post("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isBadRequest());

        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = saidaCaixaRepository.findAll().size();
        // set the field null
        saidaCaixa.setData(null);

        // Create the SaidaCaixa, which fails.

        restSaidaCaixaMockMvc.perform(post("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isBadRequest());

        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSaidaCaixas() throws Exception {
        // Initialize the database
        saidaCaixaRepository.saveAndFlush(saidaCaixa);

        // Get all the saidaCaixaList
        restSaidaCaixaMockMvc.perform(get("/api/saida-caixas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(saidaCaixa.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(DEFAULT_VALOR.intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())));
    }
    

    @Test
    @Transactional
    public void getSaidaCaixa() throws Exception {
        // Initialize the database
        saidaCaixaRepository.saveAndFlush(saidaCaixa);

        // Get the saidaCaixa
        restSaidaCaixaMockMvc.perform(get("/api/saida-caixas/{id}", saidaCaixa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(saidaCaixa.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.valor").value(DEFAULT_VALOR.intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSaidaCaixa() throws Exception {
        // Get the saidaCaixa
        restSaidaCaixaMockMvc.perform(get("/api/saida-caixas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSaidaCaixa() throws Exception {
        // Initialize the database
        saidaCaixaRepository.saveAndFlush(saidaCaixa);

        int databaseSizeBeforeUpdate = saidaCaixaRepository.findAll().size();

        // Update the saidaCaixa
        SaidaCaixa updatedSaidaCaixa = saidaCaixaRepository.findById(saidaCaixa.getId()).get();
        // Disconnect from session so that the updates on updatedSaidaCaixa are not directly saved in db
        em.detach(updatedSaidaCaixa);
        updatedSaidaCaixa
            .nome(UPDATED_NOME)
            .tipo(UPDATED_TIPO)
            .descricao(UPDATED_DESCRICAO)
            .valor(UPDATED_VALOR)
            .data(UPDATED_DATA);

        restSaidaCaixaMockMvc.perform(put("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSaidaCaixa)))
            .andExpect(status().isOk());

        // Validate the SaidaCaixa in the database
        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeUpdate);
        SaidaCaixa testSaidaCaixa = saidaCaixaList.get(saidaCaixaList.size() - 1);
        assertThat(testSaidaCaixa.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testSaidaCaixa.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testSaidaCaixa.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testSaidaCaixa.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testSaidaCaixa.getData()).isEqualTo(UPDATED_DATA);
    }

    @Test
    @Transactional
    public void updateNonExistingSaidaCaixa() throws Exception {
        int databaseSizeBeforeUpdate = saidaCaixaRepository.findAll().size();

        // Create the SaidaCaixa

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSaidaCaixaMockMvc.perform(put("/api/saida-caixas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(saidaCaixa)))
            .andExpect(status().isBadRequest());

        // Validate the SaidaCaixa in the database
        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSaidaCaixa() throws Exception {
        // Initialize the database
        saidaCaixaRepository.saveAndFlush(saidaCaixa);

        int databaseSizeBeforeDelete = saidaCaixaRepository.findAll().size();

        // Get the saidaCaixa
        restSaidaCaixaMockMvc.perform(delete("/api/saida-caixas/{id}", saidaCaixa.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SaidaCaixa> saidaCaixaList = saidaCaixaRepository.findAll();
        assertThat(saidaCaixaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SaidaCaixa.class);
        SaidaCaixa saidaCaixa1 = new SaidaCaixa();
        saidaCaixa1.setId(1L);
        SaidaCaixa saidaCaixa2 = new SaidaCaixa();
        saidaCaixa2.setId(saidaCaixa1.getId());
        assertThat(saidaCaixa1).isEqualTo(saidaCaixa2);
        saidaCaixa2.setId(2L);
        assertThat(saidaCaixa1).isNotEqualTo(saidaCaixa2);
        saidaCaixa1.setId(null);
        assertThat(saidaCaixa1).isNotEqualTo(saidaCaixa2);
    }
}
